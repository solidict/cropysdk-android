# CropySDK - README #

### Download ###

Configure your module-level `build.gradle` to include 'maven' plugin:
```
repositories {
    maven {
        url "http://dl.bintray.com/solidict/maven"
    }
}
```

Add the Cropy SDK dependency to the 'dependencies' block of your module-level `build.gradle`:
```
    compile 'com.solidict.cropysdk:cropysdk:1.0.25'
```

### Usage ###

**Normal usage with default theme and all features:**

```
Cropy cropy = new Cropy.Builder(bitmap).build();
cropy.start(MainActivity.this, bitmap);
```

--------

**You can disable any feature with using `without()` method:**

```
Cropy cropy = new Cropy.Builder(bitmap).without(Cropy.Feature.CAPS, Cropy.Feature.ADJUSTMENT, Cropy.Feature.FRAME, Cropy.Feature.PEN, Cropy.Feature.FOCUS, Cropy.Feature.TEXT, Cropy.Feature.LINE).build();
cropy.start(MainActivity.this, bitmap);
```

--------

**You can customize colors and texts with using `UiProperty` object:**

```
Cropy cropy = new Cropy.Builder(bitmap).without(Cropy.Feature.CAPS, Cropy.Feature.RECT, Cropy.Feature.ADJUSTMENT, Cropy.Feature.FRAME, Cropy.Feature.PEN, Cropy.Feature.FOCUS, Cropy.Feature.TEXT, Cropy.Feature.CIRCLE, Cropy.Feature.LINE).build();

UiProperty uiProperty = new UiProperty(MainActivity.this);
uiProperty.setHeaderColor(getResources().getColor(R.color.blue));
uiProperty.setBackgroundColor(getResources().getColor(R.color.colorAccent));
uiProperty.setShareHeaderColor(getResources().getColor(R.color.red));
uiProperty.setShareHeaderTitle("Share Title Test");
uiProperty.setBackIcon(MainActivity.this, R.drawable.ic_back_triangle);

.
.
.

cropy.setUiProperty(uiProperty);
```

```
cropy.setShareScenario(Cropy.ShareScenario.Toolbar);
// If you choose ShareScenario.Url or ShareScenario.Bitmap, you can implement `SharingListener` interface and override `onUrlPrepared(String url)` and `onBitmapPrepared(Bitmap bitmap)` methods.
```

```
cropy.setAutoFinish(false);
// If you set this 'false', Cropy won't be finished when you're done with it. This can only be used if your share scenario is set to 'Bitmap'.
// When you decide to finish Cropy, you only need to call finish() method of Cropy.
```

```
cropy.setPreCrop(true);
// If you set this true, you can implement `CropListener` interface and override `onCropped(Bitmap bitmap)` method and write that code:

//    @Override
//    public void onCropped(Bitmap bitmap) {
//        CropyApp.croppedBitmap = bitmap;
//        CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();
//        cropy.setPreCrop(false);
//        cropy.start(MainActivity.this, bitmap);
//    }
```

```
cropy.start(MainActivity.this, bitmap);
```

**You must set `apiKey` and `domain` parameters:**

```
Cropy.apiKey = "YOUR_API_KEY_HERE";
Cropy.domain = "YOUR_DOMAIN_HERE";
```

**Note:** The app needs to have READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE permissions to use Cropy SDK