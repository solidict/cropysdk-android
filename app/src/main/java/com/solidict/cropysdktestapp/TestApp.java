package com.solidict.cropysdktestapp;

import android.app.Application;

import com.solidict.cropysdk.utils.FontsOverride;

/**
 * Created by serdarbuyukkanli on 31/01/17.
 */
public class TestApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Turkcell_Satura_Regular.ttf");

    }

}
