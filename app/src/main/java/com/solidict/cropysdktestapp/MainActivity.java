package com.solidict.cropysdktestapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.solidict.cropysdk.Cropy;
import com.solidict.cropysdk.CropyApp;
import com.solidict.cropysdk.interfaces.CropListener;
import com.solidict.cropysdk.interfaces.SharingListener;
import com.solidict.cropysdk.models.UiProperty;
import com.solidict.cropysdk.utils.Utils;

public class MainActivity extends Activity implements SharingListener, CropListener {

    ImageView ivImage;
    Cropy cropy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCropActivity = (Button) findViewById(R.id.btnCropActivity);
        ivImage = (ImageView) findViewById(R.id.ivImage);


        btnCropActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ((BitmapDrawable) ivImage.getDrawable()).getBitmap();

                Cropy.apiKey = "YOUR_API_KEY_HERE";
                Cropy.origin = "YOUR_ORIGIN_HERE";

                cropy = new Cropy.Builder(bitmap).without(Cropy.Feature.RECT, Cropy.Feature.LINE).build();

                UiProperty uiProperty = new UiProperty(MainActivity.this);
                uiProperty.setCropHeaderColor(getResources().getColor(R.color.filter_green));
                uiProperty.setCropHeaderTitle("Crop Title Test");
                uiProperty.setCropRectColor(getResources().getColor(R.color.red));
                uiProperty.setHeaderTextColor(getResources().getColor(R.color.red));

                uiProperty.setHeaderColor(getResources().getColor(R.color.blue));
                uiProperty.setHeaderTitle("Edit Title Test");
                uiProperty.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                uiProperty.setCropHeaderTextColor(getResources().getColor(R.color.red));
                uiProperty.setFooterColor(getResources().getColor(R.color.black));

                uiProperty.setShareHeaderColor(getResources().getColor(R.color.red));
                uiProperty.setShareHeaderTitle("Share Title Test");
                uiProperty.setShareBackgroundColor(getResources().getColor(R.color.black));

                uiProperty.setBackIcon(MainActivity.this, R.drawable.ic_back_triangle);

                cropy.setUiProperty(uiProperty);
                cropy.setShareScenario(Cropy.ShareScenario.Bitmap);
                cropy.setAutoFinish(false);

                cropy.start(MainActivity.this, bitmap);


            }
        });

    }

    @Override
    public void onUrlPrepared(String url) {
        Toast.makeText(MainActivity.this, "Url ready to share!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBitmapPrepared(Bitmap bitmap) {
        Toast.makeText(MainActivity.this, "Bitmap ready to share!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCropped(Bitmap bitmap) {
        CropyApp.croppedBitmap = bitmap;
        CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();
        cropy.setPreCrop(false);
        cropy.start(MainActivity.this, bitmap);
    }
}
