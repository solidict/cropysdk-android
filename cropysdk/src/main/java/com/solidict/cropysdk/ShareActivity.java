package com.solidict.cropysdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.solidict.cropysdk.adapters.ShareAdapter;
import com.solidict.cropysdk.gpuimage.GPUImage;
import com.solidict.cropysdk.gpuimage.GPUImagePixelationFilter;
import com.solidict.cropysdk.interfaces.AdjustmentListener;
import com.solidict.cropysdk.interfaces.CanvasActionListener;
import com.solidict.cropysdk.interfaces.CapsTextStyleListener;
import com.solidict.cropysdk.interfaces.CapsTouchedListener;
import com.solidict.cropysdk.interfaces.ColorListener;
import com.solidict.cropysdk.interfaces.CropListener;
import com.solidict.cropysdk.interfaces.DeleteListener;
import com.solidict.cropysdk.interfaces.FilterListener;
import com.solidict.cropysdk.interfaces.FocusListener;
import com.solidict.cropysdk.interfaces.FrameListener;
import com.solidict.cropysdk.interfaces.MatrixListener;
import com.solidict.cropysdk.interfaces.PathListener;
import com.solidict.cropysdk.interfaces.RestApi;
import com.solidict.cropysdk.interfaces.RotateListener;
import com.solidict.cropysdk.interfaces.ShapeDrawerListener;
import com.solidict.cropysdk.interfaces.ShareListener;
import com.solidict.cropysdk.interfaces.SharingListener;
import com.solidict.cropysdk.interfaces.SizeListener;
import com.solidict.cropysdk.interfaces.TextStyleListener;
import com.solidict.cropysdk.interfaces.TextTouchListener;
import com.solidict.cropysdk.log.LogManager;
import com.solidict.cropysdk.models.CanvasValues;
import com.solidict.cropysdk.models.ShareItem;
import com.solidict.cropysdk.models.SnapShot;
import com.solidict.cropysdk.models.UiProperty;
import com.solidict.cropysdk.utils.Utils;
import com.solidict.cropysdk.views.ActionEditText;
import com.solidict.cropysdk.views.Adjustment;
import com.solidict.cropysdk.views.CanvasView;
import com.solidict.cropysdk.views.CircleBlurView;
import com.solidict.cropysdk.views.Filters;
import com.solidict.cropysdk.views.Focus;
import com.solidict.cropysdk.views.Frames;
import com.solidict.cropysdk.views.GapView;
import com.solidict.cropysdk.views.Shapes;
import com.solidict.cropysdk.views.TextStyles;
import com.squareup.okhttp.OkHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.solidict.cropysdk.Cropy.activity;

public class ShareActivity extends BaseActivity implements CropListener, RotateListener, MatrixListener, ColorListener, SizeListener, ShapeDrawerListener, TextTouchListener, FrameListener, FilterListener, FocusListener, AdjustmentListener, TextStyleListener, CapsTextStyleListener, PathListener, CanvasActionListener, ShareListener, CapsTouchedListener, DeleteListener {


    Adjustment adjustment;
    Frames frames;
    Filters filters;
    Focus focus;
    GridView gvShare;
    ImageView ivShare;
    ImageView ivBack;
    RelativeLayout rlCenter;
    CanvasView canvas;
    ImageView ivUndo;
    RelativeLayout rlColors;
    ImageView ivCrop;
    ImageView ivRotate;
    RelativeLayout rlshapes;
    TextView tvTitle;
    RelativeLayout rootLayout;
    EditText etText;
    RelativeLayout rlTextStyle;
    TextStyles vTextStyle;
    RelativeLayout rlFrames;
    RelativeLayout rlFilters;
    RelativeLayout rlAdjustment;
    RelativeLayout rlFocus;
    RelativeLayout rlCenterRoot;
    CircleBlurView cbvImage;
    ImageView ivMosaic;
    ActionEditText etCapsPreview;
    LinearLayout llCapsPreview;
    View viewTextDivider;
    ScrollView svMosaic;
    GapView gv;
    RelativeLayout rlToolbar;
    Shapes shapes;
    RelativeLayout rlTopBar;


    String webPageTitle;
    String webUrl;
    boolean fromRotate;
    int type;
    boolean save;
    File shareFile;
    //    File file;
    RestApi restApi;
    String shareUrl = "http://cropyapp.com/";
    ProgressDialog dialog;
    Bitmap bit;
    Bitmap focusBitmap;
    CountDownTimer timer;
    CanvasView.Drawer currentDrawer;

    boolean focusEnabled = false;
    private boolean capsActive = false;
    private boolean startCrop = true;
    boolean editEnabled = true;

    private ArrayList<SnapShot> snapShots = new ArrayList();
    private ArrayList<Integer> historyList = new ArrayList();

    BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("finish_activity")) {
                finish();
            }
        }
    };

    private void initViews() {

        adjustment = (Adjustment) findViewById(R.id.adjustment);
        frames = (Frames) findViewById(R.id.frames);
        filters = (Filters) findViewById(R.id.filters);
        focus = (Focus) findViewById(R.id.focus);
        gvShare = (GridView) findViewById(R.id.gvShare);
        ivShare = (ImageView) findViewById(R.id.ivShare);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        rlCenter = (RelativeLayout) findViewById(R.id.rlCenter);

        canvas = (CanvasView) findViewById(R.id.canvas);
        ivUndo = (ImageView) findViewById(R.id.ivUndo);
        rlColors = (RelativeLayout) findViewById(R.id.rlColors);
        ivCrop = (ImageView) findViewById(R.id.ivCrop);
        ivRotate = (ImageView) findViewById(R.id.ivRotate);
        rlshapes = (RelativeLayout) findViewById(R.id.rlshapes);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        etText = (EditText) findViewById(R.id.etText);
        rlTextStyle = (RelativeLayout) findViewById(R.id.rlTextStyle);
        vTextStyle = (TextStyles) findViewById(R.id.vTextStyle);
        rlFrames = (RelativeLayout) findViewById(R.id.rlFrames);
        rlFilters = (RelativeLayout) findViewById(R.id.rlFilters);
        rlAdjustment = (RelativeLayout) findViewById(R.id.rlAdjustment);
        rlFocus = (RelativeLayout) findViewById(R.id.rlFocus);
        rlCenterRoot = (RelativeLayout) findViewById(R.id.rlCenterRoot);
        cbvImage = (CircleBlurView) findViewById(R.id.cbvImage);
        ivMosaic = (ImageView) findViewById(R.id.ivMosaic);
        etCapsPreview = (ActionEditText) findViewById(R.id.etCapsPreview);
        llCapsPreview = (LinearLayout) findViewById(R.id.llCapsPreview);
        viewTextDivider = (View) findViewById(R.id.viewTextDivider);
        svMosaic = (ScrollView) findViewById(R.id.svMosaic);
        gv = (GapView) findViewById(R.id.gv);
        rlToolbar = (RelativeLayout) findViewById(R.id.rlToolbar);
        shapes = (Shapes) findViewById(R.id.shapes);
        rlTopBar = (RelativeLayout) findViewById(R.id.rlTopBar);


//        Typeface regular = Typeface.createFromAsset(getAssets(),
//                "fonts/Turkcell_Satura_Regular.ttf");
//
//        tvTitle.setTypeface(regular, Typeface.BOLD);


        rlTextStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogManager.addLog("Editing Screen - Feature changed - Crop");


                if (!focusTouched) {
                    cbvImage.setRadius(5000);
                    canvas.setCircleRadius(5000);
                }
                canvas.setFocusDrawModeEnabled(false);
                canvas.invalidate();

                shapes.resetBorders();
                onShapeChanged(null);

                Bitmap bitmap = takePartialScreenShot();
                int padding = (int) Utils.convertDpToPixel(3, ShareActivity.this);
                if (bitmap != null) {
                    Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, padding, padding, bitmap.getWidth() - 2 * padding, bitmap.getHeight() - 2 * padding);
                    CropActivity.startCropActivity(ShareActivity.this, croppedBitmap, 0, uiProperty);
                    overridePendingTransition(0, 0);
                }
            }
        });

        ivRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogManager.addLog("Editing Screen - Feature changed - Rotate");


                if (!focusTouched) {
                    cbvImage.setRadius(5000);
                    canvas.setCircleRadius(5000);
                }
                canvas.setFocusDrawModeEnabled(false);
                canvas.invalidate();


                shapes.resetBorders();
                onShapeChanged(null);


                Bitmap bitmap = takePartialScreenShot();
                int padding = (int) Utils.convertDpToPixel(3, ShareActivity.this);
                if (bitmap != null) {
                    Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, padding, padding, bitmap.getWidth() - 2 * padding, bitmap.getHeight() - 2 * padding);
                    boolean undoEnabled = false;

                    if (historyList.size() > 1 || savedHistoryList.size() > 0)
                        undoEnabled = true;

                    RotateActivity.startRotateActivity(ShareActivity.this, croppedBitmap, false, undoEnabled, shareScenario, uiProperty);
                    overridePendingTransition(0, 0);
                }
            }
        });

        rlToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gvShare.setVisibility(View.GONE);
                rlToolbar.setVisibility(View.GONE);
                ivShare.setVisibility(View.VISIBLE);

                tvTitle.setText(uiProperty.getHeaderTitle());
                rlTopBar.setBackgroundColor(uiProperty.getHeaderColor());


            }
        });

        ivUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!historyList.isEmpty())
                    Log.d("logHistoryy", "before historyList size: " + historyList.size() + ", last Item: " + historyList.get(historyList.size() - 1));

                boolean willUndo = false;

                if (historyList.size() > 1) {
                    if (historyList.get(historyList.size() - 1) == 0) {
                        willUndo = true;
                    }
                }

                undo();

                if (willUndo) {
                    undo();
                    Log.d("logHistoryy", "extra undo");

                }


            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteTempFiles();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finishActivityAndStartCropActivity();
                    }
                }, 200);
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Runtime.getRuntime().gc();


                if (bit != null) {


                    showTextOrCapsBar = false;

                    if (!isFinishing()) {
                        dialog.show();
                    }

                    llCapsPreview.setVisibility(View.GONE);
                    rlTextStyle.setVisibility(View.GONE);
                    capsActive = false;
                    shapes.resetBorders();
                    onShapeChanged(null);
                    hideSoftKeyboard();


                    rlToolbar.setVisibility(View.VISIBLE);
                    ivShare.setVisibility(View.INVISIBLE);


                    switch (shareScenario) {
                        case Toolbar:
                            shareImage();
                            break;

                        case Url:
                            prepareUrl();
                            break;

                        case Bitmap:
                            prepareBitmap();
                            break;

                    }


                }
            }
        });

    }


    private void prepareUrl() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = takePartialScreenShot();
                        shareFile = Utils.createDirectoryAndSaveFile("org", ShareActivity.this, bitmap, false);
                        CropyApp.croppedBitmap = bitmap;
                        getToken(false);
                    }
                });


            }
        }).start();

    }

    private void prepareBitmap() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = takePartialScreenShot();
                        shareFile = Utils.createDirectoryAndSaveFile("org", ShareActivity.this, bitmap, false);
                        CropyApp.croppedBitmap = bitmap;
                        sharingListener.onBitmapPrepared(bitmap);


                        if (!isFinishing()) {
                            dialog.dismiss();
                        }

                        if (autoFinish) {
                            finish();
                        }

                    }
                });


            }
        }).start();

    }

    private void shareImage() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = takePartialScreenShot();
                        shareFile = Utils.createDirectoryAndSaveFile("org", ShareActivity.this, bitmap, false);
                        CropyApp.croppedBitmap = bitmap;
                        getToken(true);
                    }
                });


            }
        }).start();

    }


    Activity mActivity;

    public void setParentActivity(Activity activity) {

        mActivity = activity;

    }

    private void undo() {
        if (historyList.size() <= 1 && savedHistoryList.size() > 0) {

            if (!savedBitmapPaths.isEmpty()) {

                LogManager.addLog("Editing Screen - Undo applied - Rotate");

                Log.d("logHistoryPointer", "snapshot type: " + snapShots.get(snapShots.size() - 1).getActionType());
                Log.d("logHistoryPointer", "historyList type: " + historyList.get(historyList.size() - 1));


                Log.d("logHistoryPointer", "rotate");


                Bitmap previousBitmap = Utils.loadImageFromStorage(savedBitmapPaths.get(savedBitmapPaths.size() - 1));

                CropyApp.croppedBitmap = previousBitmap;
                CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();
                bit = previousBitmap;
                focusEnabled = false;
                capsActive = false;
                startCrop = true;
                editEnabled = true;
                snapShots = savedSnapShots.get(savedSnapShots.size() - 1);
                historyList = savedHistoryList.get(savedHistoryList.size() - 1);

                savedBitmapPaths.remove(savedBitmapPaths.size() - 1);
                savedSnapShots.remove(savedSnapShots.size() - 1);
                savedHistoryList.remove(savedHistoryList.size() - 1);

                onCreate(null);

                setCanvasSnapshot(canvasValues.get(canvasValues.size() - 1));
                canvasValues.remove(canvasValues.size() - 1);
            }


        } else {


            try {
                SnapShot removedSnapShot = null;
                if (historyList.get(historyList.size() - 1) == 1) {

                    LogManager.addLog("Editing Screen - Undo applied - Drawing");

                    Log.d("logHistoryPointer", "drawing");

                    historyList.remove(historyList.size() - 1);
                    snapShots.remove(snapShots.size() - 1);
                    canvas.undo();

                } else {

                    Log.d("logHistoryPointer", "snapshot type: " + snapShots.get(snapShots.size() - 1).getActionType());


                    historyList.remove(historyList.size() - 1);
                    removedSnapShot = snapShots.remove(snapShots.size() - 1);
                    canvas.setCurrentSnapShot(snapShots.get(snapShots.size() - 1));
                    canvas.setBitmapChanged(true);

//            Log.d("historyPointer", "removedSnapShot ActionType: " + removedSnapShot.getActionType() + " ,text: " + removedSnapShot.getText());


                    if (historyList.get(historyList.size() - 1) == 8 || historyList.get(historyList.size() - 1) == 9) {

                        Log.d("logHistoryPointer", "snapshot type: " + historyList.get(historyList.size() - 1));


                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
//                finishActivityAndStartCropActivity();


                    } else {
//                reDrawOtherBitmaps();

                        snapShotLog(snapShots.get(snapShots.size() - 1).getActionType());


                        if (removedSnapShot.getActionType() == 3)
                            etCapsPreview.setText("");

                        if (removedSnapShot.getActionType() == 2) {
                            canvas.setText("");
                            canvas.setTextList(new ArrayList<String>());
                        }


                        etText.setText(snapShots.get(snapShots.size() - 1).getText());
                        canvas.invalidate();

                        if (removedSnapShot.getActionType() == 7) {
                            canvas.setFocusEnabled(false);
                            cbvImage.setRadius(5000);
                            cbvImage.invalidate();
                            cbvImage.setVisibility(View.INVISIBLE);
                            onShapeChanged(null);

//                        shapes.resetBorders();
                        }

                    }
                }


                refreshUndoIcon();
                if (removedSnapShot != null)
                    setToolbarValues(removedSnapShot);
            } catch (Exception e) {
                Log.d("logCropy", "Error occurred on undo operation");
            }


        }
        refreshUndoIcon();
    }

    private void snapShotLog(int type) {
        String actionType = "";

        switch (type) {
            case 2:
                actionType = "Text";
                break;
            case 3:
                actionType = "Caps";
                break;
            case 4:
                actionType = "Frame";
                break;
            case 5:
                actionType = "Filter";
                break;
            case 6:
                actionType = "Adjustment";
                break;
            case 7:
                actionType = "Focus";
                break;
        }

        if (!actionType.isEmpty())
            LogManager.addLog("Editing Screen - Undo applied - " + actionType);

    }

    private void setToolbarValues(SnapShot removedSnapShot) {

        Log.d("logToolbar", "size: " + snapShots.size() + ", type: " + snapShots.get(snapShots.size() - 1).getActionType());

        if (removedSnapShot.getActionType() == 4)
            frames.resetValues();
        else if (removedSnapShot.getActionType() == 5)
            filters.resetValues();
        else if (removedSnapShot.getActionType() == 6)
            adjustment.resetValues();
        else if (removedSnapShot.getActionType() == 7)
            focus.resetValues();

    }

    Bitmap mosaicBitmap;

    public void reDrawOtherBitmaps() {


        Bitmap blurredBitmap = fastblur(canvas.cachedBitmap, 0.2f, 6);

        canvas.setBlurredBitmap(blurredBitmap);

        cbvImage.setBitmap(blurredBitmap, originalWidth, originalHeight, 0, (canvas.getHeight() - originalHeight) / 2);
        cbvImage.invalidate();

//        Bitmap mosaicBitmap = getMosaicsBitmap(Utils.scaleDownHeight(canvas.cachedBitmap, 600, false, false), 0.05f);
        Bitmap mosaicBitmap = getMosaicsBitmap(canvas.cachedBitmap, 0.05f);

        this.mosaicBitmap = mosaicBitmap;

        ivMosaic.setImageBitmap(mosaicBitmap);
        canvas.setMosaicBitmap(mosaicBitmap);

        Log.d("logMosaic", "w: " + canvas.getOriginalBitmap().getWidth() + ", h: " + canvas.getOriginalBitmap().getHeight());

    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    private boolean showTextOrCapsBar = true;

    boolean rendering = false;

    private Bitmap takePartialScreenShot() {

        rendering = true;

        final int h = canvas.getHeight();
        final int numberOfScroll = (int) (canvas.getBitmapHeight() / canvas.getHeight()) + 1;
        final int leapHeight = numberOfScroll > 1 ? (int) (canvas.getBitmapHeight() % canvas.getHeight()) : 0;


        final ArrayList<Bitmap> bitmaps = new ArrayList<>();


        try {

            Bitmap returnBitmap;

            if (numberOfScroll < 2) {

                canvas.mAttacher.onDrag(0, 0);


                Bitmap bitmap = takeScreenshot(rlCenter, ShareActivity.this);
                Bitmap leapBitmap = Bitmap.createBitmap(bitmap, 0, (int) (h - canvas.getBitmapHeight()) / 2, canvas.getWidth(), (int) canvas.getBitmapHeight());
                returnBitmap = leapBitmap;


            } else {

                int startY = cbvImage.getBitmapPadding();
                canvas.mAttacher.onDrag(0, startY);


                for (int i = 0; i < numberOfScroll; i++) {

                    Bitmap bitmap = takeScreenshot(rlCenter, ShareActivity.this);


                    if (numberOfScroll > 1 && i == numberOfScroll - 1) {
                        Bitmap leapBitmap = Bitmap.createBitmap(bitmap, 0, h - leapHeight, canvas.getWidth(), leapHeight);
                        bitmaps.add(leapBitmap);


                    } else {
                        bitmaps.add(bitmap);

                    }

                    canvas.mAttacher.onDrag(0, -(h));


                }

                returnBitmap = overlay(bitmaps, h * (numberOfScroll - 1) + leapHeight);

                canvas.mAttacher.onDrag(0, canvas.getBitmapHeight() - startY - h);


                Log.d("logSsBitmap", "returnBitmap: " + returnBitmap.getHeight());


            }

            rendering = false;
            return returnBitmap;


        } catch (Exception exception) {
            Log.d("logThread", "exception: " + exception);

        }

        return null;


    }

    public static Bitmap overlay(ArrayList<Bitmap> bitmaps, int totalHeight) {
        Bitmap bmOverlay = Bitmap.createBitmap(bitmaps.get(0).getWidth(), totalHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bmOverlay);
        int h = 0;
        for (Bitmap bitmap : bitmaps) {
            canvas.drawBitmap(bitmap, 0, h, null);
            h += bitmap.getHeight();
        }

        return bmOverlay;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_share_edit;
    }

    private int originalWidth = 0;
    private int originalHeight = 0;
    float scaleRatio = 1;

    private int keyboardHeight = 0;

    Cropy.ShareScenario shareScenario = Cropy.ShareScenario.Toolbar;

    private boolean autoFinish;


    private void calculateKeyboardHeight() {
        int navigationBarHeight = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        // status bar height
        int statusBarHeight = 0;
        resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        // display window size for the app layout
        Rect rect = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

        // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard


        if (!hasNavBar(ShareActivity.this)) {
            statusBarHeight = 0;
            navigationBarHeight = 0;


        } else {
            statusBarHeight = 0;
        }
        int keyboardHeight = rootLayout.getRootView().getHeight() - (statusBarHeight + navigationBarHeight + rect.height());
        if (keyboardHeight > ShareActivity.this.keyboardHeight)
            ShareActivity.this.keyboardHeight = keyboardHeight;

    }

    private void makeUiChanges(UiProperty uiProperty) {

        //activity changes
        rlTopBar.setBackgroundColor(uiProperty.getHeaderColor());
        tvTitle.setText(uiProperty.getHeaderTitle());
        tvTitle.setTextColor(uiProperty.getHeaderTextColor());
        rootLayout.setBackgroundColor(uiProperty.getBackgroundColor());
        gv.setColor(uiProperty.getBackgroundColor());
        rlshapes.setBackgroundColor(uiProperty.getFooterColor());

        //share toolbar changes
        gvShare.setBackgroundColor(uiProperty.getShareBackgroundColor());

        ivBack.setImageBitmap(Utils.stringToBitmap(uiProperty.getBackIcon()));

    }

    private void hidefeatures() {
        if (!getIntent().getExtras().getBoolean("crop", true)) {
            ivCrop.setVisibility(View.GONE);
        }
        if (!getIntent().getExtras().getBoolean("rotate", true)) {
            ivRotate.setVisibility(View.GONE);
        }
        if (!getIntent().getExtras().getBoolean("adjustment", true)) {
            shapes.hideItem(0);
        }
        if (!getIntent().getExtras().getBoolean("filter", true)) {
            shapes.hideItem(1);
        }
        if (!getIntent().getExtras().getBoolean("text", true)) {
            shapes.hideItem(2);
        }
        if (!getIntent().getExtras().getBoolean("arrow", true)) {
            shapes.hideItem(3);
        }
        if (!getIntent().getExtras().getBoolean("line", true)) {
            shapes.hideItem(4);
        }
        if (!getIntent().getExtras().getBoolean("pen", true)) {
            shapes.hideItem(5);
        }
        if (!getIntent().getExtras().getBoolean("rect", true)) {
            shapes.hideItem(6);
        }
        if (!getIntent().getExtras().getBoolean("circle", true)) {
            shapes.hideItem(7);
        }
        if (!getIntent().getExtras().getBoolean("frame", true)) {
            shapes.hideItem(8);
        }
        if (!getIntent().getExtras().getBoolean("pixelate", true)) {
            shapes.hideItem(9);
        }
        if (!getIntent().getExtras().getBoolean("focus", true)) {
            shapes.hideItem(10);
        }
        if (!getIntent().getExtras().getBoolean("caps", true)) {
            shapes.hideItem(11);
        }
    }

    UiProperty uiProperty;

    SharingListener sharingListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        CropyPESDK.init(getApplication());

        super.onCreate(savedInstanceState);

        webPageTitle = getIntent().getExtras().getString("webPageTitle", "");
        webUrl = getIntent().getExtras().getString("webUrl", "");

        initViews();

        registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));


        if (getIntent().hasExtra("uiProperty"))
            uiProperty = getIntent().getParcelableExtra("uiProperty");
        else {
            uiProperty = new UiProperty(ShareActivity.this);
        }

        if (getIntent().hasExtra("autoFinish"))
            autoFinish = getIntent().getBooleanExtra("autoFinish", true);

        if (getIntent().hasExtra("shareScenario"))
            shareScenario = (Cropy.ShareScenario) getIntent().getSerializableExtra("shareScenario");

        if (shareScenario != Cropy.ShareScenario.Toolbar) {
            ivShare.setImageBitmap(Utils.stringToBitmap(uiProperty.getDoneIcon()));
        } else {
            ivShare.setImageBitmap(Utils.stringToBitmap(uiProperty.getShareIcon()));
        }


        hidefeatures();


        sharingListener = (SharingListener) activity;


        makeUiChanges(uiProperty);

        cbvImage.setAlpha(0f);


        rlCenterRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


                Log.d("onGlobalLayout", "2");


//                rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                Rect r = new Rect();
                rootLayout.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootLayout.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;


                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    Log.d("logFocus", "opened");
                    if (currentDrawer == CanvasView.Drawer.TEXT) {
                        canvas.setTextTouch(true);
                        canvas.setTempTouchY(canvas.getStartY());

                    }

                    calculateKeyboardHeight();
                    onShowKeyboard(keyboardHeight);


                } else {
                    // keyboard is closed
                    Log.d("logFocus", "closed");
                    llCapsPreview.setVisibility(View.GONE);
                    rlTextStyle.setVisibility(View.GONE);

                    canvas.setTextTouch(false);
                    canvas.setTempTouchY(-1);

                    if (currentDrawer == CanvasView.Drawer.CAPS) {
                        Log.d("onCapsTextChanged", "onCapsTextChanged 1");
                        onCapsTextChanged();
                    }

                    onHideKeyboard();

                    //TODO can be needed to add caps
//                    canvas.switchToZoom();
//                    canvas.setDrawMode(false);


                }

            }

        });


        if (!isFinishing()) {

            dialog = new ProgressDialog(this);
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setCancelable(false);
            Log.d("logShare", "1");
        }


        ViewTreeObserver vto = rlCenterRoot.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                rlCenterRoot.getViewTreeObserver().removeGlobalOnLayoutListener(this);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        final float width = rlCenterRoot.getMeasuredWidth();
                        float height = rlCenterRoot.getMeasuredHeight();

                        if (bit == null)
                            bit = CropyApp.croppedBitmap;

                        if (bit.getWidth() > Utils.getScreenWidth(ShareActivity.this)) {
//                            bit = Utils.fitWidthToScreen(CropyApp.croppedBitmap, Utils.getScreenWidth(ShareActivity.this));
                            Log.d("croppedBitmap", "before width: " + bit.getWidth() + ", height: " + bit.getHeight());
                            bit = Utils.scaleDown(CropyApp.croppedBitmap, Utils.getScreenWidth(ShareActivity.this) / 2, true, true);
                        }

//                bit = Utils.scaleDown(CropyApp.croppedBitmap, height, true, true);

                        Log.d("croppedBitmap", "after width: " + bit.getWidth() + ", height: " + bit.getHeight());

                        canvas.setAlpha(0f);
                        canvas.setImageBitmap(bit);

                        Log.d("setImageBitmap", "setImageBitmap called");


                        focusBitmap = bit;

                        float cons = 1f;
//                        if (scaleRatio > 1)
                        scaleRatio = width / bit.getWidth() * cons;

                        canvas.mAttacher.onScale(scaleRatio, 0, 0);
                        canvas.setAlpha(1f);
                        canvas.mAttacher.zoomEnabled = false;
//                        canvas.mAttacher.setZoomable(false);

                        originalWidth = (int) (bit.getWidth() * scaleRatio);
                        originalHeight = (int) (bit.getHeight() * scaleRatio);


                        canvas.setBitmapScale(scaleRatio);
                        canvas.setBitmapX((canvas.getWidth() - originalWidth) / 2);

                        Log.d("logBitmapDimensions", "canvas.getHeight(): " + canvas.getHeight() + ", originalHeight: " + originalHeight);

                        canvas.setBitmapY(originalHeight < canvas.getHeight() ? (canvas.getHeight() - originalHeight) / 2 : 0);

                        Log.d("logBitmapDimensions", "canvas.getBitmapY(): " + canvas.getBitmapY());

                        canvas.setBitmapWidth(originalWidth);
                        canvas.setBitmapHeight(originalHeight);
                        canvas.setOriginalBitmap(bit);
                        canvas.setCachedBitmap(bit);

                        if (originalHeight < canvas.getHeight()) {
                            gv.setLocation(canvas.getWidth(), canvas.getHeight(), 0, (int) canvas.getBitmapY(), canvas.getWidth(), originalHeight);
                            gv.invalidate();
                        }

                        int padding = 0;
                        if (canvas.getBitmapY() > 0)
                            padding = (int) canvas.getBitmapY();
                        svMosaic.setPadding(0, padding, 0, 0);


                        Log.d("logSizes", "image h: " + originalHeight + ", screen h: " + canvas.getHeight());


//                        Bitmap blurredBitmap = BlurBuilder.blur( ShareActivity.this, bit );


//                        cbvImage.setBitmap(fastblur(bit, 0.2f, 6), originalWidth, originalHeight, 0, (canvas.getHeight() - originalHeight) / 2);
//                        ivMosaic.setImageBitmap(getMosaicsBitmap(bit, 0.05));


                        if (fromRotate) {
//                            SnapShot newSnapShot = new SnapShot();
//                            snapShots.add(newSnapShot);
//                            historyList.add(8);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    selectItem(type);
                                    fromRotate = false;

                                }
                            }, 300);

                        }

//        SnapShot newSnapShot = new SnapShot();
//        snapShots.add(newSnapShot);
//        historyList.add(0);

//                        if (!restart)
//                            prepareImage();
//                        else {
//                            dumbPrepare();
//                        }

                        prepareImage();


//                        refreshUndoIcon();

                    }
                }, 200);


            }
        });


        canvas.setBaseColor(Color.parseColor("#00000000"));
        canvas.setPaintStrokeColor(Color.parseColor("#FFD4001E"));

        canvas.setPaintStrokeWidth(Utils.dpToPx(this, 3f));
        canvas.setDrawer(CanvasView.Drawer.PEN);


        new Thread(new Runnable() {
            @Override
            public void run() {
                shareFile = Utils.createDirectoryAndSaveFile("org", ShareActivity.this, bit, false);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!isFinishing()) {
                            dialog.dismiss();

                        }

                    }
                });

            }
        }).start();

    }

    boolean restart = false;


    private void prepareImage() {


        if (!historyList.contains(0)) {

        }

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }


        if (!snapShots.isEmpty()) {
            if (snapShots.get(snapShots.size() - 1).getActionType() == -1) {
                snapShots.remove(snapShots.size() - 1);
                historyList.remove(historyList.size() - 1);
            }
        }


        historyList.add(0);
        snapShots.add(newSnapShot);

        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

//        if (restart)
//            ivUndo.performClick();

        restart = false;
    }

    private void dumbPrepare() {


//        SnapShot newSnapShot = new SnapShot();
//        if (!snapShots.isEmpty()) {
//            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));
//
//        } else {
//            newSnapShot = new SnapShot();
//        }
//
//        if (!restart) {
//        }
//
//        historyList.add(0);
//        snapShots.add(newSnapShot);
//
//        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

//        if (restart)
//            ivUndo.performClick();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {

            ((BitmapDrawable) canvas.getDrawable()).getBitmap().recycle();
            canvas.setImageBitmap(null);

            bit.recycle();
            bit = null;
            focusBitmap.recycle();
            focusBitmap = null;
            canvas.recycleBitmaps();
            Runtime.getRuntime().gc();
        } catch (Exception e) {

        }
    }

    private void prepareshareIntent() {


        ivShare.setEnabled(true);

//        ivShare.setVisibility(View.GONE);
//        ivShare.setAlpha(0f);

        if (gvShare.getVisibility() == View.VISIBLE) {
            gvShare.setVisibility(View.GONE);
        } else {

            gvShare.setVisibility(View.VISIBLE);


            tvTitle.setText(uiProperty.getShareHeaderTitle());
            rlTopBar.setBackgroundColor(uiProperty.getShareHeaderColor());

            ArrayList<ShareItem> shareItems = new ArrayList<>();
            shareItems.add(new ShareItem("BiP", R.drawable.paylas_bip));
            shareItems.add(new ShareItem("WhatsApp", R.drawable.paylas_whatsapp));
            shareItems.add(new ShareItem(getResources().getString(R.string.email), R.drawable.paylas_mail));
            shareItems.add(new ShareItem("Facebook", R.drawable.paylas_fb));
            shareItems.add(new ShareItem("Twitter", R.drawable.paylas_twitter));
            shareItems.add(new ShareItem("Instagram", R.drawable.paylas_instagram));
            shareItems.add(new ShareItem("lifebox", R.drawable.paylas_akilli_depo));
            shareItems.add(new ShareItem(getResources().getString(R.string.save), R.drawable.paylas_kaydet));
            shareItems.add(new ShareItem(getResources().getString(R.string.others), R.drawable.paylas_diger));
            gvShare.setAdapter(new ShareAdapter(ShareActivity.this, shareItems, webPageTitle, shareUrl, webUrl));

        }

        showTextOrCapsBar = true;
    }

    String token = "";

    private void getToken(final boolean showToolbar) {

        LogManager.addLog("Editing Screen - getToken request sent");


        restApi = setupRestClient(null);

        restApi.getToken(new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                LogManager.addLog("Editing Screen - getToken response came - Success");

                token = s;
                saveCropped(s, showToolbar);


            }

            @Override
            public void failure(RetrofitError error) {

                LogManager.addLog("getToken response came - Failure: " + error.getMessage());

                if (Utils.connected(ShareActivity.this)) {
                    showCustomDialog(R.drawable.ic_dialog_error, getString(R.string.error), getResources().getString(R.string.error_occured));
//                    Toast.makeText(ShareActivity.this, getResources().getString(R.string.error_occured), Toast.LENGTH_SHORT).show();

                } else {
                    showCustomDialog(R.drawable.ic_dialog_error, getString(R.string.error), getResources().getString(R.string.check_connection));
//                    Toast.makeText(ShareActivity.this, getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
                }


                if (!isFinishing()) {
                    dialog.dismiss();
                }

            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (timer != null)
            timer.cancel();
    }

    private void shareRequest(String shareType) {

        RestApi restApi = setupRestClientForShareRequest(token);

        restApi.share(Utils.getVersion(ShareActivity.this), shareType, Cropy.getSessionId(), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void saveCropped(final String token, final boolean showToolbar) {


        LogManager.addLog("Editing Screen - saveCropped request sent");


        restApi = setupRestClient(token);

        final File org = shareFile;
        final File thumb = createThumb(org);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmapOrg = BitmapFactory.decodeFile(thumb.getPath(), options);
        Bitmap bitmapThumb = BitmapFactory.decodeFile(thumb.getPath(), options);

        final int imageWidth = bitmapThumb.getWidth();
        final int imageHeight = bitmapThumb.getHeight();


        TypedFile orgTyped = new TypedFile("image/jpeg", shareFile);
        TypedFile thumbTyped = new TypedFile("image/jpeg", thumb);


        timer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (!isFinishing()) {
                    dialog.dismiss();

                }

                Utils.deleteTempFile(thumb.getPath(), ShareActivity.this, false);
                Utils.deleteTempFile(shareFile.getPath(), ShareActivity.this, false);

                shareUrl = "";

                if (showToolbar)
                    prepareshareIntent();
                else {
                    sharingListener.onUrlPrepared(shareUrl);
                    finish();
                }

            }

        };

        timer.start();


        String lang = "tr";

        if (Locale.getDefault().getDisplayLanguage().equals("English"))
            lang = "en";

//        restApi.saveCropped("cropyApp", webPageTitle, webPageTitle, webUrl, webUrl.equals("") ? "photo" : "web", "1", orgTyped, thumbTyped, imageWidth, imageHeight, new Callback<Response>() {
        restApi.saveCropped("cropyApp", webPageTitle, webPageTitle, webUrl, lang, webUrl.equals("") ? "photo" : "web", "1", orgTyped, thumbTyped, imageWidth, imageHeight, new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {

                LogManager.addLog("Editing Screen - saveCropped response came - Success");


                if (timer != null)
                    timer.cancel();

                Log.d("logSaveFile", "success: " + System.currentTimeMillis());
                if (!isFinishing()) {
                    dialog.dismiss();

                }

                Utils.deleteTempFile(thumb.getPath(), ShareActivity.this, false);
                Utils.deleteTempFile(shareFile.getPath(), ShareActivity.this, false);


                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {

                    reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

                    String line;

                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String result = sb.toString();

                Log.d("logRetro", "result: " + result);
                shareUrl = result;

                if (showToolbar)
                    prepareshareIntent();
                else {
                    sharingListener.onUrlPrepared(shareUrl);
                    finish();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                LogManager.addLog("saveCropped response came - Failure: " + error.getMessage());


                if (timer != null)
                    timer.cancel();

                Log.d("logShare", "2 failure");

                Log.d("logRetro", "error: " + error.getMessage());
                Utils.deleteTempFile(thumb.getPath(), ShareActivity.this, false);


                if (!isFinishing()) {
                    dialog.dismiss();

                }
                ivShare.performClick();


            }
        });
    }

    private File createThumb(File file) {


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);

        int w = bitmap.getWidth() < 600 ? 600 : 1200;
        int h = bitmap.getWidth() < 600 ? 315 : 630;


        if (h == 315)
            bitmap = Utils.scaleDown(bitmap, 315, false, true);
        else
            bitmap = Utils.scaleDown(bitmap, 630, false, true);


        Bitmap bmOverlay = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);

        Paint textPaint = new Paint();
        textPaint.setARGB(255, 255, 255, 255);
        textPaint.setTextAlign(Paint.Align.RIGHT);
        textPaint.setTextSize(11);
        textPaint.setTypeface(Typeface.DEFAULT);

        Canvas canvas = new Canvas(bmOverlay);
//        canvas.drawRect(0, 0, 0, 0, textPaint);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, (w / 2) - (bitmap.getWidth() / 2), (h / 2) - (bitmap.getHeight() / 2), null);

        String mPath = Environment.getExternalStorageDirectory() + "/thumb";
        File imageFile = new File(mPath);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(imageFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bmOverlay.compress(Bitmap.CompressFormat.JPEG, 99, outputStream);

        return imageFile;
    }

    private Bitmap takeScreenshot(View v, Activity activity) {


        try {
            View v1 = activity.getWindow().getDecorView().getRootView();
            if (v != null)
                v1 = v;

            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);


            return bitmap;

        } catch (Throwable e) {
            e.printStackTrace();

            return null;
        }

    }

    private RestApi setupRestClient(final String token) {


        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("apikey", Cropy.apiKey);
                request.addHeader("origin", Cropy.origin);
                request.addHeader("session-id", Cropy.getSessionId());

                if (token != null) {

                    request.addHeader("cropsharetoken", token);

                    String editType = "";
                    if (photoEditEventSentCount > 1) {
                        editType = webUrl.equals("") ? "photo" : "web";
                        request.addHeader("editType", editType);
                    }
                }
            }
        };

        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(Utils.getBaseUrlShare());
        builder.setRequestInterceptor(requestInterceptor);
        builder.setClient(new OkClient(new OkHttpClient()));
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        RestApi restApi = restAdapter.create(RestApi.class);
        return restApi;
    }

    private RestApi setupRestClientForShareRequest(final String token) {


        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("apikey", Cropy.apiKey);
                request.addHeader("origin", Cropy.origin);

                if (token != null)
                    request.addHeader("cropsharetoken", token);
            }
        };


        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(Utils.getBaseUrlCropyHttps());
        builder.setRequestInterceptor(requestInterceptor);
        builder.setClient(new OkClient(new OkHttpClient()));
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        RestApi restApi = restAdapter.create(RestApi.class);
        return restApi;
    }


    @Override
    public void onColorchanged(int color) {
        canvas.setPaintStrokeColor(color);
        canvas.invalidate();
    }

    @Override
    public void onSizeChanged(float size) {
        canvas.setPaintStrokeWidth(size);
        canvas.invalidate();

    }

    @Override
    public void onShapeChanged(CanvasView.Drawer drawer) {

        currentDrawer = drawer;

        canvas.setFocusDrawModeEnabled(false);

        if (drawer == CanvasView.Drawer.ROTATE) {
            ivRotate.performClick();
            return;
        }

        canvas.setVisibility(View.VISIBLE);

        cbvImage.setVisibility(View.INVISIBLE);

        rlColors.setVisibility(View.INVISIBLE);
        rlFrames.setVisibility(View.INVISIBLE);
        rlFilters.setVisibility(View.INVISIBLE);
        rlAdjustment.setVisibility(View.INVISIBLE);
        rlFocus.setVisibility(View.INVISIBLE);
        rlTextStyle.setVisibility(View.INVISIBLE);
        viewTextDivider.setVisibility(View.INVISIBLE);

        cbvImage.setEnabled(false);
        cbvImage.setDrawModeEnabled(false);


        if (!focusTouched) {
            cbvImage.setDrawModeEnabled(false);
            canvas.setFocusDrawModeEnabled(false);
            canvas.setFocusEnabled(false);


            cbvImage.invalidate();
            canvas.invalidate();
        }


        if (drawer == CanvasView.Drawer.FRAME || drawer == CanvasView.Drawer.FILTER || drawer == CanvasView.Drawer.ADJUSTMENT || drawer == CanvasView.Drawer.FOCUS) {
            canvas.setDrawMode(false);
        } else {
            canvas.setDrawMode(true);
        }


        if (drawer == null) {
            canvas.switchToZoom();
            canvas.setDrawMode(false);


        } else {

            if (drawer == CanvasView.Drawer.TEXT) {
                viewTextDivider.setVisibility(View.VISIBLE);
                rlColors.setVisibility(View.VISIBLE);

                canvas.setMode(CanvasView.Mode.TEXT);
//                canvas.setText("");
                canvas.setFontSize(Utils.dpToPx(this, 50));

                etText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        canvas.setTyping(true);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        canvas.setText("" + s.toString());


                        String multiLines = etText.getText().toString();
                        String[] streets;
                        String delimiter = "\n";

                        streets = multiLines.split(delimiter);

                        canvas.textList.clear();
                        for (int i = 0; i < streets.length; i++) {
                            canvas.textList.add(streets[i]);
                            Log.d("logTextt", "textt: " + streets[i]);
                        }

                        canvas.invalidate();

                    }
                });

            } else {
                rlColors.setVisibility(View.VISIBLE);
                canvas.setMode(CanvasView.Mode.DRAW);
//                if (drawer != CanvasView.Drawer.PIXELATE) {
//                    canvas.setDrawer(drawer);
//                    canvas.invalidate();
//                }

                canvas.setDrawer(drawer);
                canvas.invalidate();

                cbvImage.setDrawModeEnabled(false);

            }

            if (drawer == CanvasView.Drawer.FRAME) {
                rlFrames.setVisibility(View.VISIBLE);
                canvas.switchToZoom();
                canvas.setDrawMode(false);
            } else if (drawer == CanvasView.Drawer.FILTER) {
                rlFilters.setVisibility(View.VISIBLE);
                canvas.switchToZoom();
                canvas.setDrawMode(false);
            } else if (drawer == CanvasView.Drawer.PIXELATE) {
                rlColors.setVisibility(View.INVISIBLE);


                if (!mosaicBitmapCreated || bitmapChanged) {

                    try {
                        reDrawOtherBitmaps();
                    } catch (Exception e) {

                    }

                    int padding = 0;
                    if (canvas.getBitmapY() > 0)
                        padding = (int) canvas.getBitmapY();
                    svMosaic.setPadding(0, padding, 0, 0);
                    mosaicBitmapCreated = true;
                    bitmapChanged = false;

                }

                canvas.setMode(CanvasView.Mode.ERASER);
                canvas.setDrawer(CanvasView.Drawer.PEN);

            } else if (drawer == CanvasView.Drawer.ADJUSTMENT) {
                rlAdjustment.setVisibility(View.VISIBLE);
                canvas.switchToZoom();
                canvas.setDrawMode(false);

            } else if (drawer == CanvasView.Drawer.FOCUS) {

                focusTouched = false;

                canvas.setFocusDrawModeEnabled(true);


                cbvImage.setmCx(-1);
                cbvImage.setmCy(-1);


                cbvImage.setEnabled(true);

                canvas.switchToEdit();
                canvas.setDrawMode(true);

                canvas.setFocusEnabled(true);

                if (!blurredBitmapCreated || bitmapChanged) {
                    try {
                        reDrawOtherBitmaps();
                    } catch (Exception e) {

                    }
                    focusEnabled = true;
                    blurredBitmapCreated = true;
                    bitmapChanged = false;
                }

                rlFocus.setVisibility(View.VISIBLE);
                cbvImage.setVisibility(View.VISIBLE);

                cbvImage.setDrawModeEnabled(true);
                cbvImage.setRadius(150);
                cbvImage.setmScaleFactor(1);

                cbvImage.invalidate();

                canvas.invalidate();

            } else if (drawer == CanvasView.Drawer.CAPS) {
                enableCaps();

            } else if (drawer == CanvasView.Drawer.TEXT) {
                capsActive = false;


                rlTextStyle.setVisibility(View.VISIBLE);
            }

            if (drawer != CanvasView.Drawer.FRAME && drawer != CanvasView.Drawer.FILTER && drawer != CanvasView.Drawer.ADJUSTMENT)
                canvas.switchToEdit();

        }

    }


    private void enableCaps() {
        capsActive = true;

        rlColors.setVisibility(View.INVISIBLE);
        etCapsPreview.setVisibility(View.VISIBLE);

        etCapsPreview.setFocusableInTouchMode(true);
        etCapsPreview.requestFocus();

        final InputMethodManager inputMethodManager = (InputMethodManager) ShareActivity.this
                .getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(etCapsPreview, InputMethodManager.SHOW_FORCED);
        canvas.invalidate();

        etCapsPreview.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onCapsTextChanged();
                }
                return false;
            }

        });


        etCapsPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCapsPreview.setHint("");
            }
        });

        etCapsPreview.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().isEmpty()) {
                    etCapsPreview.setHint(getResources().getString(R.string.enter_text));
                } else {
                    etCapsPreview.setHint("");
                }

                etCapsPreview.setVisibility(View.VISIBLE);
                canvas.setCapsText("" + s.toString());

                canvas.capsList.clear();

                for (int i = 0; i < etCapsPreview.getLineCount(); i++) {
                    int startPos = etCapsPreview.getLayout().getLineStart(i);
                    int endPos = etCapsPreview.getLayout().getLineEnd(i);
                    String theLine = etCapsPreview.getText().toString().substring(startPos, endPos);
                    canvas.capsList.add(theLine);
                }

                canvas.invalidate();

            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    boolean blurredBitmapCreated = false;
    boolean mosaicBitmapCreated = false;
    boolean bitmapChanged = true;


    @Override
    public void onCropped(final Bitmap bitmap) {
        Log.d("logCrop", "1 cropped succesfully!");

        LogManager.addLog("Editing Screen - Feature applied - Crop");

        savedSnapShots.add(snapShots);
        savedHistoryList.add(historyList);
        canvasValues.add(getCanvasSnapshot());

        bit = bitmap;
        focusEnabled = false;
        capsActive = false;
        startCrop = true;
        editEnabled = true;
        snapShots = new ArrayList();
        historyList = new ArrayList();
        restart = true;
        onCreate(null);

        ivUndo.setClickable(false);

        new Thread(new Runnable() {
            @Override
            public void run() {

                savedBitmapPaths.add(Utils.saveToInternalStorage(ShareActivity.this, CropyApp.croppedBitmap));

                ivUndo.setClickable(true);

                CropyApp.croppedBitmap = bitmap;
                CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();
            }
        }).start();

    }

    ArrayList<File> savedBitmapPaths = new ArrayList<>();
    ArrayList<ArrayList<SnapShot>> savedSnapShots = new ArrayList<>();
    ArrayList<ArrayList<Integer>> savedHistoryList = new ArrayList<>();
    ArrayList<CanvasValues> canvasValues = new ArrayList<>();


    @Override
    public void onRotated(final Bitmap bitmap, final int type) {
        Log.d("logCrop", "1 rotated succesfully!");

        LogManager.addLog("Editing Screen - Feature applied - Rotate");

        savedSnapShots.add(snapShots);
        savedHistoryList.add(historyList);
        canvasValues.add(getCanvasSnapshot());

        fromRotate = true;
        this.type = type;

        bit = bitmap;
        focusEnabled = false;
        capsActive = false;
        startCrop = true;
        editEnabled = true;
        snapShots = new ArrayList();
        historyList = new ArrayList();
        restart = true;
        onCreate(null);


        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                ivUndo.setClickable(false);

            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {

                savedBitmapPaths.add(Utils.saveToInternalStorage(ShareActivity.this, CropyApp.croppedBitmap));

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            ivUndo.setClickable(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                CropyApp.croppedBitmap = bitmap;
                CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();
            }
        }).start();

//        selectItem(type);

    }

    private CanvasValues getCanvasSnapshot() {
        CanvasValues values = new CanvasValues();
        values.setMode(canvas.getMode());
        values.setDrawer(canvas.getDrawer());

        values.setPathLists(canvas.getPathLists());
        values.setPaintLists(canvas.getPaintLists());
        values.setRegionList(canvas.getRegionList());

        values.setCapsList(canvas.getCapsList());
        values.setTextList(canvas.getTextList());

        values.setText(canvas.getText());
        values.setTextX(canvas.getTextX());
        values.setTextY(canvas.getTextY());

        values.setCapsText(canvas.getCapsText());
        values.setCapsSize(canvas.getCapsSize());
        values.setHistoryPointer(canvas.getHistoryPointer());

        return values;
    }

    private CanvasValues setCanvasSnapshot(CanvasValues values) {
        canvas.setMode(values.getMode());
        canvas.setDrawer(values.getDrawer());

        canvas.setPathLists(values.getPathLists());
        canvas.setPaintLists(values.getPaintLists());
        canvas.setRegionList(values.getRegionList());

        canvas.setCapsList(values.getCapsList());
        canvas.setTextList(values.getTextList());

        canvas.setText(values.getText());
        canvas.setTextX(values.getTextX());
        canvas.setTextY(values.getTextY());

        canvas.setCapsText(values.getCapsText());
        canvas.setCapsSize(values.getCapsSize());
        canvas.setHistoryPointer(values.getHistoryPointer());

        canvas.invalidate();


        return values;
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    private float touchY = -1;

    @Override
    public void onTextTouch(final float touchY) {


        canvas.setTextTouch(true);
        canvas.setTempTouchY(canvas.getStartY());


        this.touchY = touchY;
        etText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(etText, InputMethodManager.SHOW_IMPLICIT);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                int limit = canvas.getHeight() + rlshapes.getHeight() - keyboardHeight - vTextStyle.getHeight();
                Log.d("onTextTouch", "touchY: " + touchY + ", keyboard height: " + limit + ", startY: " + canvas.getStartY());

                if (limit < touchY) {
                    ShareActivity.this.touchY = limit + diff - canvas.getBitmapY();
                    canvas.setStartY(limit + diff - canvas.getBitmapY());
                    canvas.invalidate();
                }


            }
        }, 500);


//        canvas.invalidate();


    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onFrameChanged(int resId) {

        LogManager.addLog("Editing Screen - Feature applied - Frame");

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }
        newSnapShot.setFrameId(resId);
        newSnapShot.setActionType(4);

//        for (int i = 0; i < snapShots.size(); i++) {
//            if (snapShots.get(i).getActionType() == 4) {
//                snapShots.remove(i);
//                historyList.remove(i);
//            }
//        }
//        for (int i = 0; i < snapShots.size(); i++) {
//            snapShots.get(i).setFrameId(0);
//        }

        if (snapShots.get(snapShots.size() - 1).getActionType() == 4) {
            snapShots.remove(snapShots.size() - 1);
            historyList.remove(historyList.size() - 1);
        }

        historyList.add(4);


//        snapShots.get(snapShots.size() - 1).setFrameId(0);
        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
//        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

//        reDrawOtherBitmaps();


    }

    @Override
    public void onFilterChanged(int resId) {

        LogManager.addLog("Editing Screen - Feature applied - Filter");

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }
        newSnapShot.setFilterId(resId);
        newSnapShot.setActionType(5);

//        for (int i = 0; i < snapShots.size(); i++) {
//
//            if (snapShots.get(i).getActionType() == 5) {
//                snapShots.remove(i);
//                historyList.remove(i);
//            }
//
//        }
//        for (int i = 0; i < snapShots.size(); i++) {
//            snapShots.get(i).setFilterId(0);
//        }

        if (snapShots.get(snapShots.size() - 1).getActionType() == 5) {
            snapShots.remove(snapShots.size() - 1);
            historyList.remove(historyList.size() - 1);
        }


        historyList.add(5);

//        snapShots.get(snapShots.size() - 1).setFilterId(0);
        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

//        reDrawOtherBitmaps();


        Log.d("historyPointer", "onFilterChanged historyPointer: " + historyList.get(historyList.size() - 1));


    }

    @Override
    public void onAdjustmentChanged(float brightness, float contrast) {

        LogManager.addLog("Editing Screen - Feature applied - Adjustment");

        Log.d("logStackSize", "onAdjustmentChanged snapShotStack size: " + snapShots.size());

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }

        newSnapShot.setBrightnessValue(brightness);
        newSnapShot.setContrastValue(contrast);
        newSnapShot.setActionType(6);

//        for (int i = 0; i < snapShots.size(); i++) {
//            if (snapShots.get(i).getActionType() == 6) {
//                snapShots.remove(i);
//                historyList.remove(i);
//            }
//        }
//        for (int i = 0; i < snapShots.size(); i++) {
//            snapShots.get(i).setBrightnessValue(0);
//            snapShots.get(i).setContrastValue(1);
//
//        }

        if (snapShots.get(snapShots.size() - 1).getActionType() == 6) {
            snapShots.remove(snapShots.size() - 1);
            historyList.remove(historyList.size() - 1);
        }


        historyList.add(6);

//        snapShots.get(snapShots.size() - 1).setBrightnessValue(0);
//        snapShots.get(snapShots.size() - 1).setContrastValue(1);

        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

        Log.d("logStackSize", "2 onAdjustmentChanged snapShotStack size: " + snapShots.size());


    }

    @Override
    public void onFocusLocationChanged(float x, float y, float radius, int alpha) {


        canvas.setCircleX(x);
        canvas.setCircleY(y);
        canvas.setCircleRadius(radius);
        canvas.setCircleBitmapAlpha(alpha);
        canvas.invalidate();

//        cbvImage.setmCx(x);
//        cbvImage.setmCy(y);
//        cbvImage.setRadius(radius);
//        cbvImage.setBitmapAlpha(alpha);

    }

    @Override
    public void onFocusRadiusChanged(float value) {
        Log.d("logCircleInıt", "onFocusRadiusChanged y: " + value);


        cbvImage.setBitmapAlpha((int) (value));
        cbvImage.invalidate();

        canvas.invalidate();
    }


    private void dumbEvent(int radius) {
        focusTouched = true;
        Log.d("logFocus", "onFocusChanged focusTouched: " + focusTouched);


        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }

        if (radius == -1)
            newSnapShot.setFocusRadius((int) cbvImage.getRadius());
        else
            newSnapShot.setFocusRadius((int) radius);

        newSnapShot.setActionType(7);

        for (int i = 0; i < snapShots.size(); i++) {

            if (snapShots.get(i).getActionType() == 7) {
                snapShots.remove(i);
                historyList.remove(i);
            }

        }

        for (int i = 0; i < snapShots.size(); i++) {
            snapShots.get(i).setFocusRadius(5000);

        }

        historyList.add(7);

//        snapShots.get(snapShots.size() - 1).setFocusRadius(5000);

        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        if (radius == -1)
            cbvImage.setRadius(cbvImage.getRadius());
        else
            cbvImage.setRadius(radius);
        cbvImage.invalidate();
        cbvImage.setVisibility(View.VISIBLE);
        refreshUndoIcon();

        canvas.invalidate();
    }

    private boolean focusTouched = false;

    @Override
    public void onFocusChanged(float radius) {
        focusTouched = true;
        Log.d("logFocus", "onFocusChanged focusTouched: " + focusTouched);

        LogManager.addLog("Editing Screen - Feature applied - Focus");

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }

        if (radius == -1)
            newSnapShot.setFocusRadius((int) cbvImage.getRadius());
        else
            newSnapShot.setFocusRadius((int) radius);

        newSnapShot.setActionType(7);

        for (int i = 0; i < snapShots.size(); i++) {

            if (snapShots.get(i).getActionType() == 7) {
                snapShots.remove(i);
                historyList.remove(i);
            }

        }

        for (int i = 0; i < snapShots.size(); i++) {
            snapShots.get(i).setFocusRadius(5000);

        }

        historyList.add(7);

//        snapShots.get(snapShots.size() - 1).setFocusRadius(5000);

        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        if (radius == -1)
            cbvImage.setRadius(cbvImage.getRadius());
        else
            cbvImage.setRadius(radius);
        cbvImage.invalidate();
        cbvImage.setVisibility(View.VISIBLE);
        refreshUndoIcon();

        canvas.invalidate();


    }


    private void onCapsTextChanged() {

        LogManager.addLog("Editing Screen - Feature applied - Caps");

        canvas.mAttacher.onDrag(0, -bit.getHeight());

        Log.d("onCapsTextChangedd", "bit.getHeight(): " + bit.getHeight());

        onShapeChanged(null);

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }
        newSnapShot.setCapsText(etCapsPreview.getText().toString());
        newSnapShot.setActionType(3);

        for (int i = 0; i < snapShots.size(); i++) {

            if (snapShots.get(i).getActionType() == 3) {
                snapShots.remove(i);
                historyList.remove(i);
            }

        }

        for (int i = 0; i < snapShots.size(); i++) {
            snapShots.get(i).setCapsText("");
        }


        historyList.add(3);

//        snapShots.get(snapShots.size() - 1).setFilterId(0);
        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        canvas.invalidate();
        refreshUndoIcon();

    }

    private void onTextChanged() {

        LogManager.addLog("Editing Screen - Feature applied - Text");

//        onShapeChanged(null);

        SnapShot newSnapShot = new SnapShot();
        if (!snapShots.isEmpty()) {
            newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

        } else {
            newSnapShot = new SnapShot();
        }
        newSnapShot.setText(etText.getText().toString());
//        newSnapShot.setTextList(canvas.getTextList());
        newSnapShot.setActionType(2);

        for (int i = 0; i < snapShots.size(); i++) {

            if (snapShots.get(i).getActionType() == 2) {
                snapShots.remove(i);
                historyList.remove(i);
            }

        }

        for (int i = 0; i < snapShots.size(); i++) {
            snapShots.get(i).setText("");
        }


        historyList.add(2);

//        snapShots.get(snapShots.size() - 1).setFilterId(0);
        snapShots.add(newSnapShot);
        canvas.setCurrentSnapShot(newSnapShot);
        canvas.setBitmapChanged(true);
        Log.d("logText", "onTextChanged " + snapShots.get(snapShots.size() - 1).getText());

        canvas.invalidate();
        refreshUndoIcon();

    }

    @Override
    public void onTextStyleChanged(int resId) {

        if (resId == 0)
            bold = !bold;
        else if (resId == 1)
            italic = !italic;
        else if (resId == 2)
            underlined = !underlined;

        if (bold && italic)
            canvas.setFontFamily(Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC));
        else if (bold)
            canvas.setFontFamily(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        else if (italic)
            canvas.setFontFamily(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
        else
            canvas.setFontFamily(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));


        if (underlined)
            canvas.setUnderLined(true);
        else
            canvas.setUnderLined(false);


        canvas.invalidate();


    }

    @Override
    public void onTypingFinished() {
        canvas.setTyping(false);
        canvas.setTextTouch(false);
        canvas.setTempTouchY(-1);
        canvas.setText(null);

        onTextChanged();
        hideSoftKeyboard();

    }

    @Override
    public void onCapsTextStyleChanged(int resId) {


        if (resId == -1) {
            canvas.setCapsText("");
            canvas.invalidate();
//            tvCapsPreview.setVisibility(View.INVISIBLE);
            etCapsPreview.setVisibility(View.INVISIBLE);
        } else {
            canvas.setCapsSize(resId);
            canvas.invalidate();
//            tvCapsPreview.setTextSize(resId / 3);
            etCapsPreview.setTextSize(resId);
        }

        canvas.invalidate();


    }

    private Bitmap getMosaicsBitmap(Bitmap bmp, float precent) {

        double ratio = (double) bmp.getWidth() / bmp.getHeight();

        int h = 300;

        Bitmap resized = Bitmap.createScaledBitmap(bmp, (int) (h * ratio), h, false);


        float mosaic = (0.025f * (resized.getWidth()));

//        float mosaic = (bmp.getWidth() / 90) * (300 / (float) bmp.getHeight());


        Log.d("logMosaic", "bmp w: " + bmp.getWidth() + ", h: " + bmp.getHeight() + ", mosaic: " + mosaic);
        Log.d("logMosaic", "resized w: " + resized.getWidth() + ", h: " + resized.getHeight() + ", mosaic: " + mosaic);


        GPUImage mGPUImage = new GPUImage(ShareActivity.this);
        mGPUImage.setImage(resized); // this loads image on the current thread, should be run in a thread

        GPUImagePixelationFilter filter = new GPUImagePixelationFilter();
        filter.setPixel(mosaic);

        mGPUImage.setFilter(filter);
        mGPUImage.requestRender();

        Bitmap filteredBitmap = mGPUImage.getBitmapWithFilterApplied();

        resized = Bitmap.createScaledBitmap(filteredBitmap, bmp.getWidth(), bmp.getHeight(), false);


        Log.d("logPixelatePaint", "resized.getWidth(): " + resized.getWidth() + ", resized.getHeight(): " + resized.getHeight());


        return resized;
    }


    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    boolean italic = false;
    boolean bold = false;
    boolean underlined = false;

    int diff = 0;


    @Override
    public void onMatrixChanged(int type) {

        Log.d("logMatrix", "onMatrixChanged type: " + type);

        svMosaic.scrollTo(0, type);
        diff = type;

//        canvas.setCircleY(canvas.getCircleY()  type);
//        canvas.invalidate();

        if (mosaicBitmap != null) {
            canvas.setMosaicBitmap(mosaicBitmap);

            cbvImage.setBitmapPadding(type);
            cbvImage.invalidate();
        }


        onFocusLocationChanged(cbvImage.getmCx(), cbvImage.getmCy() - type, cbvImage.getRadius(), cbvImage.getBitmapAlpha());

    }

    int photoEditEventSentCount = 0;

    private void refreshUndoIcon() {

//        if (photoEditEventSentCount == 1) {
//            PhotoEdit event = new PhotoEdit();
//            Netmera.sendEvent(event);
//        }

        photoEditEventSentCount++;

        if ((historyList.size() > 1 && snapShots.size() > 1) || savedHistoryList.size() > 0) {
            Log.d("logStackSize", "1 historyList historyList.size(): " + historyList.size());
            ivUndo.setEnabled(true);
            ivUndo.setAlpha(1f);
        } else {
            Log.d("logStackSize", "2 historyList historyList.size(): " + historyList.size());
            ivUndo.setEnabled(false);
            ivUndo.setAlpha(0.4f);
        }
    }

    @Override
    public void onPathChanged(List<Path> paths, List<ArrayList<Region>> regions, List<Paint> paints) {

        if (currentDrawer != CanvasView.Drawer.TEXT) {

            LogManager.addLog("Editing Screen - Feature applied - Drawing");

            SnapShot newSnapShot = new SnapShot();
            if (!snapShots.isEmpty()) {
                newSnapShot = newSnapShot.getNewInstance(snapShots.get(snapShots.size() - 1));

            } else {
                newSnapShot = new SnapShot();
            }
            newSnapShot.setActionType(1);

            historyList.add(1);

            snapShots.add(newSnapShot);
            canvas.setCurrentSnapShot(newSnapShot);
//        canvas.setBitmapChanged(true);
            canvas.invalidate();
            refreshUndoIcon();

            Log.d("onPathChanged", "onPathChanged historyPointer: " + historyList.get(historyList.size() - 1));
        }


    }

    @Override
    public void onActionListener(int historyPointer) {
//        Log.d("historyPointer", "onActionListener historyPointer: " + historyList.get(historyList.size() - 1));

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1 && resultCode == RESULT_OK) {
            int result = data.getIntExtra("result", 0);
            selectItem(result);
        }

        if (requestCode == 2) {
            if (resultCode != RESULT_OK)
                finishActivityAndStartCropActivity();
        }

    }

    private void selectItem(int result) {
        if (result != 0) {
            shapes.selectItem(result);

            focusTouched = true;

            switch (result) {
                case 13:
                    onShapeChanged(CanvasView.Drawer.CAPS);
                    break;
                case 12:
                    onShapeChanged(CanvasView.Drawer.FOCUS);
                    break;
                case 11:
                    onShapeChanged(CanvasView.Drawer.PIXELATE);
                    break;
                case 10:
                    onShapeChanged(CanvasView.Drawer.FRAME);
                    break;
                case 9:
                    onShapeChanged(CanvasView.Drawer.ELLIPSE);
                    break;
                case 8:
                    onShapeChanged(CanvasView.Drawer.RECTANGLE);
                    break;
                case 7:
                    onShapeChanged(CanvasView.Drawer.PEN);
                    break;
                case 6:
                    onShapeChanged(CanvasView.Drawer.LINE);
                    break;
                case 5:
                    onShapeChanged(CanvasView.Drawer.ARROW);
                    break;
                case 4:
                    onShapeChanged(CanvasView.Drawer.TEXT);
                    break;
                case 3:
                    onShapeChanged(CanvasView.Drawer.FILTER);
                    break;
                case 2:
                    onShapeChanged(CanvasView.Drawer.ADJUSTMENT);
                    break;
                case 1:
                    ivCrop.performClick();
                    break;
                case 0:
//                    onShapeChanged(null);
                    break;
                case -1:
                    ivShare.performClick();
                    break;
                case -2:
                    ivBack.performClick();
                    break;
                case -3:
                    ivUndo.performClick();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {


        if (gvShare.getVisibility() == View.VISIBLE) {
            rlToolbar.performClick();
        } else {

            deleteTempFiles();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finishActivityAndStartCropActivity();
                }
            }, 200);


        }

    }

    private void deleteTempFiles() {
        if (!save) {
            Log.d("logSave", "save: false");
            SharedPreferences prefs = getSharedPreferences("Prefs", MODE_PRIVATE);
            boolean autoSave = prefs.getBoolean("autoSave", true);
            Log.d("logSave", "autoSave: " + autoSave);
            if (autoSave) {
                Log.d("logSave", "autoSave: true");
            } else {
                Log.d("logSave", "autoSave: false");
                Utils.deleteTempFile(CropyApp.croppedBitmapPath, this, true);
            }
        } else {
            Log.d("logSave", "save: true");
        }
    }


    @Override
    public void onImageDeleted() {
        finishActivityAndStartCropActivity();
    }

    private void finishActivityAndStartCropActivity() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("startCrop", startCrop);
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onShared(int position) {
        startCrop = false;

        switch (position) {
            case 0:
                shareRequest("bipButton");
                break;
            case 1:
                shareRequest("whatsappButton");
                break;
            case 2:
                shareRequest("mailButton");
                break;
            case 3:
                shareRequest("fbButton");
                break;
            case 4:
                shareRequest("twitterButton");
                break;
            case 5:
                shareRequest("instagramButton");
                break;
            case 6:
                shareRequest("turkcellCloudButton");
                break;
            case 7:
                shareRequest("download");
                break;
        }

    }

    boolean hasNavBar(Context context) {
        Resources resources = context.getResources();
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            return resources.getBoolean(id);
        } else {    // Check for keys
            boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            return !hasMenuKey && !hasBackKey;
        }
    }

    public void onShowKeyboard(int keyboardHeight) {


        if (capsActive) {

            if (showTextOrCapsBar) {
                Log.d("logCapsKeyboard", "keyboardHeight: " + keyboardHeight + " getStatusBarHeight: " + getStatusBarHeight());
                Rect r = new Rect();
                rootLayout.getWindowVisibleDisplayFrame(r);

                llCapsPreview.setVisibility(View.VISIBLE);
                llCapsPreview.setPadding(0, 0, 0, keyboardHeight - getStatusBarHeight());
            }


        } else {

            if (showTextOrCapsBar) {
                Log.d("logCapsKeyboard", "touchY: " + touchY);
                Rect r = new Rect();
                rootLayout.getWindowVisibleDisplayFrame(r);

                rlTextStyle.setVisibility(View.VISIBLE);
                rlTextStyle.setPadding(0, 0, 0, keyboardHeight - getStatusBarHeight());


            }


//            if (touchY != -1) {
//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                lp.setMargins(0, 0, 0, touchY > keyboardHeight - getStatusBarHeight() ? (int) touchY - (keyboardHeight - getStatusBarHeight()) : 0);
//                rlCenter.setLayoutParams(lp);
//
//            }


        }

    }


    @Override
    public void onCapsTouched() {

        Log.d("onCapsTouched", "onCapsTouched");
        enableCaps();
        shapes.resetBorders();
    }


    public void unbindDrawables(View view) {//pass your parent view here

        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }

            Log.d("logUnbind", "success");

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("logUnbind", "fail error: " + e.getMessage());

        }
    }


}