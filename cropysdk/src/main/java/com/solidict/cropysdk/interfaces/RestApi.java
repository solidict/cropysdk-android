package com.solidict.cropysdk.interfaces;


import com.solidict.cropysdk.models.ActionResult;
import com.solidict.cropysdk.models.DeviceInfo;
import com.solidict.cropysdk.models.Eula;
import com.solidict.cropysdk.models.NewUser;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;

/**
 * Created by serdarbuyukkanli on 08/03/16.
 */
public interface RestApi {


    @POST("/api/auth/rememberMe")
    void rememberMe(@Body DeviceInfo body, Callback<Object> actionResult);

    @POST("/api/auth/gsm/login?rememberMe=on")
    void gsm(@Body DeviceInfo body, Callback<Object> actionResult);

    @POST("/api/auth/token?rememberMe=on")
    void login(@Body NewUser body, Callback<Object> actionResult);

    @POST("/api/container/baseUrl")
    void baseUrl(Callback<ActionResult> actionResult);

    @PUT("/{uuid}")
    void uploadImage(@Path("uuid") String uuid, @Body TypedInput file, Callback<ActionResult> actionResult);

    @GET("/api/eula/get/{language}")
    void eulaGet(@Path("language") String language, Callback<Eula> termCallback);

    @GET("/api/eula/approve/{eulaid}")
    void eulaApprove(@Path("eulaid") int eulaid, Callback<ActionResult> actionResult);

    @POST("/api/account/provision")
    void provision(Callback<ActionResult> actionResult);

    @POST("/getToken")
    void getToken(Callback<String> token);

    @Multipart
    @POST("/saveCropped")
    void saveCropped(@Part("shareType") String shareType, @Part("title") String title, @Part("desc") String desc, @Part("url") String url, @Part("lang") String lang, @Part("contentType") String contentType, @Part("mix") String mix, @Part("thumb") TypedFile thumb, @Part("org") TypedFile org, @Part("imageWidth") int imageWidth, @Part("imageHeight") int imageHeight, Callback<Response> response);

    @POST("/upload/share")
    @FormUrlEncoded
    void share(@Field("app-version") String appVersion, @Field("shareType") String shareType, @Field("session-id") String sessionId, Callback<Response> response);

}
