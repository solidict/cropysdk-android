package com.solidict.cropysdk.interfaces;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface ShapeListener {

    void onShapeChanged(int type);
}
