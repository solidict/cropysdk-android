package com.solidict.cropysdk.interfaces;

import android.graphics.Bitmap;

/**
 * Created by serdarbuyukkanli on 27/06/16.
 */
public interface RotateListener {

    void onRotated(Bitmap bitmap, int type);
}