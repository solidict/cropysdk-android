package com.solidict.cropysdk.interfaces;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface FilterListener {

    void onFilterChanged(int resId);
}
