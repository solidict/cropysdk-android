package com.solidict.cropysdk.interfaces;


import com.solidict.cropysdk.views.CanvasView;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface ShapeDrawerListener {

    void onShapeChanged(CanvasView.Drawer drawer);
}
