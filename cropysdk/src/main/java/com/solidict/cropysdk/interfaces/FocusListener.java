package com.solidict.cropysdk.interfaces;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface FocusListener {

    void onFocusRadiusChanged(float value);

    void onFocusChanged(float radius);

    void onFocusLocationChanged(float x, float y, float radius, int alpha);


}
