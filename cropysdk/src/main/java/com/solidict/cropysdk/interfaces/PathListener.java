package com.solidict.cropysdk.interfaces;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Region;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface PathListener {

    void onPathChanged(List<Path> paths, List<ArrayList<Region>> regions, List<Paint> paints);
}
