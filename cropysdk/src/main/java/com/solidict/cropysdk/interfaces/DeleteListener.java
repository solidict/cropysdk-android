package com.solidict.cropysdk.interfaces;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public interface DeleteListener {

    void onImageDeleted();
}
