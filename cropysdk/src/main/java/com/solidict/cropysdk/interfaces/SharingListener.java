package com.solidict.cropysdk.interfaces;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by serdarbuyukkanli on 27/01/17.
 */
public interface SharingListener extends Serializable {

    void onUrlPrepared(String url);

    void onBitmapPrepared(Bitmap bitmap);

}
