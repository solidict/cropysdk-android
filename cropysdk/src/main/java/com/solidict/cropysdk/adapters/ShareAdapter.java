package com.solidict.cropysdk.adapters;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.graphics.BitmapCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.solidict.cropysdk.CropyApp;
import com.solidict.cropysdk.DepoActivity;
import com.solidict.cropysdk.R;
import com.solidict.cropysdk.ShareActivity;
import com.solidict.cropysdk.interfaces.ShareListener;
import com.solidict.cropysdk.log.LogManager;
import com.solidict.cropysdk.models.ShareItem;
import com.solidict.cropysdk.utils.Utils;
import com.solidict.cropysdk.views.ShareIntentChooserDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdarbuyukkanli on 09/06/16.
 */
public class ShareAdapter extends BaseAdapter {

    private Activity mActivity;
    ArrayList<ShareItem> shareItems;
    //    String path;
    String webPageTitle;
    String shareUrl;
    String webUrl;

    private long mLastClickTime = 0;


    public ShareAdapter(Activity activity, ArrayList<ShareItem> shareItems, String webPageTitle, String shareUrl, String webUrl) {
        mActivity = activity;
        this.shareItems = shareItems;
//        this.path = path;
        this.webPageTitle = webPageTitle;
        this.shareUrl = shareUrl;
        this.webUrl = webUrl;
    }

    public int getCount() {
        return this.shareItems.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.cell_share, null);
        }
        TextView tvShareItemName = (TextView) convertView.findViewById(R.id.tvShareItemName);
        ImageView ivShareItemIcon = (ImageView) convertView.findViewById(R.id.ivShareItemIcon);

        tvShareItemName.setText(shareItems.get(position).getName());
        ivShareItemIcon.setImageResource(shareItems.get(position).getIcon());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareListener shareListener = (ShareListener) mActivity;
                shareListener.onShared(position);

                if (SystemClock.elapsedRealtime() - mLastClickTime < 5000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                sharingLog(position);

                if (position == shareItems.size() - 1)
                    shareOthers();
                else if (position == 0)
                    shareWithIntent("bip");
                else if (position == 1)
                    shareWithIntent("com.whatsapp");
                else if (position == 2)
                    shareWithIntent("email");
                else if (position == 3)
                    shareWithIntent("com.facebook.katana");
                else if (position == 4)
                    shareWithIntent("com.twitter");
                else if (position == 5)
                    shareWithIntent("com.instagram");
                else if (position == 6) {
                    Intent startIntent = new Intent(mActivity, DepoActivity.class);
//                    startIntent.putExtra("path", path);
                    mActivity.startActivity(startIntent);
                } else if (position == 7) {
                    save();
                } else
                    shareWithIntent("com.twitter");

            }
        });

        return convertView;
    }

    public void save() {
        Utils.createDirectoryAndSaveFile(CropyApp.croppedBitmapPath, mActivity, CropyApp.croppedBitmap, true).getPath();

        ShareActivity shareActivity = (ShareActivity) mActivity;
        shareActivity.setSave(true);
        shareActivity.showCustomDialog(R.drawable.ic_dialog_info, mActivity.getString(R.string.information), mActivity.getResources().getString(R.string.saved));
//        Toast.makeText(mActivity, R.string.saved, Toast.LENGTH_SHORT).show();
    }

    private void sharingLog(int position) {
        String channel = "";

        switch (position) {
            case 0:
                channel = "Bip";
                break;
            case 1:
                channel = "WhatsApp";
                break;
            case 2:
                channel = "E-Mail";
                break;
            case 3:
                channel = "Facebook";
                break;
            case 4:
                channel = "Twitter";
                break;
            case 5:
                channel = "Linkedin";
                break;
            case 6:
                channel = "lifebox";
                break;
            case 8:
                channel = "another channel";
                break;
        }

        if (position == 7) {
            LogManager.addLog("Editing Screen - Image saved to device");
        } else {
            LogManager.addLog("Editing Screen - Image shared via " + channel);
        }

    }


    private void shareWithIntent(String packageName) {


        String path = Utils.createDirectoryAndSaveFile(CropyApp.croppedBitmapPath, mActivity, CropyApp.croppedBitmap, true).getPath();
//        Cropy.croppedBitmapPath = path;


        if (packageName.equals("bip")) {

            Bitmap bm = BitmapFactory.decodeFile(path);

            Bitmap scaledBitmap = Utils.scaleDown(bm, 2000, false, false);
            bm = scaledBitmap;

            int sBitmapByteCount = BitmapCompat.getAllocationByteCount(bm);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 40, baos); //bm is the bitmap object


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(Utils.getBipShare() + shareUrl));

            try {
                mActivity.startActivity(intent);
            } catch (ActivityNotFoundException exception) {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();
            }
        } else if (packageName.equals("com.instagram")) {


            String body = "";

            if (!webPageTitle.equals("")) {
                try {

                    String channel = Utils.getDomainName(webUrl, true);
                    channel = channel.substring(0, 1).toUpperCase() + channel.substring(1, channel.length());

//                    body = (ignoreTurkishCharacters(webPageTitle.toLowerCase()).contains(Utils.getDomainName(webUrl, true).toLowerCase()) ? webPageTitle : webPageTitle + " - " + channel) + " - " + shareUrl;
                    body = shareUrl;

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
//                body = mActivity.getResources().getString(R.string.your_friend_sent_you) + " " + shareUrl;
                body = shareUrl;

            }

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_TEXT, body);
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(path)));


            boolean linkedinAppFound = false;
            List<ResolveInfo> matches2 = mActivity.getPackageManager()
                    .queryIntentActivities(share, 0);

            for (ResolveInfo info : matches2) {

                if (info.activityInfo.packageName.toLowerCase().startsWith(
                        packageName)) {
                    share.setPackage(info.activityInfo.packageName);
                    linkedinAppFound = true;
                    break;
                }
            }

            if (linkedinAppFound) {
                mActivity.startActivity(Intent.createChooser(share, mActivity.getResources().getString(R.string.share_image)));

            } else {
//                ((ShareActivity) mActivity).showCustomDialog(R.drawable.ic_dialog_error, mActivity.getString(R.string.error), mActivity.getResources().getString(R.string.dont_have_app));
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();
            }
        } else if (packageName.equals("com.facebook.katana")) {


            String body;

            if (!webPageTitle.equals("")) {
                body = shareUrl;
            } else {
                body = mActivity.getResources().getString(R.string.your_friend_sent_you) + " " + shareUrl;

            }


            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, body);

            boolean linkedinAppFound = false;
            List<ResolveInfo> matches2 = mActivity.getPackageManager()
                    .queryIntentActivities(share, 0);

            for (ResolveInfo info : matches2) {

                if (info.activityInfo.packageName.toLowerCase().startsWith(
                        packageName)) {
                    share.setPackage(info.activityInfo.packageName);
                    linkedinAppFound = true;
                    break;
                }
            }

            if (linkedinAppFound) {
                mActivity.startActivity(Intent.createChooser(share, mActivity.getResources().getString(R.string.share_image)));

            } else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();
            }

        } else if (packageName.equals("email")) {


            String body = "";
            String subject = "";

            if (!webPageTitle.equals("")) {
                try {

                    String channel = Utils.getDomainName(webUrl, true);
                    channel = channel.substring(0, 1).toUpperCase() + channel.substring(1, channel.length());

                    body = mActivity.getResources().getString(R.string.share_body_email, (ignoreTurkishCharacters(webPageTitle.toLowerCase()).contains(Utils.getDomainName(webUrl, true).toLowerCase()) ? webPageTitle : (webPageTitle + " - " + channel))) + " <br />\n" +
                            "<br />\n" + shareUrl;


                    subject = mActivity.getResources().getString(R.string.share_subj_email) + ": " + webPageTitle;


                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {

                body = mActivity.getResources().getString(R.string.your_friend_sent_you) + "<br />\n" +
                        "<br />\n" + shareUrl;
                subject = mActivity.getResources().getString(R.string.share_subj_email);

            }


            sendImageToMail(mActivity, new File(path), body, subject);


        } else if (packageName.equals("com.whatsapp") || packageName.equals("com.twitter")) {

            String body = "";

            if (!webPageTitle.equals("")) {
                try {

                    String channel = Utils.getDomainName(webUrl, true);
                    channel = channel.substring(0, 1).toUpperCase() + channel.substring(1, channel.length());
                    body = (ignoreTurkishCharacters(webPageTitle.toLowerCase()).contains(Utils.getDomainName(webUrl, true).toLowerCase()) ? webPageTitle : (webPageTitle + " - " + channel)) + (packageName.equals("com.twitter") ? " @cropyapp " : " ") + shareUrl;

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                body = mActivity.getResources().getString(R.string.your_friend_sent_you) + " " + shareUrl;

            }


            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, body);

            boolean linkedinAppFound = false;
            List<ResolveInfo> matches2 = mActivity.getPackageManager()
                    .queryIntentActivities(share, 0);

            for (ResolveInfo info : matches2) {

                if (info.activityInfo.packageName.toLowerCase().startsWith(
                        packageName)) {
                    share.setPackage(info.activityInfo.packageName);
                    linkedinAppFound = true;
                    break;
                }
            }

            if (linkedinAppFound) {
                mActivity.startActivity(Intent.createChooser(share, mActivity.getResources().getString(R.string.share_image)));

            } else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();
            }


        } else {

            String body = "";

            if (!webPageTitle.equals("")) {
                try {

                    String channel = Utils.getDomainName(webUrl, true);
                    channel = channel.substring(0, 1).toUpperCase() + channel.substring(1, channel.length());
                    body = (ignoreTurkishCharacters(webPageTitle.toLowerCase()).contains(Utils.getDomainName(webUrl, true).toLowerCase()) ? webPageTitle : (webPageTitle + " - " + channel)) + (packageName.equals("com.twitter") ? " @cropyapp " : " ") + shareUrl;

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                body = mActivity.getResources().getString(R.string.your_friend_sent_you) + " " + shareUrl;

            }


            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("*/*");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, body);

            sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(path)));


            // Narrow down to official Twitter app, if available:
            List<ResolveInfo> matches = mActivity.getPackageManager().queryIntentActivities(sharingIntent, 0);

            if (matches.size() == 0)
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();
            else {
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith(packageName)) {
                        sharingIntent.setPackage(info.activityInfo.packageName);
                        Log.d("logPackage", "package: " + info.activityInfo.packageName);
                    }

                }
                mActivity.startActivity(sharingIntent);
            }

        }


    }


    private String ignoreTurkishCharacters(String string) {

        string = string.replaceAll("ı", "i");
        string = string.replaceAll("ö", "o");
        string = string.replaceAll("ü", "u");

        return string;
    }


    //TODO alanları düzenle
    public void sendImageToMail(final Activity context, File url, String body, String subject) {


        final ShareIntentChooserDialog shareIntentChooserDialog = new ShareIntentChooserDialog(context, url, body, subject);
        shareIntentChooserDialog.show();
        shareIntentChooserDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
//                context.finish();
                shareIntentChooserDialog.dismiss();
            }
        });
        try {
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.dont_have_app), Toast.LENGTH_SHORT).show();

        }

    }

    private void shareOthers() {

        String path = Utils.createDirectoryAndSaveFile(CropyApp.croppedBitmapPath, mActivity, CropyApp.croppedBitmap, true).getPath();

        String body = "";
        if (!webPageTitle.equals("")) {
            try {

                String channel = Utils.getDomainName(webUrl, true);
                channel = channel.substring(0, 1).toUpperCase() + channel.substring(1, channel.length());
                body = mActivity.getResources().getString(R.string.share_body_email, (ignoreTurkishCharacters(webPageTitle.toLowerCase()).contains(Utils.getDomainName(webUrl, true).toLowerCase()) ? webPageTitle : (webPageTitle + " - " + channel))) + " <br />\n" +
                        "<br />\n" + shareUrl;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {

            body = mActivity.getResources().getString(R.string.your_friend_sent_you) + "<br />\n" +
                    "<br />\n" + Utils.SHARE_BODY;

        }


        Uri pictureUri = Uri.fromFile(new File(path));

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, body);
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
        shareIntent.setType("image/jpeg");


        List<ResolveInfo> activities = mActivity.getPackageManager().queryIntentActivities(shareIntent, 0);

        Log.d("logShare", "activities size: " + activities.size());


        List<Intent> targetPDFIntents = new ArrayList<Intent>();
        for (ResolveInfo currentInfo : activities) {
            String packageName = currentInfo.activityInfo.packageName;


            if (!"com.turkcell.bip".equals(packageName) && !"com.whatsapp".equals(packageName) && !"com.google.android.apps.inbox".equals(packageName) && !"com.google.android.gm".equals(packageName) && !"com.samsung.android.email.provider".equals(packageName) && !"com.facebook.katana".equals(packageName) && !"com.twitter.android".equals(packageName) && !"com.linkedin.android".equals(packageName) && !"com.turkcell.akillidepo".equals(packageName)) {
                Intent targetPdfIntent = new Intent();
                targetPdfIntent.setPackage(packageName);
                targetPdfIntent.setAction(Intent.ACTION_SEND);
                targetPdfIntent.putExtra(Intent.EXTRA_TEXT, body);
                targetPdfIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
                targetPdfIntent.setType("image/jpeg");
                targetPDFIntents.add(targetPdfIntent);

                Log.d("logShare", packageName + " included");

            } else {
                Log.d("logShare", packageName + " excluded");

            }
        }

        Log.d("logShare", "targetPDFIntents size: " + targetPDFIntents.size());


        Intent chooserIntent = Intent.createChooser(targetPDFIntents.remove(0), "Paylaş...");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetPDFIntents.toArray(new Parcelable[]{}));
        mActivity.startActivity(chooserIntent);


    }

}
