package com.solidict.cropysdk.models;

import android.util.Log;


import com.solidict.cropysdk.utils.Utils;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by serdarbuyukkanli on 23/06/16.
 */
public class Bookmark {

    String name;
    String url;
    boolean googleSearch;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFavicon() {
        try {

            String domainName = Utils.getDomainName(url, false);
            Log.d("logIcon", "domainName: " + domainName);


//            String domain = getDomainName(url);


            String favicon = "https://icons.better-idea.org/icon?url=" + domainName + "&size=64";
            Log.d("logIcon", "favIcon: " + favicon);

            return favicon;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGoogleSearch(boolean googleSearch) {
        this.googleSearch = googleSearch;
    }

    public String getName() {

        if (googleSearch) {
            return name;
        } else {
            try {
                Log.d("logToken", "getDomainName: " + Utils.getDomainName(url, true));

                String[] tokens = Utils.getDomainName(url, true).split("\\.");
                Log.d("logToken", "tokens: " + tokens.length);
                for (int i = 0; i < tokens.length; i++) {
                    Log.d("logToken", "token: " + tokens[i]);

                }

                if (tokens[0].replace(" ", "").length() < 1) {
                    return tokens[0];

                } else {

                    String keyword = tokens[0].substring(0, 1).toUpperCase() + tokens[0].substring(1);
                    if (keyword.length() > 10)
                        keyword = keyword.substring(0, 9) + "...";

                    return keyword;

                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }


    }

    private String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }


}
