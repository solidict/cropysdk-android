package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 14/07/16.
 */
public class NewUser {
    String username;
    String password;
    DeviceInfo deviceInfo;

    public NewUser(String username, String password, DeviceInfo deviceInfo) {
        this.username = username;
        this.password = password;
        this.deviceInfo = deviceInfo;
    }
}
