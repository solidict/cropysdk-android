package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 24/08/15.
 */
public class FAQItem {

    private String header;
    private String text;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}