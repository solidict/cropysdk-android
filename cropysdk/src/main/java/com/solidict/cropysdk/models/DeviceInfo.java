package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 14/07/16.
 */
public class DeviceInfo {

    String uuid;
    String name;
    String deviceType;

    public DeviceInfo(String uuid, String name, String deviceType) {
        this.uuid = uuid;
        this.name = name;
        this.deviceType = deviceType;
    }
}
