package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 07/12/16.
 */
public class FlaotPoint {
    float x, y;

    public FlaotPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}