package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 12/07/16.
 */
public class ShareItem {

    String name;
    int iconResId;

    public ShareItem(String name, int iconResId) {
        this.name = name;
        this.iconResId = iconResId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return iconResId;
    }

    public void setIcon(int iconResId) {
        this.iconResId = iconResId;
    }
}
