package com.solidict.cropysdk.models;

/**
 * Created by serdarbuyukkanli on 14/07/16.
 */
public class ActionResult {
    String status;
    String value;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
