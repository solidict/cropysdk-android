package com.solidict.cropysdk.models;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Region;


import com.solidict.cropysdk.views.CanvasView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serdarbuyukkanli on 20/12/16.
 */
public class CanvasValues {

    private List<Path> pathLists;
    private List<Paint> paintLists;
    private List<ArrayList<Region>> regionList;

    private CanvasView.Mode mode;
    private CanvasView.Drawer drawer;

    public ArrayList<String> capsList;
    public ArrayList<String> textList;

    private String text;
    private float textX;
    private float textY;

    private String capsText;
    private int capsSize;
    private int historyPointer;

    public List<Path> getPathLists() {
        return pathLists;
    }

    public void setPathLists(List<Path> pathLists) {
        this.pathLists = pathLists;
    }

    public List<Paint> getPaintLists() {
        return paintLists;
    }

    public void setPaintLists(List<Paint> paintLists) {
        this.paintLists = paintLists;
    }

    public List<ArrayList<Region>> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<ArrayList<Region>> regionList) {
        this.regionList = regionList;
    }

    public CanvasView.Mode getMode() {
        return mode;
    }

    public void setMode(CanvasView.Mode mode) {
        this.mode = mode;
    }

    public CanvasView.Drawer getDrawer() {
        return drawer;
    }

    public void setDrawer(CanvasView.Drawer drawer) {
        this.drawer = drawer;
    }

    public ArrayList<String> getCapsList() {
        return capsList;
    }

    public void setCapsList(ArrayList<String> capsList) {
        this.capsList = capsList;
    }

    public ArrayList<String> getTextList() {
        return textList;
    }

    public void setTextList(ArrayList<String> textList) {
        this.textList = textList;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getTextX() {
        return textX;
    }

    public void setTextX(float textX) {
        this.textX = textX;
    }

    public float getTextY() {
        return textY;
    }

    public void setTextY(float textY) {
        this.textY = textY;
    }

    public String getCapsText() {
        return capsText;
    }

    public void setCapsText(String capsText) {
        this.capsText = capsText;
    }

    public int getCapsSize() {
        return capsSize;
    }

    public void setCapsSize(int capsSize) {
        this.capsSize = capsSize;
    }

    public int getHistoryPointer() {
        return historyPointer;
    }

    public void setHistoryPointer(int historyPointer) {
        this.historyPointer = historyPointer;
    }
}
