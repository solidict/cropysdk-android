package com.solidict.cropysdk.models;


import com.solidict.cropysdk.utils.Utils;

import java.util.ArrayList;

/**
 * Created by serdarbuyukkanli on 16/11/16.
 */
public class SnapShot {

    int actionType = -1;

    private int filterId = 0;
    private int frameId = -1;

    private float brightnessValue = 0;
    private float contrastValue = 1;

    private int focusX = 0;
    private int focusY = 0;
    private int focusRadius = 5000;

    private String text = "";
    public ArrayList<String> textList;


    private String capsText = "";
    private int capsSize = 16;

    public int getActionType() {
        return actionType;
    }

    public void setActionType(int actionType) {
        this.actionType = actionType;
    }

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }

    public int getFrameId() {
        return frameId;
    }

    public void setFrameId(int frameId) {
        this.frameId = frameId;
    }

    public float getBrightnessValue() {
        return brightnessValue;
    }

    public void setBrightnessValue(float brightnessValue) {
        this.brightnessValue = brightnessValue;
    }

    public float getContrastValue() {
        return contrastValue;
    }

    public void setContrastValue(float contrastValue) {
        this.contrastValue = contrastValue;
    }

    public int getFocusX() {
        return focusX;
    }

    public void setFocusX(int focusX) {
        this.focusX = focusX;
    }

    public int getFocusY() {
        return focusY;
    }

    public void setFocusY(int focusY) {
        this.focusY = focusY;
    }

    public int getFocusRadius() {
        return focusRadius;
    }

    public void setFocusRadius(int focusRadius) {
        this.focusRadius = focusRadius;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<String> getTextList() {
        return textList;
    }

    public void setTextList(ArrayList<String> textList) {
        this.textList = textList;
    }

    public String getCapsText() {
        return capsText;
    }

    public void setCapsText(String capsText) {
        this.capsText = capsText;
    }

    public int getCapsSize() {
        return capsSize;
    }

    public void setCapsSize(int capsSize) {
        this.capsSize = capsSize;
    }

    public SnapShot getNewInstance(SnapShot snapShot) {

        String jsonSnapShot = Utils.snapShotToJson(snapShot);
        SnapShot newSnapShot = Utils.jsonToSnapShot(jsonSnapShot);

        return newSnapShot;
    }

}
