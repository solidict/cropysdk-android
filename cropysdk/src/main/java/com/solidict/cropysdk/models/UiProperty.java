package com.solidict.cropysdk.models;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.utils.Utils;

import static com.solidict.cropysdk.utils.Utils.bitmapToString;

/**
 * Created by serdarbuyukkanli on 05/01/17.
 */
public class UiProperty implements Parcelable {

    Integer cropHeaderColor;
    String cropHeaderTitle;
    Integer cropRectColor;
    Integer cropHeaderTextColor;

    Integer headerColor;
    String headerTitle;
    Integer backgroundColor;
    Integer headerTextColor;
    Integer footerColor;

    Integer shareHeaderColor;
    String shareHeaderTitle;
    Integer shareBackgroundColor;

    String backIcon;
    String doneIcon;
    String shareIcon;


    public UiProperty(Context context) {

        cropHeaderColor = context.getResources().getColor(R.color.yellow);
        cropHeaderTitle = "Cropy";
        cropRectColor = context.getResources().getColor(R.color.yellow);
        cropHeaderTextColor = context.getResources().getColor(R.color.black);

        headerColor = context.getResources().getColor(R.color.yellow);
        headerTitle = context.getResources().getString(R.string.edit);
        backgroundColor = context.getResources().getColor(R.color.share_background);
        headerTextColor = context.getResources().getColor(R.color.black);
        footerColor = context.getResources().getColor(R.color.darkBlue);

        shareHeaderColor = context.getResources().getColor(R.color.yellow);
        shareHeaderTitle = context.getResources().getString(R.string.share);
        shareBackgroundColor = context.getResources().getColor(R.color.darkBlue);

        backIcon = bitmapToString(BitmapFactory.decodeResource(context.getResources(), R.drawable.geri));
        doneIcon = bitmapToString(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_done));
        shareIcon = bitmapToString(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_share));

    }


    public UiProperty(Parcel in) {
        cropHeaderColor = in.readInt();
        cropHeaderTitle = in.readString();
        cropRectColor = in.readInt();
        cropHeaderTextColor = in.readInt();

        headerColor = in.readInt();
        headerTitle = in.readString();
        backgroundColor = in.readInt();
        headerTextColor = in.readInt();
        footerColor = in.readInt();

        shareHeaderColor = in.readInt();
        shareHeaderTitle = in.readString();
        shareBackgroundColor = in.readInt();

        backIcon = in.readString();
        doneIcon = in.readString();
        shareIcon = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (cropHeaderColor != null)
            dest.writeInt(cropHeaderColor);
        if (cropHeaderTitle != null)
            dest.writeString(cropHeaderTitle);
        if (cropRectColor != null)
            dest.writeInt(cropRectColor);
        if (cropHeaderTextColor != null)
            dest.writeInt(cropHeaderTextColor);

        if (headerColor != null)
            dest.writeInt(headerColor);
        if (headerTitle != null)
            dest.writeString(headerTitle);
        if (backgroundColor != null)
            dest.writeInt(backgroundColor);
        if (headerTextColor != null)
            dest.writeInt(headerTextColor);
        if (footerColor != null)
            dest.writeInt(footerColor);

        if (shareHeaderColor != null)
            dest.writeInt(shareHeaderColor);
        if (shareHeaderTitle != null)
            dest.writeString(shareHeaderTitle);
        if (shareBackgroundColor != null)
            dest.writeInt(shareBackgroundColor);

        if (backIcon != null)
            dest.writeString(backIcon);
        if (doneIcon != null)
            dest.writeString(doneIcon);
        if (shareIcon != null)
            dest.writeString(shareIcon);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UiProperty> CREATOR = new Creator<UiProperty>() {
        @Override
        public UiProperty createFromParcel(Parcel in) {
            return new UiProperty(in);
        }

        @Override
        public UiProperty[] newArray(int size) {
            return new UiProperty[size];
        }
    };

    public Integer getCropHeaderColor() {
        return cropHeaderColor;
    }

    public void setCropHeaderColor(Integer cropHeaderColor) {
        this.cropHeaderColor = cropHeaderColor;
    }

    public String getCropHeaderTitle() {
        return cropHeaderTitle;
    }

    public void setCropHeaderTitle(String cropHeaderTitle) {
        this.cropHeaderTitle = cropHeaderTitle;
    }

    public Integer getCropRectColor() {
        return cropRectColor;
    }

    public void setCropRectColor(Integer cropRectColor) {
        this.cropRectColor = cropRectColor;
    }

    public Integer getHeaderColor() {
        return headerColor;
    }

    public void setHeaderColor(Integer headerColor) {
        this.headerColor = headerColor;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Integer getShareHeaderColor() {
        return shareHeaderColor;
    }

    public void setShareHeaderColor(Integer shareHeaderColor) {
        this.shareHeaderColor = shareHeaderColor;
    }

    public String getShareHeaderTitle() {
        return shareHeaderTitle;
    }

    public void setShareHeaderTitle(String shareHeaderTitle) {
        this.shareHeaderTitle = shareHeaderTitle;
    }

    public Integer getShareBackgroundColor() {
        return shareBackgroundColor;
    }

    public void setShareBackgroundColor(Integer shareBackgroundColor) {
        this.shareBackgroundColor = shareBackgroundColor;
    }

    public Integer getCropHeaderTextColor() {
        return cropHeaderTextColor;
    }

    public void setCropHeaderTextColor(Integer cropHeaderTextColor) {
        this.cropHeaderTextColor = cropHeaderTextColor;
    }

    public Integer getHeaderTextColor() {
        return headerTextColor;
    }

    public void setHeaderTextColor(Integer headerTextColor) {
        this.headerTextColor = headerTextColor;
    }

    public Integer getFooterColor() {
        return footerColor;
    }

    public void setFooterColor(Integer footerColor) {
        this.footerColor = footerColor;
    }

    public String getBackIcon() {
        return backIcon;
    }

    public void setBackIcon(Context context, @DrawableRes int resId) {
        this.backIcon = Utils.bitmapToString(BitmapFactory.decodeResource(context.getResources(), resId));
    }

    public String getDoneIcon() {
        return doneIcon;
    }

    public void setDoneIcon(Context context, @DrawableRes int resId) {
        this.doneIcon = Utils.bitmapToString(BitmapFactory.decodeResource(context.getResources(), resId));
    }

    public String getShareIcon() {
        return shareIcon;
    }

    public void setShareIcon(Context context, @DrawableRes int resId) {
        this.shareIcon = Utils.bitmapToString(BitmapFactory.decodeResource(context.getResources(), resId));
    }
}
