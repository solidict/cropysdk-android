package com.solidict.cropysdk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.solidict.cropysdk.cropper.CropImageView;
import com.solidict.cropysdk.cropper.CropOverlayView;
import com.solidict.cropysdk.interfaces.CropListener;
import com.solidict.cropysdk.log.LogManager;
import com.solidict.cropysdk.models.UiProperty;
import com.solidict.cropysdk.utils.Utils;


public class CropActivity extends Activity {

    CropImageView cropImageView;
    ImageView ivBack;
    ImageView ivDone;
    ImageView ivCrop;
    RelativeLayout rlZoom;
    RelativeLayout rlBar;
    RelativeLayout rootLayout;
    RelativeLayout rlFrame;
    TextView tvTitle;

    // TODO burdaki static activity referansı için geniş zamanda uygun çözüm buluncak
    static Activity mActivity;
    static Bitmap mBitmap;
    static UiProperty mUiProperty;

    int scrollY;

    boolean reset = false;

    float ratio = 1.0f;

    public static int x, y, width, height;

    boolean firstTouch = true;
    boolean init = true;
    int resetCount = 0;

    int left, top, right, bottom;


    @Override
    protected void onRestart() {
        super.onRestart();
        ivDone.setVisibility(View.VISIBLE);

    }

    private void makeUiChanges(UiProperty uiProperty) {

        //activity changes
        rlBar.setBackgroundColor(uiProperty.getCropHeaderColor());
        tvTitle.setText(uiProperty.getCropHeaderTitle());
        tvTitle.setTextColor(uiProperty.getCropHeaderTextColor());
        rlFrame.setBackgroundColor(uiProperty.getBackgroundColor());

        ivBack.setImageBitmap(Utils.stringToBitmap(uiProperty.getBackIcon()));
        ivDone.setImageBitmap(Utils.stringToBitmap(uiProperty.getDoneIcon()));

    }

    private void initViews() {

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivCrop = (ImageView) findViewById(R.id.ivCrop);
        ivDone = (ImageView) findViewById(R.id.ivDone);
        rlZoom = (RelativeLayout) findViewById(R.id.rlZoom);
        rlBar = (RelativeLayout) findViewById(R.id.rlBar);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        rlFrame = (RelativeLayout) findViewById(R.id.rlFrame);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

    }


    @Override
    protected void onStart() {
        super.onStart();
        System.gc();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (!Utils.isTablet(this)) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        } else {
////            if (this instanceof IntroActivity || this instanceof IntroTermsActivity || this instanceof SplashActivity) {
//            if (getResources().getConfiguration().orientation == 1)
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            else
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
////            }
//        }

        scrollY = getIntent().getExtras().getInt("scrollY", 0);
        ratio = mBitmap.getHeight() / Utils.getScreenHeight(this);
        setContentView(R.layout.activity_crop);


//        final ScrollView svZoom = (ScrollView) findViewById(R.id.svZoom);


        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivCrop = (ImageView) findViewById(R.id.ivCrop);
        ivDone = (ImageView) findViewById(R.id.ivDone);
        rlZoom = (RelativeLayout) findViewById(R.id.rlZoom);
        rlBar = (RelativeLayout) findViewById(R.id.rlBar);

        initViews();

        cropImageView.setAlpha(0f);
        cropImageView.setAutoZoomEnabled(false);


        if (mUiProperty != null)
            makeUiChanges(mUiProperty);
        else {
            mUiProperty = new UiProperty(CropActivity.this);
        }


        final CropOverlayView mCropOverlayView = (CropOverlayView) cropImageView.findViewById(R.id.CropOverlayView);

        mCropOverlayView.setCropWindowChangeListener(new CropOverlayView.CropWindowChangeListener() {
            @Override
            public void onCropWindowChanged(boolean inProgress) {


                cropImageView.handleCropWindowChanged(inProgress, true);

                Rect r = cropImageView.getCropRect();
                Log.d("logCrop", "left: " + r.left + ", top: " + r.top + ", right: " + r.right + ", bottom: " + r.bottom);

                Log.d("logFocus", "onCropWindowChanged inProgess: " + inProgress);
                if (inProgress) {
                    ivBack.setImageResource(R.drawable.reset_icon_black);
                    reset = true;
                }

                if ((resetCount == 2 || resetCount == 3 || resetCount == 4) && !init) {

                    firstTouch = false;

                    Log.d("logFocus", "reset ratio: " + ratio);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cropImageView.mAttacher.onScale(1, 0, scrollY / ratio);
                        }
                    }, 10);
                }

                init = false;
                resetCount++;

            }
        });


//        Bitmap scaledBitmap = scaleBitmap(mBitmap);
        final Bitmap scaledBitmap = mBitmap;

//      ivOriginal.setImageBitmap(scaledBitmap);
        cropImageView.setImageBitmap(scaledBitmap);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d("logCrop", "scrollY: " + scrollY);

//can be needed
//                if (cropImageView.getHeight() - rlZoom.getHeight() < scrollY && cropImageView.getHeight() - rlZoom.getHeight() >= 0)
//                    scrollY = cropImageView.getHeight() - rlZoom.getHeight();

                Bitmap holder = BitmapFactory.decodeResource(getResources(), R.drawable.crop_area_sari);


                Log.d("logCrop", "scaledBitmap.getWidth(): " + scaledBitmap.getWidth() + ", scaledBitmap.getHeight(): " + scaledBitmap.getHeight());
                Log.d("logCrop", "rlZoom.getWidth(): " + rlZoom.getWidth() + ", rlZoom.getHeight(): " + rlZoom.getHeight());
                Log.d("logCrop", "cropImageView.getWidth(): " + cropImageView.getWidth() + ", cropImageView.getHeight(): " + cropImageView.getHeight());


                //this works
//                int padding = (holder.getWidth() / 2) + 150;
//                int left = padding;
//                int top = padding + scrollY;
//                int right = scaledBitmap.getWidth() - padding;
//                int bottom = (scaledBitmap.getHeight() > rlZoom.getHeight() ? rlZoom.getHeight() : scaledBitmap.getHeight()) - padding + scrollY;

                int padding = (holder.getWidth() / 2) + 150;
                left = scaledBitmap.getWidth() / 4;
                top = scaledBitmap.getHeight() / 4;
                right = 3 * scaledBitmap.getWidth() / 4;
                bottom = 3 * scaledBitmap.getHeight() / 4;

//                int left = Utils.getScreenWidth(CropActivity.this) / 2 - 500;
//                int top = Utils.getScreenHeight(CropActivity.this) / 2 - 500;
//                int right = Utils.getScreenWidth(CropActivity.this) / 2 + 500;
//                int bottom = Utils.getScreenHeight(CropActivity.this) / 2 + 500;

                cropImageView.setAlpha(1f);


                float wRatio = scaledBitmap.getWidth() / Utils.getScreenWidth(CropActivity.this);
                float hRatio = scaledBitmap.getHeight() / Utils.getScreenHeight(CropActivity.this);

                cropImageView.setCropRect(new Rect(left, top, right, bottom));
//                cropImageView.setCropRect(new Rect(50, 50, (int) (5000 * wRatio), (int) (5000 * hRatio)));

                Rect r = cropImageView.getCropRect();

                cropImageView.resetCropRect();


                if (ratio > 1)
                    cropImageView.mAttacher.onScale(ratio, 0, scrollY / ratio);


                cropImageView.mAttacher.switchToZoom();


            }
        }, 200);


        ivDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                LogManager.addLog("Cropping Screen - Done icon pressed");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap cropped;
//                        Log.d("logSizeFinal", "rect x: " + x + ", y: " + y + ", width: " + width + ", height: " + height);


                        x = Math.abs(x);
                        y = Math.abs(y);
                        width = Math.abs(width);
                        height = Math.abs(height);

                        Rect rRect = new Rect(x, y, x + width, y + height);

                        Log.d("logSizeFinal", "rRect x: " + rRect.left + ", y: " + rRect.top + ", width: " + (rRect.right - rRect.left) + ", height: " + (rRect.bottom - rRect.top));


                        RectF rBitmap = cropImageView.mAttacher.getDisplayRect();
                        Log.d("logSizeFinal", "rBitm x: " + rBitmap.left + ", y: " + rBitmap.top + ", width: " + (rBitmap.right - rBitmap.left) + ", height: " + (rBitmap.bottom - rBitmap.top));


                        cropped = getCalculatedBitmap(rRect, rBitmap, cropImageView.getWidth() / 2, cropImageView.getHeight() / 2, mBitmap);


                        if (cropped == null) {
//                            showCustomDialog(R.drawable.ic_dialog_error, getString(R.string.error), getResources().getString(R.string.please_select_a_valid_area));
//                            Toast.makeText(CropActivity.this, getResources().getString(R.string.please_select_a_valid_area), Toast.LENGTH_SHORT).show();

                        } else {
                            Rect rect = cropImageView.getCropRect();
                            Log.d("logSizeFinal", "rect x: " + rect.left + ", y: " + rect.top + ", width: " + (rect.right - rect.left) + ", height: " + (rect.bottom - rect.top));

                            if (mBitmap != null) {
                                mBitmap.recycle();
                                mBitmap = null;
                                Runtime.getRuntime().gc();
                            }

                            finish();
                            CropListener cropListener = (CropListener) mActivity;
                            cropListener.onCropped(cropped);
                        }


                    }
                }, 100);


            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reset) {

                    LogManager.addLog("Cropping Screen - Reset icon pressed");


//                    cropImageView.mAttacher.onScale(1, 0, 0);
//                    cropImageView.resetCropRect();

//                    if (ratio > 1)
//                        cropImageView.mAttacher.onScale(ratio, 0, scrollY / ratio);
//                    else
//                        cropImageView.mAttacher.onScale(1, 0, 0);

                    firstTouch = true;
                    resetCount = 2;


//                    cropImageView.setCropRect(new Rect(50, 50, (int) (5000 * wRatio), (int) (5000 * hRatio)));
                    cropImageView.resetCropRect();
                    cropImageView.setCropRect(new Rect(left, top, right, bottom));


                    reset = false;
                    ivBack.setImageResource(R.drawable.close);

//                    svZoom.scrollTo(0, scrollY);

                } else
                    finish();
            }
        });

    }

    private Bitmap getCalculatedBitmap(Rect rRect, RectF rBitmap, int centerX, int centerY, Bitmap bitmap) {


        double x, y, w, h;

        x = (rRect.left - rBitmap.left) / (rBitmap.right - rBitmap.left) * bitmap.getWidth();
        y = (rRect.top - rBitmap.top) / (rBitmap.bottom - rBitmap.top) * bitmap.getHeight();
        w = (rRect.right - rRect.left) / (rBitmap.right - rBitmap.left) * bitmap.getWidth();
        h = (rRect.bottom - rRect.top) / (rBitmap.bottom - rBitmap.top) * bitmap.getHeight();

        Log.d("logSizeRect", "Final rect x: " + x + ", y: " + y + ", w: " + w + ", h: " + h);

        if (x < 0) {
            w += x;
            x = 0;
        }

        if (x + w > bitmap.getWidth()) {
            w = bitmap.getWidth() - x;
        }

        if (y < 0) {
            h += y;
            y = 0;
        }

        if (y + h > bitmap.getHeight()) {
            h = bitmap.getHeight() - y;
        }

        try {
            return Bitmap.createBitmap(bitmap, (int) x, (int) y, (int) w, (int) h);
        } catch (IllegalArgumentException e) {
            return null;
        }

    }


    public static void startCropActivity(Activity activity, Bitmap bitmap, int scrollY, UiProperty uiProperty) {

        mActivity = activity;
        mUiProperty = uiProperty;

        if (bitmap.getWidth() > Utils.getScreenWidth(mActivity))
            mBitmap = scaleDown(bitmap, Utils.getScreenWidth(mActivity), false, true);
        else
            mBitmap = bitmap;

        Intent intent = new Intent(activity, CropActivity.class);
        intent.putExtra("scrollY", scrollY);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);

    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter, boolean canBeLarger) {
//        float ratio = Math.min(
//                (float) maxImageSize / realImage.getWidth(),
//                (float) maxImageSize / realImage.getHeight());

        float ratio = maxImageSize / realImage.getWidth();

        if ((maxImageSize < realImage.getWidth() || maxImageSize < realImage.getHeight()) || canBeLarger) {

            int width = Math.round((float) ratio * realImage.getWidth());
            int height = Math.round((float) ratio * realImage.getHeight());
            Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);

            return newBitmap;
        }


        return realImage;
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }


}
