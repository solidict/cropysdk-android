package com.solidict.cropysdk.log;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;

/**
 * Created by serdarbuyukkanli on 29/12/16.
 */
public class LogManager {

    public static FileHandler logger = null;
    private static String filename = "cropy_log";

    static String state = Environment.getExternalStorageState();

    //TODO have a look at other LogManager
    public static void addLog(String message) {
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/cropy_log_file");
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (!dir.exists()) {
                Log.d("logFile", "Dir created ");
                dir.mkdirs();
            }

            File logFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/cropy_log_file/" + filename + ".txt");

            if (!logFile.exists()) {
                try {
                    Log.d("logFile", "File created ");
                    logFile.createNewFile();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                String currentDateandTime = sdf.format(new Date());
                message = currentDateandTime + " : " + message;
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));

                buf.write(message + "\r\n");
                //buf.append(message);
                buf.newLine();
                buf.flush();
                buf.close();
                Log.d("logFile", "Message written to the file: " + message);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.d("logFile", "Error: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static File getLogFile() {
        File logFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/cropy_log_file/" + filename + ".txt");
        return logFile;
    }


}