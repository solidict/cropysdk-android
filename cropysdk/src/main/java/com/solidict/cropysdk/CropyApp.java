package com.solidict.cropysdk;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.solidict.cropysdk.interfaces.RestApi;
import com.solidict.cropysdk.utils.Utils;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by serdarbuyukkanli on 14/07/16.
 */
public class CropyApp extends Application {


    public static Bitmap croppedBitmap;
    public static String croppedBitmapPath;


    @Override
    public void onCreate() {
        super.onCreate();

    }

    public void setxRememberMe(String xRememberMe) {
        SharedPreferences preferences = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("xRememberMe", xRememberMe);
        editor.commit();
    }

    public String getxRememberMe() {
        SharedPreferences prefs = getSharedPreferences("Prefs", MODE_PRIVATE);
        String xRememberMe = prefs.getString("xRememberMe", null);
        return xRememberMe;
    }

    public RestApi setupRestClient(boolean hasHeader, boolean https) {

        RestAdapter.Builder builder;
        if (hasHeader) {
            if (https) {
                builder = new RestAdapter.Builder()
                        .setEndpoint(Utils.BASE_URL_HTTPS)
                        .setRequestInterceptor(requestInterceptorWithToken)
                        .setClient(new OkClient(new OkHttpClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL);
            } else {
                builder = new RestAdapter.Builder()
                        .setEndpoint(Utils.BASE_URL_HTTP)
                        .setRequestInterceptor(requestInterceptorWithToken)
                        .setClient(new OkClient(new OkHttpClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL);
            }


        } else {

            if (https) {
                builder = new RestAdapter.Builder()
                        .setEndpoint(Utils.BASE_URL_HTTPS)
                        .setRequestInterceptor(requestInterceptor)
                        .setClient(new OkClient(new OkHttpClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL);
            } else {
                builder = new RestAdapter.Builder()
                        .setEndpoint(Utils.BASE_URL_HTTP)
                        .setRequestInterceptor(requestInterceptor)
                        .setClient(new OkClient(new OkHttpClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL);
            }

        }
        RestAdapter restAdapter = builder.build();
        RestApi restApi = restAdapter.create(RestApi.class);
        return restApi;
    }


    RequestInterceptor requestInterceptorWithToken = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("X-Remember-Me-Token", getxRememberMe());
            request.addHeader("Accept", "application/json");
        }
    };

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
        }
    };
}
