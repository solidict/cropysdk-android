package com.solidict.cropysdk.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.FilterListener;
import com.solidict.cropysdk.log.LogManager;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Filters extends EditView {

    ImageView ivOriginal;
    TextView tvOriginal;
    ImageView ivBlackWhite;
    TextView tvBlackWhite;
    ImageView ivSepya;
    TextView tvSepya;
    ImageView ivVintage;
    TextView tvVintage;

    ImageView ivFridge;
    TextView tvFridge;
    ImageView ivFront;
    TextView tvFront;
    ImageView ivAD1920;
    TextView tvAD1920;

    ImageView ivQuozi;
    TextView tvQuozi;
    ImageView ivCelcius;
    TextView tvCelcius;
    ImageView ivTexas;
    TextView tvTexas;
    ImageView ivGoblin;
    TextView tvGoblin;
    ImageView ivMellow;
    TextView tvMellow;
    ImageView ivBlues;
    TextView tvBlues;
    ImageView ivCreamy;
    TextView tvCreamy;
    ImageView ivSepiaHigh;
    TextView tvSepiaHigh;
    ImageView ivClassic;
    TextView tvClassic;
    ImageView ivPlate;
    TextView tvPlate;
    ImageView ivPitched;
    TextView tvPitched;
    ImageView ivNeat;
    TextView tvNeat;

    public Filters(Context context) {
        super(context);
    }

    public Filters(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Filters(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    void init() {
        inflate(getContext(), R.layout.view_filters, this);
        final FilterListener frameListener = (FilterListener) getContext();

        ivOriginal = (ImageView) findViewById(R.id.ivOriginal);
        tvOriginal = (TextView) findViewById(R.id.tvOriginal);
        ivBlackWhite = (ImageView) findViewById(R.id.ivBlackWhite);
        tvBlackWhite = (TextView) findViewById(R.id.tvBlackWhite);
        ivSepya = (ImageView) findViewById(R.id.ivSepya);
        tvSepya = (TextView) findViewById(R.id.tvSepya);
        ivVintage = (ImageView) findViewById(R.id.ivVintage);
        tvVintage = (TextView) findViewById(R.id.tvVintage);

        ivFridge = (ImageView) findViewById(R.id.ivFridge);
        tvFridge = (TextView) findViewById(R.id.tvFridge);
        ivFront = (ImageView) findViewById(R.id.ivFront);
        tvFront = (TextView) findViewById(R.id.tvFront);
        ivAD1920 = (ImageView) findViewById(R.id.ivAD1920);
        tvAD1920 = (TextView) findViewById(R.id.tvAD1920);
        ivQuozi = (ImageView) findViewById(R.id.ivQuozi);
        tvQuozi = (TextView) findViewById(R.id.tvQuozi);
        ivCelcius = (ImageView) findViewById(R.id.ivCelcius);
        tvCelcius = (TextView) findViewById(R.id.tvCelcius);
        ivTexas = (ImageView) findViewById(R.id.ivTexas);
        tvTexas = (TextView) findViewById(R.id.tvTexas);
        ivGoblin = (ImageView) findViewById(R.id.ivGoblin);
        tvGoblin = (TextView) findViewById(R.id.tvGoblin);
        ivMellow = (ImageView) findViewById(R.id.ivMellow);
        tvMellow = (TextView) findViewById(R.id.tvMellow);
        ivBlues = (ImageView) findViewById(R.id.ivBlues);
        tvBlues = (TextView) findViewById(R.id.tvBlues);
        ivCreamy = (ImageView) findViewById(R.id.ivCreamy);
        tvCreamy = (TextView) findViewById(R.id.tvCreamy);
        ivSepiaHigh = (ImageView) findViewById(R.id.ivSepiaHigh);
        tvSepiaHigh = (TextView) findViewById(R.id.tvSepiaHigh);
        ivClassic = (ImageView) findViewById(R.id.ivClassic);
        tvClassic = (TextView) findViewById(R.id.tvClassic);
        ivPlate = (ImageView) findViewById(R.id.ivPlate);
        tvPlate = (TextView) findViewById(R.id.tvPlate);
        ivPitched = (ImageView) findViewById(R.id.ivPitched);
        tvPitched = (TextView) findViewById(R.id.tvPitched);
        ivNeat = (ImageView) findViewById(R.id.ivNeat);
        tvNeat = (TextView) findViewById(R.id.tvNeat);

        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.sample);

        //TODO extended
//        ivFridge.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterFridge()));
//        ivFront.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterFront()));
//        ivAD1920.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterAD1920()));
//        ivQuozi.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterQuozi()));
//        ivCelcius.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterCelsius()));
//        ivTexas.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterTexas()));
//        ivGoblin.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterGobblin()));
//        ivMellow.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterMellow()));
//        ivBlues.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterBlues()));
//        ivCreamy.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterCreamy()));
//        ivSepiaHigh.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterSepiahigh()));
//        ivClassic.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterClassic()));
//        ivPlate.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterPlate()));
//        ivPitched.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterPitched()));
//        ivNeat.setImageBitmap(ImgLy.getFilterAppliedBitmap(icon, new ColorFilterNeat()));


        ivOriginal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvOriginal.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(0);
                LogManager.addLog("Editing Screen - Filter applied - Original");
                sendEvent("Original");


            }
        });

        ivBlackWhite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvBlackWhite.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(1);
                LogManager.addLog("Editing Screen - Filter applied - Black&White");
                sendEvent("Black&White");


            }
        });

        ivSepya.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvSepya.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(2);
                LogManager.addLog("Editing Screen - Filter applied - Oldie");
                sendEvent("Oldie");


            }
        });

        ivVintage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvVintage.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(3);
                LogManager.addLog("Editing Screen - Filter applied - Vintage");
                sendEvent("Vintage");


            }
        });

        ivFridge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvFridge.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(4);
                LogManager.addLog("Editing Screen - Filter applied - Fridge");
                sendEvent("Fridge");


            }
        });

        ivFront.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvFront.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(5);
                LogManager.addLog("Editing Screen - Filter applied - Front");
                sendEvent("Front");


            }
        });

        ivAD1920.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvAD1920.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(6);
                LogManager.addLog("Editing Screen - Filter applied - AD1920");
                sendEvent("AD1920");


            }
        });

        ivQuozi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvQuozi.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(7);
                LogManager.addLog("Editing Screen - Filter applied - Quozi");
                sendEvent("Quozi");


            }
        });

        ivCelcius.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvCelcius.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(8);
                LogManager.addLog("Editing Screen - Filter applied - Celcius");
                sendEvent("Celcius");


            }
        });

        ivTexas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvTexas.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(9);
                LogManager.addLog("Editing Screen - Filter applied - Texas");
                sendEvent("Texas");


            }
        });

        ivGoblin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvGoblin.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(10);
                LogManager.addLog("Editing Screen - Filter applied - Goblin");
                sendEvent("Goblin");


            }
        });

        ivMellow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvMellow.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(11);
                LogManager.addLog("Editing Screen - Filter applied - Mellow");
                sendEvent("Mellow");


            }
        });

        ivBlues.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvBlues.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(12);
                LogManager.addLog("Editing Screen - Filter applied - Blues");
                sendEvent("Blues");


            }
        });

        ivCreamy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvCreamy.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(13);
                LogManager.addLog("Editing Screen - Filter applied - Creamy");
                sendEvent("Creamy");


            }
        });

        ivSepiaHigh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvSepiaHigh.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(14);
                LogManager.addLog("Editing Screen - Filter applied - Sepia High");
                sendEvent("Sepia High");


            }
        });

        ivClassic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvClassic.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(15);
                LogManager.addLog("Editing Screen - Filter applied - Classic");
                sendEvent("Classic");


            }
        });

        ivPlate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvPlate.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(16);
                LogManager.addLog("Editing Screen - Filter applied - Plate");
                sendEvent("Plate");


            }
        });

        ivPitched.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvPitched.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(17);
                LogManager.addLog("Editing Screen - Filter applied - Pitched");
                sendEvent("Pitched");


            }
        });

        ivNeat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvNeat.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFilterChanged(18);
                LogManager.addLog("Editing Screen - Filter applied - Neat");
                sendEvent("Neat");


            }
        });


    }

    public void resetValues() {
        resetBorders();
        tvOriginal.setTextColor(getResources().getColor(R.color.yellow));
    }

    @Override
    void resetBorders() {
        tvOriginal.setTextColor(getResources().getColor(R.color.white));
        tvVintage.setTextColor(getResources().getColor(R.color.white));
        tvBlackWhite.setTextColor(getResources().getColor(R.color.white));
        tvSepya.setTextColor(getResources().getColor(R.color.white));

        tvFridge.setTextColor(getResources().getColor(R.color.white));
        tvFront.setTextColor(getResources().getColor(R.color.white));
        tvAD1920.setTextColor(getResources().getColor(R.color.white));
        tvQuozi.setTextColor(getResources().getColor(R.color.white));
        tvCelcius.setTextColor(getResources().getColor(R.color.white));
        tvTexas.setTextColor(getResources().getColor(R.color.white));
        tvGoblin.setTextColor(getResources().getColor(R.color.white));
        tvMellow.setTextColor(getResources().getColor(R.color.white));
        tvBlues.setTextColor(getResources().getColor(R.color.white));
        tvCreamy.setTextColor(getResources().getColor(R.color.white));
        tvSepiaHigh.setTextColor(getResources().getColor(R.color.white));
        tvClassic.setTextColor(getResources().getColor(R.color.white));
        tvPlate.setTextColor(getResources().getColor(R.color.white));
        tvPitched.setTextColor(getResources().getColor(R.color.white));
        tvNeat.setTextColor(getResources().getColor(R.color.white));

    }

    private void sendEvent(String name) {
//        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        Bundle params = new Bundle();
//        params.putBoolean("success", true);
//        params.putString("name", name);
//        firebaseAnalytics.logEvent("FILTER", params);

    }


}
