/**
 * CanvasView.java
 * <p/>
 * Copyright (c) 2014 Tomohiro IKEDA (Korilakkuma)
 * Released under the MIT license
 */

package com.solidict.cropysdk.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;


import com.solidict.cropysdk.photoView.IPhotoView;
import com.solidict.cropysdk.photoView.PhotoViewAttacherCompx;
import com.solidict.cropysdk.R;
import com.solidict.cropysdk.ShareActivity;
import com.solidict.cropysdk.interfaces.CapsTouchedListener;
import com.solidict.cropysdk.interfaces.MatrixListener;
import com.solidict.cropysdk.interfaces.PathListener;
import com.solidict.cropysdk.interfaces.TextTouchListener;
import com.solidict.cropysdk.log.LogManager;
import com.solidict.cropysdk.models.FlaotPoint;
import com.solidict.cropysdk.models.SnapShot;
import com.solidict.cropysdk.utils.Utils;

import java.util.ArrayList;
import java.util.List;



/**
 * This class defines fields and methods for drawing.
 */
public class CanvasView extends ImageView implements IPhotoView, CapsTouchedListener {

    private SnapShot currentSnapShot = new SnapShot();

    public SnapShot getCurrentSnapShot() {
        return currentSnapShot;
    }

    public void setCurrentSnapShot(SnapShot currentSnapShot) {
        this.currentSnapShot = currentSnapShot;
    }

    @Override
    public void onCapsTouched() {
        CapsTouchedListener capsTouchedListener = (CapsTouchedListener) getContext();
        capsTouchedListener.onCapsTouched();
    }

    // Enumeration for Mode
    public enum Mode {
        DRAW,
        TEXT,
        ERASER, EMPTY
    }

    // Enumeration for Drawer
    public enum Drawer {
        PEN,
        LINE,
        RECTANGLE,
        CIRCLE,
        ELLIPSE,
        QUADRATIC_BEZIER,
        QUBIC_BEZIER, ARROW, FRAME, TEXT, FILTER, PIXELATE, FOCUS, ADJUSTMENT, ROTATE, CAPS
    }

    private Context context = null;
    private Canvas canvas = null;


    private List<Path> pathLists = new ArrayList<Path>();
    private List<Paint> paintLists = new ArrayList<Paint>();
    private List<ArrayList<Region>> regionList = new ArrayList<>();


    // for Eraser
    private int baseColor = Color.WHITE;

    // for Undo, Redo
    private int historyPointer = 0;

    // Flags
    private Mode mode = Mode.EMPTY;
    private Drawer drawer = Drawer.PEN;
    private boolean isDown = false;

    // for Paint
    private Paint.Style paintStyle = Paint.Style.STROKE;
    private int paintStrokeColor = Color.BLACK;
    private int paintFillColor = Color.BLACK;
    private float paintStrokeWidth = 3F;
    private int opacity = 255;
    private float blur = 0F;
    private Paint.Cap lineCap = Paint.Cap.ROUND;

    // for Text
    private String text = "";
    private Typeface fontFamily = Typeface.DEFAULT;
    private float fontSize = 32F;
    private Paint.Align textAlign = Paint.Align.LEFT;  // fixed
    private Paint textPaint = new Paint();
    private float textX = 0F;
    private float textY = 0F;

    // for Drawer
    private float startX = 0F;
    private float startY = 0F;
    private float controlX = 0F;
    private float controlY = 0F;

    private float bitmapX;
    private float bitmapY;
    private float bitmapWidth;
    private float bitmapHeight;
    private float bitmapScale;

    private boolean underLined;

    float tempTouchY = -1;
    float diffY = 0;
    int touchIndex = -1;

    boolean onDown = false;
    private Bitmap originalBitmap;

    boolean isTyping = false;
    private float textWidth = 0;
    private float textHeight = 0;


    private boolean drawMode = false;


    public ArrayList<String> capsList = new ArrayList<>();
    public ArrayList<String> textList = new ArrayList<>();


    private int capsSize = 24;

    public Bitmap cachedBitmap;
    private boolean bitmapChanged = false;
    boolean textTouched = false;
    private boolean textTouch = false;


    float scaleFactor = 1;
    float focusX = 0;
    float focusY = 0;
    RectF rect;
    private String capsText = "";


    public void recycleBitmaps() {
        originalBitmap.recycle();
        originalBitmap = null;
        cachedBitmap.recycle();
        cachedBitmap = null;
        blurredBitmap.recycle();
        blurredBitmap = null;
        mosaicBitmap.recycle();
        mosaicBitmap = null;
    }


    //region gettersAndSetters


    public float getTextX() {
        return textX;
    }

    public void setTextX(float textX) {
        this.textX = textX;
    }

    public float getTextY() {
        return textY;
    }

    public int getHistoryPointer() {
        return historyPointer;
    }

    public void setHistoryPointer(int historyPointer) {
        this.historyPointer = historyPointer;
    }

    public int getCapsSize() {
        return capsSize;
    }

    public List<Path> getPathLists() {
        return pathLists;
    }

    public void setPathLists(List<Path> pathLists) {
        this.pathLists = pathLists;
    }

    public List<Paint> getPaintLists() {
        return paintLists;
    }

    public void setPaintLists(List<Paint> paintLists) {
        this.paintLists = paintLists;
    }

    public List<ArrayList<Region>> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<ArrayList<Region>> regionList) {
        this.regionList = regionList;
    }

    public ArrayList<String> getCapsList() {
        return capsList;
    }

    public void setCapsList(ArrayList<String> capsList) {
        this.capsList = capsList;
    }

    public ArrayList<String> getTextList() {
        return textList;
    }

    public void setTextList(ArrayList<String> textList) {
        this.textList = textList;
    }

    public void setTextY(float textY) {
        this.textY = textY;
    }

    public String getCapsText() {
        return capsText;
    }

    public void setCapsText(String capsText) {
        this.capsText = capsText;
    }

    public void setTempTouchY(float tempTouchY) {
        this.tempTouchY = tempTouchY;
    }


    public float getStartY() {
        return startY;
    }

    public void setStartY(float startY) {
        this.startY = startY;
    }

    public boolean isUnderLined() {
        return underLined;
    }

    public void setUnderLined(boolean underLined) {
        this.underLined = underLined;
    }

    public float getBitmapScale() {
        return bitmapScale;
    }

    public void setBitmapScale(float bitmapScale) {
        this.bitmapScale = bitmapScale;
    }

    public float getBitmapX() {
        return bitmapX;
    }

    public void setBitmapX(float bitmapX) {
        this.bitmapX = bitmapX;
    }

    public float getBitmapY() {
        return bitmapY;
    }

    public void setBitmapY(float bitmapY) {
        this.bitmapY = bitmapY;
    }

    public float getBitmapWidth() {
        return bitmapWidth;
    }

    public void setBitmapWidth(float bitmapWidth) {
        this.bitmapWidth = bitmapWidth;
    }

    public float getBitmapHeight() {
        return bitmapHeight;
    }

    public void setBitmapHeight(float bitmapHeight) {
        this.bitmapHeight = bitmapHeight;
    }

    /**
     * This method gets the instance of Path that pointer indicates.
     *
     * @return the instance of Path
     */
    private Path getCurrentPath() {
        return this.pathLists.get(this.historyPointer - 1);
    }


    public void setOriginalBitmap(Bitmap originalBitmap) {
        this.originalBitmap = originalBitmap;
    }

    public Bitmap getOriginalBitmap() {
        return originalBitmap;
    }


    public boolean isTextTouch() {
        return textTouch;
    }

    public void setTextTouch(boolean textTouch) {
        this.textTouch = textTouch;
    }


    public boolean isDrawMode() {
        return drawMode;
    }

    public void setDrawMode(boolean drawMode) {
        this.drawMode = drawMode;
    }


    /**
     * This method is getter for mode.
     *
     * @return
     */
    public Mode getMode() {
        return this.mode;
    }

    /**
     * This method is setter for mode.
     *
     * @param mode
     */
    public void setMode(Mode mode) {
        this.mode = mode;

        if (mode == Mode.TEXT) {
            textTouched = false;
        }
    }


    /**
     * This method is getter for drawer.
     *
     * @return
     */
    public Drawer getDrawer() {
        return this.drawer;
    }

    /**
     * This method is setter for drawer.
     *
     * @param drawer
     */
    public void setDrawer(Drawer drawer) {
        this.drawer = drawer;
    }


    public boolean isTyping() {
        return isTyping;
    }

    public void setTyping(boolean typing) {
        isTyping = typing;
    }


    public void setCapsSize(int capsSize) {
        this.capsSize = capsSize;
    }

    public void setCachedBitmap(Bitmap cachedBitmap) {
        this.cachedBitmap = cachedBitmap;
    }

    public void setBitmapChanged(boolean bitmapChanged) {
        this.bitmapChanged = bitmapChanged;
    }

    /**
     * This method gets current canvas as bitmap.
     *
     * @return This is returned as bitmap.
     */
    public Bitmap getBitmap() {

        this.setDrawingCacheEnabled(false);
        this.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(this.getDrawingCache());
        if (bitmap != null)
            return bitmap;
        else
            return null;
    }

    //endregion gettersAndSetters

    //region init

    public CanvasView(Context context) {
        this(context, null);
        Log.d("logHistory", " cons 1");
        init();
        this.setup(context);

    }

    public CanvasView(Context context, AttributeSet attr) {
        this(context, attr, -1);
        Log.d("logHistory", " cons 2");
    }

    public CanvasView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        super.setScaleType(ScaleType.MATRIX);
        if (defStyle != -1) {
            Log.d("logHistory", " cons 3");
            init();
        }
    }


    protected void init() {
        if (null == mAttacher || null == mAttacher.getImageView()) {
            mAttacher = new PhotoViewAttacherCompx(this);
            mAttacher.setMaximumScale(10);
            mAttacher.setZoomable(true);
        }

        if (null != mPendingScaleType) {
            setScaleType(mPendingScaleType);
            mPendingScaleType = null;
        }
        this.setup(context);
    }


    /**
     * Common initialization.
     *
     * @param context
     */
    private void setup(final Context context) {
        this.context = context;

        Log.d("logHistory", "setup hp: " + historyPointer);

        this.pathLists.add(new Path());
        this.regionList.add(new ArrayList<Region>());
        this.paintLists.add(this.createPaint());
        this.historyPointer++;


//        mAttacher.setContext(getContext());
        mAttacher.setCapsStartingPoint(capsStartingPoint);

        mAttacher.switchToZoom();


        mAttacher.setOnScaleChangeListener(new PhotoViewAttacherCompx.OnScaleChangeListener() {
            @Override
            public void onScaleChange(float mScaleFactor, float focusX, float focusY) {

                Log.d("logScale", "onScaleChange");

                if (scaleFactor != mScaleFactor) {
                    scaleFactor *= mScaleFactor;
                    CanvasView.this.focusX = focusX;
                    CanvasView.this.focusY = focusY;
                }


            }
        });


        mAttacher.setOnMatrixChangeListener(new PhotoViewAttacherCompx.OnMatrixChangedListener() {
            @Override
            public void onMatrixChanged(RectF rectF) {
                Log.d("logScale", "rectF.top: " + rectF.top);

                if (rect != null) {
                    for (int i = 0; i < regionList.size(); i++) {
                        pathLists.get(i).offset(0, rectF.top - diffY);

                        RectF e = new RectF();
                        Path currentPath = pathLists.get(i);
                        currentPath.computeBounds(e, true);

//                        regionList.get(i).setPath(currentPath, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom));

                        for (int j = 0; j < regionList.get(i).size(); j++) {

                            Rect regionBounds = regionList.get(i).get(j).getBounds();

                            regionList.get(i).get(j).set(regionBounds.left, (int) (regionBounds.top + rectF.top - diffY), regionBounds.right, (int) (regionBounds.bottom + rectF.top - diffY));
                        }

                        MatrixListener matrixListener = (MatrixListener) getContext();
                        matrixListener.onMatrixChanged((int) -rectF.top);

                    }
                }

                rect = rectF;
                diffY = rectF.top;

            }
        });


    }

    //endregion init

    //region actions


    /**
     * This method set event listener for drawing.
     *
     * @param event the instance of MotionEvent
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);


        Log.d("onTouchEvent", "onTouchEventt");


        if (drawMode) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    this.onActionDown(event);
                    break;
                case MotionEvent.ACTION_MOVE:
                    this.onActionMove(event);
                    break;
                case MotionEvent.ACTION_UP:
                    this.onActionUp(event);
                    break;
                default:
                    break;
            }

            // Re draw
            this.invalidate();

            return true;

        } else {
            return false;
        }
    }


    /**
     * This method defines processes on MotionEvent.ACTION_DOWN
     *
     * @param event This is argument of onTouchEvent method
     */
    private void onActionDown(MotionEvent event) {

        onDown = true;


        Log.d("logCanvasMode", "this.mode: " + this.mode);
        Log.d("logCanvasMode", "this.drawer: " + this.drawer);


        if (capsStartingPoint != -1 && event.getY() > capsStartingPoint) {
            Log.d("logCapsCanvas", "inside event.getY(): " + event.getY() + ", diffY: " + diffY + ", bitmapY: " + bitmapY + ", capsStartingPoint: " + capsStartingPoint);

            CapsTouchedListener capsTouchedListener = (CapsTouchedListener) getContext();
            capsTouchedListener.onCapsTouched();

        } else {
            Log.d("logCapsCanvas", "outside event.getY(): " + event.getY() + ", diffY: " + diffY + ", bitmapY: " + bitmapY + ", capsStartingPoint: " + capsStartingPoint);


            switch (this.mode) {
                case DRAW:
                case ERASER:


                    boolean contains = false;
                    for (int i = 0; i < regionList.size(); i++) {

                        Log.d("logPathRectSize", "regionList.size(): " + regionList.size());
                        Log.d("logPathRectSize", "pathLists.size(): " + pathLists.size());
                        Log.d("logPathRectSize", "paintLists.size(): " + paintLists.size());


//                    canvas.drawRect(regionList.get(i).getBounds(), paintLists.get(i));
//                    Log.d("logPathRegion", "down l: " + regionList.get(i).getBounds().left + ", t: " + regionList.get(i).getBounds().top + ", r: " + regionList.get(i).getBounds().right + ", b: " + regionList.get(i).getBounds().bottom);

                        for (int j = 0; j < regionList.get(i).size(); j++) {


                            if (regionList.get(i).size() != 0 && regionList.get(i).get(j).contains((int) event.getX(), (int) event.getY())) {

//                            if (paintLists.get(i).getXfermode() == null) {
//                                contains = true;
//                                touchIndex = i;
//                            }

                                if (paintLists.get(i).getAlpha() != 254) {
                                    Log.d("logPathRect", "contains index: " + i);
                                    contains = true;
                                    touchIndex = i;
                                }


//                        paint.getStrokeWidth()

                            } else
                                Log.d("logPathRect", "doesn't contain");


                            Log.d("logPixel", "Xfermode: " + paintLists.get(i).getXfermode());
                            Log.d("logPixel", "StrokeWidth: " + paintLists.get(i).getStrokeWidth());

                        }

                    }

                    if (contains) {
                        this.startX = event.getX();
                        this.startY = event.getY();
                        Log.d("logMove", "onActionDown move");

                        LogManager.addLog("Editing Screen - Object moved");

                    } else {
                        Log.d("logMove", "onActionDown draw");

                        if ((this.drawer != Drawer.QUADRATIC_BEZIER) && (this.drawer != Drawer.QUBIC_BEZIER)) {
                            // Oherwise
                            this.updateHistory(this.createPath(event));
                            this.isDown = true;
                        } else {
                            // Bezier
                            if ((this.startX == 0F) && (this.startY == 0F)) {
                                // The 1st tap
                                this.updateHistory(this.createPath(event));
                            } else {
                                // The 2nd tap
                                this.controlX = event.getX();
                                this.controlY = event.getY();

                                this.isDown = true;
                            }
                        }
                    }


                    break;
                case TEXT:
                    this.startX = event.getX() - textWidth / 2;
                    this.startY = event.getY() + textHeight / 2 - diffY;


                    setTempTouchY(event.getY());

                    if (!textTouched) {
                        Log.d("logTextEvent", "onActionDown text textTouched: " + textTouched);

                        TextTouchListener textTouchListener = (TextTouchListener) getContext();
                        textTouchListener.onTextTouch(event.getY());
                        textTouched = true;
                    } else {
//                        Log.d("logTextEvent", "onActionDown text textTouched: " + textTouched);
//
//                        if (System.currentTimeMillis() - textTouchMillis < 500) {
//                            TextTouchListener textTouchListener = (TextTouchListener) getContext();
//                            textTouchListener.onTextTouch(event.getY());
//                            textTouched = true;
//                            textTouchMillis = 0;
//                        } else {
//                            textTouchMillis = System.currentTimeMillis();
//
//                        }

                    }


                    break;
                default:
                    break;
            }
        }

    }

    boolean shadowEnabled = false;

    long textTouchMillis = 0;

    /**
     * This method defines processes on MotionEvent.ACTION_MOVE
     *
     * @param event This is argument of onTouchEvent method
     */

    private void onActionMove(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        Log.d("onActionMoveDrawer", "drawer: " + drawer);

        if (drawer == Drawer.FOCUS && mode != Mode.TEXT) {

            circleX = x;
            circleY = y;
        }


        switch (this.mode) {
            case DRAW:
            case ERASER:

                if (touchIndex != -1) {

                    shadowEnabled = true;

                    pathLists.get(touchIndex).offset(x - this.startX, y - this.startY);


                    RectF rectF = new RectF();
                    Path currentPath = pathLists.get(touchIndex);
                    currentPath.computeBounds(rectF, true);

                    //todo offset
//                    regionList.get(touchIndex).setPath(currentPath, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom));


                    for (int i = 0; i < regionList.get(touchIndex).size(); i++) {

                        Rect regionBounds = regionList.get(touchIndex).get(i).getBounds();

                        regionList.get(touchIndex).get(i).set((int) (regionBounds.left + (x - this.startX)), (int) (regionBounds.top + y - this.startY), (int) (regionBounds.right + x - this.startX), (int) (regionBounds.bottom + y - this.startY));
                    }

                    this.startX = x;
                    this.startY = y;


//                    if (rect.top <= bitmapY || rect.bottom >= bitmapY + bitmapHeight || rect.left <= bitmapX || rect.right >= bitmapX + bitmapWidth) {
//                        onActionUp(event);
//                        Log.d("logBound", "in");
//                    }

                    Log.d("logMove", "onActionMove move");
                } else {

                    Log.d("logMove", "onActionMove draw");

                    Log.d("logMove", "x: " + x + ", y: " + y);

//                    if (y < bitmapY || y > bitmapY + bitmapHeight || x < bitmapX || x > bitmapX + bitmapWidth) {
//                        Log.d("logBounds", "Out");
//                        onActionUp(event);
//
//                    }
//                    else {
                    Log.d("logBounds", "In");
                    if ((this.drawer != Drawer.QUADRATIC_BEZIER) && (this.drawer != Drawer.QUBIC_BEZIER)) {
                        if (!isDown) {
                            return;
                        }

                        Path path = this.getCurrentPath();

                        switch (this.drawer) {
                            case PEN:


                                for (int i = 0; i < event.getHistorySize(); i++) {

                                    Log.d("logHistoryX", "x: " + event.getHistoricalX(i) + ", y: " + event.getHistoricalY(i));


                                    path.lineTo(event.getHistoricalX(i), event.getHistoricalY(i));

                                }

//                                path.lineTo(x, y);


//                                int tempColor = getPaintStrokeColor();
//                                setPaintStrokeColor(Color.parseColor("#000000"));
//                                path.lineTo(x + 50, y + 50);
//                                path.moveTo(x, y);
//                                setPaintStrokeColor(tempColor);


                                break;
                            case LINE:
                                path.reset();
                                path.moveTo(this.startX, this.startY);
                                path.lineTo(x, y);
                                break;
                            case ARROW:

                                drawArrow(path, x, y);

                                break;


                            case RECTANGLE:

//                            Log.d("logRect", "startX: " + this.startX + ", startY: " + this.startY + ", x: " + x + ", y: " + y);
//                            path.reset();
//                            path.addRect(this.startX, this.startY, x, y, Path.Direction.CCW);


                                path.reset();
                                float sX = this.startX;
                                float sY = this.startY;
                                float eX = x;
                                float eY = y;
                                if (sX > eX) {
                                    sX = x;
                                    eX = this.startX;
                                }
                                if (sY > eY) {
                                    sY = y;
                                    eY = this.startY;
                                }
                                path.addRect(sX, sY, eX, eY, Path.Direction.CCW);

                                break;

                            case CIRCLE:
                                double distanceX = Math.abs((double) (this.startX - x));
                                double distanceY = Math.abs((double) (this.startX - y));
                                double radius = Math.sqrt(Math.pow(distanceX, 2.0) + Math.pow(distanceY, 2.0));

                                path.reset();
                                path.addCircle(this.startX, this.startY, (float) radius, Path.Direction.CCW);
                                break;
                            case ELLIPSE:
                                RectF rect = new RectF(this.startX, this.startY, x, y);

                                path.reset();
                                path.addOval(rect, Path.Direction.CCW);

                                break;
                            default:
                                break;
                        }
                    } else {
                        if (!isDown) {
                            return;
                        }

                        Path path = this.getCurrentPath();

                        path.reset();
                        path.moveTo(this.startX, this.startY);
                        path.quadTo(this.controlX, this.controlY, x, y);


                    }
//                    }


                }


                break;
            case TEXT:
                this.startX = event.getX() - textWidth / 2;
                this.startY = event.getY() + textHeight / 2 - diffY;


                break;
            default:
                break;
        }
    }

    private FlaotPoint[] getPoints(Path path0) {


        PathMeasure pm = new PathMeasure(path0, false);
        float length = pm.getLength();

        FlaotPoint[] pointArray = new FlaotPoint[(int) (length / 1)];
        float distance = 0f;
        float speed = length / pointArray.length;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < pointArray.length)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);
            pointArray[counter] = new FlaotPoint(aCoordinates[0],
                    aCoordinates[1]);
            counter++;
            distance = distance + speed;
        }

        Log.d("getPoints", "getPoints: " + pointArray.length);

        return pointArray;
    }

    /**
     * This method defines processes on MotionEvent.ACTION_DOWN
     *
     * @param event This is argument of onTouchEvent method
     */
    private void onActionUp(MotionEvent event) {

        onDown = false;
        shadowEnabled = false;


        if (touchIndex == -1) {

            Log.d("logMove", "onActionUp draw");

            if (isDown) {
                this.startX = 0F;
                this.startY = 0F;
                this.isDown = false;
//                canvasActionListener.onActionListener(historyPointer);


//                Log.d("logPixelate", "mode: " + mode);

                RectF rectF = new RectF();
                Path currentPath = getCurrentPath();
                currentPath.computeBounds(rectF, true);
//                Region region = new Region();


                Path tempPath = new Path();

                if (paintLists.get(paintLists.size() - 1).getAlpha() == 254) {

                    Log.d("logPixelate", "mode: " + mode);

                    ArrayList<Region> regionArrayList = new ArrayList<>();
                    regionList.add(regionArrayList);


                } else {

//                    region.setPath(currentPath, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom));

                    FlaotPoint[] pointArray = getPoints(currentPath);

                    ArrayList<Region> regionArrayList = new ArrayList<>();

                    for (int i = 0; i < pointArray.length; i++) {

                        int padding = 30;

                        Log.d("logFlaotPoints", "x: " + pointArray[i].getX() + ", y: " + pointArray[i].getY());
                        Region region = new Region((int) pointArray[i].getX() - padding, (int) pointArray[i].getY() - padding, (int) pointArray[i].getX() + padding, (int) pointArray[i].getY() + padding);
                        regionArrayList.add(region);

                    }

                    regionList.add(regionArrayList);

                    Log.d("logPixelate", "mode: " + mode);

                }

//                region.op(region, Region.Op.INTERSECT);


//                Log.d("logPathRegion", "added l: " + region.getBounds().left + ", t: " + region.getBounds().top + ", r: " + region.getBounds().right + ", b: " + region.getBounds().bottom);


            }

            PathListener pathListener = (ShareActivity) getContext();
            pathListener.onPathChanged(pathLists, regionList, paintLists);

        }

        {
//            this.startX = event.getX();
//            this.startY = event.getY();
            Log.d("logMove", "onActionUp move");

            this.startX = event.getX() - textWidth / 2;
            this.startY = event.getY() + textHeight / 2 - diffY;


//            RectF rectF = new RectF();
//            Path currentPath = pathLists.get(touchIndex);
//            currentPath.computeBounds(rectF, true);
//            regionList.get(touchIndex).setPath(currentPath, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom));

            touchIndex = -1;

        }


    }

    //endregion actions

    Bitmap blurredBitmap;

    public void setBlurredBitmap(Bitmap blurredBitmap) {
        this.blurredBitmap = blurredBitmap;

    }

    float circleX = -1;
    float circleY = -1;
    float circleRadius = 150;
    int circleBitmapAlpha = 202;
    boolean focusEnabled = false;

    public boolean isFocusEnabled() {
        return focusEnabled;
    }

    public void setFocusEnabled(boolean focusEnabled) {
        this.focusEnabled = focusEnabled;
    }

    public void setCircleBitmapAlpha(int circleBitmapAlpha) {
        this.circleBitmapAlpha = circleBitmapAlpha;
    }

    public void setCircleX(float circleX) {
        this.circleX = circleX;
    }

    public void setCircleY(float circleY) {
        this.circleY = circleY;
    }

    public float getCircleY() {
        return circleY;
    }

    public void setCircleRadius(float circleRadius) {
        this.circleRadius = circleRadius;
    }

    //region drawing

    float capsStartingPoint = -1;

    /**
     * This method updates the instance of Canvas (View)
     *
     * @param canvas the new instance of Canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        if (bitmapChanged) {
            cachedBitmap = applyFilter(originalBitmap, currentSnapShot.getFilterId());
            cachedBitmap = changeBitmapContrastBrightness(cachedBitmap, currentSnapShot.getContrastValue(), currentSnapShot.getBrightnessValue(), false);
            ShareActivity shareActivity = (ShareActivity) getContext();
            shareActivity.reDrawOtherBitmaps();

        }
        bitmapChanged = false;
        Rect src = new Rect(0, 0, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight));
        Rect dest = new Rect(0, (int) diffY, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight + diffY));
        if (cachedBitmap != null) {
            canvas.drawBitmap(cachedBitmap, src, dest, null);
        }

        if (blurredBitmap != null && focusEnabled)
            canvas.drawBitmap(clipBitmap(blurredBitmap), src, dest, null);

        Log.d("logOnDraw", "drawings historyPointer: " + historyPointer);

        for (int i = 0; i < this.historyPointer; i++) {
            Path path = this.pathLists.get(i);
            Paint paint = this.paintLists.get(i);
            if (shadowEnabled && touchIndex == i)
                paint.setShadowLayer(30, 0, 0, Color.BLACK);
            else
                paint.clearShadowLayer();
            canvas.drawPath(path, paint);


        }

        capsStartingPoint = -1;
        mAttacher.setCapsStartingPoint(-1);


        this.canvas = canvas;
        this.drawText(canvas);


        if (currentSnapShot.getFrameId() != -1) {
            applyFrame(currentSnapShot.getFrameId());
        }

        if (!currentSnapShot.getCapsText().equals("") && capsList.size() != 0) {
            int padding = 40;
            int numberOfLine = capsList.size();


            Rect boundsHeight = new Rect();
            textPaint.setTextSize(Utils.convertSpToPixels(capsSize, getContext()));
            textPaint.getTextBounds("l", 0, 1, boundsHeight);
            float text_height = boundsHeight.height();


            Paint mPaint = new Paint();

            Rect r = new Rect();
            mPaint.getTextBounds(currentSnapShot.getCapsText(), 0, 1, r);
            float xPos = Utils.getScreenWidth((ShareActivity) getContext()) / 2 - (Math.abs(r.width()) / 2);

            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(Color.RED);
            capsStartingPoint = (int) (bitmapHeight - (text_height * numberOfLine + 2 * numberOfLine * padding) + diffY);
            mAttacher.setCapsStartingPoint(capsStartingPoint);
            canvas.drawRect(0, capsStartingPoint, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight + diffY), mPaint);

            mPaint.setColor(Color.WHITE);
            mPaint.setFakeBoldText(true);
            mPaint.setTextSize(Utils.convertSpToPixels(capsSize, getContext()));
            mPaint.setTextAlign(Paint.Align.CENTER);

            Typeface regular = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/Turkcell_Satura_Regular.ttf");
            mPaint.setTypeface(regular);


            for (int i = 0; i < capsList.size(); i++) {
                canvas.drawText(capsList.get((capsList.size() - 1) - i).toString(), xPos, bitmapHeight - ((2 * i + 1) * padding + text_height * i) + diffY, mPaint);

            }


        }


    }


    /**
     * This method draws text.
     *
     * @param canvas the instance of Canvas
     */
    public void drawText(Canvas canvas) {


        if (!isTyping) {
            Log.d("historyPointer", "isTyping false currentSnapShot.getText(): " + currentSnapShot.getText() + " , this.text: " + this.text);
            this.text = currentSnapShot.getText();
        } else {
            Log.d("historyPointer", "isTyping true currentSnapShot.getText(): " + currentSnapShot.getText() + " , this.text: " + this.text);

        }


//        if (this.text.length() <= 0) {
//            return;
//        }


        if (this.mode == Mode.TEXT) {

//            if (this.textX == 0 && this.textY == 0) {
//                this.textX = this.startX;
//                this.textY = this.startY;
//            }

            Log.d("textTouched", "textTouched: " + textTouched);

            if (textTouched) {
                this.textX = this.startX;
                this.textY = this.startY;
            }

            if (this.startX == 0)
                this.startX = Utils.getScreenWidth((ShareActivity) getContext()) / 2;

//            this.textPaint = this.createPaint();
        }


//        this.textPaint = this.createPaint();


        float textX = this.textX;
        float textY = this.textY;

        Log.d("logText", "text: " + this.text);

        boolean empty = text.isEmpty();

        if (empty)
            this.textPaint = this.createPaint();


//            textX -= numChars * lengthOfChar / 2;


        textPaint.setUnderlineText(underLined);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTextSize(Utils.convertDpToPixel(getPaintStrokeWidth() * 2, getContext()));


//            if (mode == Mode.TEXT && empty){
//                empty=false;
//                text = getResources().getString(R.string.enter_text);
//
//            }


        String tempText = text.isEmpty() ? getResources().getString(R.string.enter_text_lower_case) : text;

        boolean hasNewline;
        if (!text.isEmpty()) {
            String newline = System.getProperty("line.separator");
            hasNewline = text.substring(text.length() - 1, text.length()).contains(newline);
            Log.d("logNewline", "hasNewline: " + hasNewline);
        } else {
            hasNewline = false;

        }


        Rect bounds = new Rect();
        textPaint.getTextBounds(tempText, 0, tempText.length(), bounds);

        Rect boundsHeight = new Rect();
        textPaint.getTextBounds("l", 0, 1, boundsHeight);

        float text_height = boundsHeight.height();
        float text_width = bounds.width();

        Log.d("logText", "text_width: " + text_width + " text_height " + text_height);


        float maxLength = 0;
        for (int i = 0; i < textList.size(); i++) {
            Rect boundsLine = new Rect();
            textPaint.getTextBounds(textList.get(i), 0, textList.get(i).length(), boundsLine);
            float width = boundsLine.width();
            if (maxLength < width)
                maxLength = width;
        }

        int padding = 30;

        if (maxLength > 1) {
            text_width = maxLength;
        }

        this.textWidth = text_width;
        this.textHeight = text_height;


        if (tempTouchY > 0) {
            Paint mPaint = new Paint();
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(getResources().getColor(R.color.darkGray));
            mPaint.setStrokeWidth(4f);
//            canvas.drawRect(textX - 30, this.startY - text_height - 30 + diffY, textX + text_width + 30, this.startY + 50 + diffY, mPaint);


            if (textList.isEmpty()) {
                textList.add("");
            }

            if (hasNewline)
                canvas.drawRect(textX - padding, textY - text_height - padding + diffY, textX + text_width + padding, textY + diffY + (text_height * (textList.size() == 0 ? 1 : textList.size())) + (padding * (textList.size() * 2)), mPaint);
            else
                canvas.drawRect(textX - padding, textY - text_height - padding + diffY, textX + text_width + padding, textY + diffY + (text_height * (textList.size() == 0 ? 1 : textList.size() - 1)) + (padding * (textList.size() * 2 - 1)), mPaint);


            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setAlpha(125);
            if (hasNewline)
                canvas.drawRect(textX - padding, textY - text_height - padding + diffY, textX + text_width + padding, textY + diffY + (text_height * (textList.size() == 0 ? 1 : textList.size())) + (padding * (textList.size() * 2)), mPaint);
            else
                canvas.drawRect(textX - padding, textY - text_height - padding + diffY, textX + text_width + padding, textY + diffY + (text_height * (textList.size() == 0 ? 1 : textList.size() - 1)) + (padding * (textList.size() * 2 - 1)), mPaint);
        }

        if (tempTouchY > 0 && empty) {
            canvas.drawText(getResources().getString(R.string.enter_text_lower_case), textX, textY + diffY, this.textPaint);
        } else {
//            canvas.drawText(text, textX, textY + diffY, this.textPaint);

            for (int i = 0; i < textList.size(); i++) {
                canvas.drawText(textList.get(i), textX, textY + diffY + i * (textHeight + 2 * padding), this.textPaint);
            }


        }


    }

    boolean focusDrawModeEnabled = false;

    public boolean isFocusDrawModeEnabled() {
        return focusDrawModeEnabled;
    }

    public void setFocusDrawModeEnabled(boolean focusDrawModeEnabled) {
        this.focusDrawModeEnabled = focusDrawModeEnabled;
    }

    private Bitmap clipBitmap(Bitmap bitmap) {
        Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth() * 5, bitmap.getHeight() * 5, Bitmap.Config.ARGB_8888);


        Paint mBackgroundPaint = new Paint();
        mBackgroundPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));


        Canvas c = new Canvas(bmOverlay);

        c.drawRect(0, 0, bmOverlay.getWidth(), bmOverlay.getHeight(), mBackgroundPaint);


//        c.drawBitmap(bitmap, 0, 0, null);


        Rect source = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        Rect dest = new Rect(0, 0, (bitmap.getWidth() * 5), (bitmap.getHeight() * 5));
        Paint bitmapPaint = new Paint();
        bitmapPaint.setAlpha(circleBitmapAlpha);


//        if (circleY == -1) {
//            circleY = (this.getHeight()) / 2 + diffY;
//            Log.d("logCircleInıt", "1 circleY: " + circleY);
//            circleX = (this.getWidth() / 2);
//
//        } else {
//            Log.d("logCircleInıt", "2 circleY: " + circleY);
//
//        }


        c.drawBitmap(bitmap, source, dest, bitmapPaint);

//        c.drawCircle(circleX, circleY, circleRadius, mBackgroundPaint);
        c.drawCircle(circleX, circleY - diffY, circleRadius, mBackgroundPaint);


        Log.d("Drawer", "focusEnabled: " + focusEnabled);


        if (focusDrawModeEnabled) {
            Paint mPaint = new Paint();
            mPaint.setStrokeWidth(6f);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(getResources().getColor(R.color.yellow));

//        c.drawCircle(circleX, circleY, circleRadius, mPaint);

            Log.d("onFocusLocationChangedd", "circleX: " + circleX + ", circleY: " + circleY + ", bitmapY: " + bitmapY + ", circleRadius: " + circleRadius + ", diffY: " + diffY);


            c.drawCircle(circleX, circleY - diffY, circleRadius, mPaint);
        }


        return bmOverlay;
    }

    //endregion drawing

    //region frameAndFilters

    public Bitmap toGrayscale(Bitmap bmpOriginal) {


        Bitmap bmpGrayscale = Bitmap.createBitmap((int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);

        Rect source = new Rect(0, 0, bmpOriginal.getWidth(), bmpOriginal.getHeight());
        Rect dest = new Rect(0, 0, (int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale));
        c.drawBitmap(bmpOriginal, source, dest, paint);
        return bmpGrayscale;
    }

    public Bitmap applyEffect(int color, Bitmap bmpOriginal) {


        Bitmap bmpGrayscale = Bitmap.createBitmap((int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        if (color != 0)
            cm.setSaturation(0.3f);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);

        Rect source = new Rect(0, 0, bmpOriginal.getWidth(), bmpOriginal.getHeight());
        Rect dest = new Rect(0, 0, (int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale));
        c.drawBitmap(bmpOriginal, source, dest, paint);
        if (color != 0)
            c.drawColor(getResources().getColor(color));
        return bmpGrayscale;
    }

    public Bitmap applyVintageEffect(int color, Bitmap bmpOriginal) {


        Bitmap bmpGrayscale = Bitmap.createBitmap((int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.3f);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);

        Rect source = new Rect(0, 0, bmpOriginal.getWidth(), bmpOriginal.getHeight());
        Rect dest = new Rect(0, 0, (int) (bmpOriginal.getWidth() * bitmapScale), (int) (bmpOriginal.getHeight() * bitmapScale));
        c.drawBitmap(bmpOriginal, source, dest, paint);

        Bitmap bmp = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.ARGB_8888);
        Drawable drawable = getResources().getDrawable(color);
        Canvas canvasNine = new Canvas(bmp);
        drawable.setBounds(0, 0, bmpOriginal.getWidth(), bmpOriginal.getHeight());
        drawable.draw(canvasNine);
        c.drawBitmap(bmp, source, dest, paint);
        c.drawColor(getResources().getColor(R.color.vintage_filter));
        return bmpGrayscale;
    }

    private Bitmap applyFilter(final Bitmap bitmap, int id) {


        switch (id) {
            case 0:
                return applyEffect(0, bitmap);
            case 1:
                return toGrayscale(bitmap);
            case 2:
                return applyEffect(R.color.yellow_sepia, bitmap);
            case 3:
                return applyVintageEffect(R.drawable.filter_vintage, bitmap);
//            case 4:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterFridge()));
//            case 5:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterFront()));
//            case 6:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterAD1920()));
//            case 7:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterQuozi()));
//            case 8:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterCelsius()));
//            case 9:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterTexas()));
//            case 10:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterGobblin()));
//            case 11:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterMellow()));
//            case 12:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterBlues()));
//            case 13:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterCreamy()));
//            case 14:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterSepiahigh()));
//            case 15:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterClassic()));
//            case 16:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterPlate()));
//            case 17:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterPitched()));
//            case 18:
//                return applyEffect(0, ImgLy.getFilterAppliedBitmap(bitmap, new ColorFilterNeat()));
            default:
                return applyEffect(0, bitmap);
        }
    }

    private void applyFrame(int id) {


        if (id >= 0 && id <= 9) {

//            final ArrayList<CustomPatchFrameConfig> frames = ImgLy.createFramesList();
//            final Bitmap frame = ImgLy.getFrameInRequiredSize(cachedBitmap.getWidth(), cachedBitmap.getHeight(), frames.get(id));
//
//            Rect src = new Rect(0, 0, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight));
//            Rect dest = new Rect(0, (int) diffY, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight + diffY));
//            canvas.drawBitmap(frame, src, dest, null);

        } else {

            Activity activity = (ShareActivity) getContext();

            Rect src = new Rect(0, 0, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight));
            Rect dest = new Rect(0, (int) diffY, Utils.getScreenWidth((ShareActivity) getContext()), (int) (bitmapHeight + diffY));

            Bitmap bmp = Bitmap.createBitmap((Utils.getScreenWidth(activity)), (int) getBitmapHeight(), Bitmap.Config.ARGB_8888);
            Drawable drawable = getResources().getDrawable(id);
            Canvas canvasNine = new Canvas(bmp);
            drawable.setBounds(0, 0, (Utils.getScreenWidth(activity)), canvasNine.getHeight());
            drawable.draw(canvasNine);
            canvas.drawBitmap(bmp, src, dest, null);
        }


    }

    private Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness, boolean resize) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(resize ? ((int) (bmp.getWidth() * getBitmapScale())) : (bmp.getWidth()), resize ? ((int) (bmp.getHeight() * getBitmapScale())) : (bmp.getHeight()), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        Rect source = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());
        Rect dest = new Rect(0, 0, resize ? ((int) (bmp.getWidth() * this.getBitmapScale())) : bmp.getWidth(), resize ? ((int) (bmp.getHeight() * this.getBitmapScale())) : bmp.getHeight());
        canvas.drawBitmap(bmp, source, dest, paint);
        return ret;
    }

    //endregion frameAndFilters

    //region unused

    public void switchToZoom() {
        mAttacher.switchToZoom();
    }

    public void switchToEdit() {
        mAttacher.switchToEdit();
    }

    private Bitmap mosaicBitmap;

    public Bitmap getMosaicBitmap() {
        return mosaicBitmap;
    }

    public void setMosaicBitmap(Bitmap filteredBitmap) {

//        Log.d("logMosaicBitmapp", "setMosaicBitmap");

//        if (filteredBitmap.getHeight() < canvas.getHeight()) {
        if (cachedBitmap != null) {

            if (this.getHeight() < cachedBitmap.getHeight()) {
                Bitmap.Config conf = Bitmap.Config.RGB_565; // see other conf types
                Bitmap bitmap = Bitmap.createBitmap(cachedBitmap.getWidth(), cachedBitmap.getHeight(), conf); // this creates a MUTABLE bitmap
                Canvas canvas = new Canvas(bitmap);
                Rect src = new Rect(0, 0, filteredBitmap.getWidth(), filteredBitmap.getHeight());
                Rect dest = new Rect(0, (int) getBitmapY() + (int) diffY, getWidth(), filteredBitmap.getHeight() + (int) getBitmapY() + (int) diffY);
                canvas.drawBitmap(filteredBitmap, src, dest, null);

                filteredBitmap = bitmap;

            } else {
                Bitmap.Config conf = Bitmap.Config.RGB_565; // see other conf types
                Bitmap bitmap = Bitmap.createBitmap(cachedBitmap.getWidth(), this.getHeight(), conf); // this creates a MUTABLE bitmap
                Canvas canvas = new Canvas(bitmap);
                Rect src = new Rect(0, 0, filteredBitmap.getWidth(), filteredBitmap.getHeight());
                Rect dest = new Rect(0, (int) getBitmapY(), getWidth(), filteredBitmap.getHeight() + (int) getBitmapY());
                canvas.drawBitmap(filteredBitmap, src, dest, null);

                filteredBitmap = bitmap;
            }


//            return bitmap;


        }


//        }


        this.mosaicBitmap = filteredBitmap;

//        Log.d("logMosaicBitmapp", "mosaicBitmap: "+mosaicBitmap.getWidth()+"x"+mosaicBitmap.getHeight());
//        Log.d("logMosaicBitmapp", "mosaicBitmap: "+mosaicBitmap.getWidth()+"x"+mosaicBitmap.getHeight());


        refreshMosaicPaint();

    }


    private void refreshMosaicPaint() {
        for (int i = 0; i < paintLists.size(); i++) {
            if (paintLists.get(i).getAlpha() == 254) {
                initPixelatePaint(paintLists.get(i));

            }
        }
    }

    /**
     * This method creates the instance of Paint.
     * In addition, this method sets styles for Paint.
     *
     * @return paint This is returned as the instance of Paint
     */
    private Paint createPaint() {
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        paint.setStyle(this.paintStyle);
        paint.setStrokeWidth(this.paintStrokeWidth);
        paint.setStrokeCap(this.lineCap);
        paint.setStrokeJoin(Paint.Join.MITER);  // fixed

        // for Text
        if (this.mode == Mode.TEXT) {
            paint.setTypeface(this.fontFamily);
            paint.setTextSize(this.fontSize);
            paint.setTextAlign(this.textAlign);
            paint.setStrokeWidth(0F);
        }

        if (this.mode == Mode.ERASER) {

            Log.d("logEraser", "mode: ERASER");

//            // Eraser
//            setWillNotDraw(false);
//            setLayerType(LAYER_TYPE_HARDWARE, null);
//
////            if (android.os.Build.VERSION.SDK_INT >= 11)
////            {
////                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
////            }
////
//
//            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//            paint.setStrokeWidth(100);


//            int ww = originalBitmap.getWidth();
//            int hh = originalBitmap.getHeight();
//            float scaleWidth = ((float) this.getWidth()) / ww;
//            float scaleHeight = ((float) this.getHeight()) / hh;
//            Matrix matrix = new Matrix();
//            matrix.postScale(scaleWidth, scaleHeight);
//            Bitmap masScaledbitmap = Bitmap.createBitmap(blurredBitmap, 0, 0, ww, hh, matrix, true);

            initPixelatePaint(paint);

//            BitmapShader mBitmapShader = new BitmapShader(mosaicBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
//            paint.setShader(mBitmapShader);
//            paint.setStrokeWidth(40);
//            paint.setAlpha(254);

        } else {

            Log.d("logEraser", "mode: DRAW");

            // Otherwise
            paint.setColor(this.paintStrokeColor);
            paint.setShadowLayer(this.blur, 0F, 0F, this.paintStrokeColor);
            paint.setAlpha(this.opacity);
            paint.setStrokeWidth(this.paintStrokeWidth);

        }

        return paint;
    }

    private void initPixelatePaint(Paint paint) {
        BitmapShader mBitmapShader = new BitmapShader(mosaicBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        Log.d("logPixelatePaint", "mosaicBitmap: " + mosaicBitmap.getWidth() + "x" + mosaicBitmap.getHeight() + ", cachedBitmap: " + cachedBitmap.getWidth() + "x" + cachedBitmap.getHeight());


        paint.setShader(mBitmapShader);
        paint.setStrokeWidth(40);
        paint.setAlpha(254);
    }


    /**
     * This method initialize Path.
     * Namely, this method creates the instance of Path,
     * and moves current position.
     *
     * @param event This is argument of onTouchEvent method
     * @return path This is returned as the instance of Path
     */
    private Path createPath(MotionEvent event) {
        Path path = new Path();

        // Save for ACTION_MOVE
        this.startX = event.getX();
        this.startY = event.getY();

        path.moveTo(this.startX, this.startY);

        return path;
    }


    private void updateHistory(Path path) {
        if (this.historyPointer == this.pathLists.size()) {


            this.pathLists.add(path);
            this.paintLists.add(this.createPaint());
            this.historyPointer++;
        } else {


            // On the way of Undo or Redo
            this.pathLists.set(this.historyPointer, path);
            this.paintLists.set(this.historyPointer, this.createPaint());
            this.historyPointer++;

            for (int i = this.historyPointer, size = this.paintLists.size(); i < size; i++) {
                this.pathLists.remove(this.historyPointer);
                this.paintLists.remove(this.historyPointer);
            }
        }
    }


    private void drawArrow(Path path, float x, float y) {
        setPaintFillColor(getPaintStrokeColor());
        setPaintStyle(Paint.Style.STROKE);
        path.setFillType(Path.FillType.EVEN_ODD);
        path.reset();


        PointF p = new PointF(x, y);
        int dx = (int) (x - this.startX);
        int dy = (int) (y - this.startY);

        Log.d("logArrow", "dx: " + dx + ", dy: " + dy);

        int degree = 135;
        double cos = Math.cos(Math.toRadians(degree));
        double sin = Math.sin(Math.toRadians(degree));

        PointF end1 = new PointF(
                (float) (this.startX + (dx * cos + dy * -sin)),
                (float) (this.startY + (dx * sin + dy * cos)));
        PointF end2 = new PointF(
                (float) (this.startX + (dx * cos + dy * sin)),
                (float) (this.startY + (dx * -sin + dy * cos)));

        float h = (float) Math.sqrt(Math.pow((double) (x - this.startX), 2) + Math.pow((double) (y - this.startY), 2));
        float distance = 50;

        if (h < 140) {
            distance = (float) (h * Math.sqrt(2) / 4);
        }


        PointF normalizedE1 = new PointF((p.x * (h - distance) + (end1.x) * distance) / h, (p.y * (h - distance) + (end1.y) * distance) / h);
        PointF normalizedE2 = new PointF((p.x * (h - distance) + end2.x * distance) / h, (p.y * (h - distance) + end2.y * distance) / h);


        path.moveTo(p.x, p.y);
        path.lineTo(this.startX, this.startY);
        path.moveTo(p.x, p.y);
        path.lineTo(normalizedE1.x, normalizedE1.y);
        path.moveTo(p.x, p.y);
        path.lineTo(normalizedE2.x, normalizedE2.y);

    }


    /**
     * This method draws canvas again for Undo.
     *
     * @return If Undo is enabled, this is returned as true. Otherwise, this is returned as false.
     */
    public boolean undo() {
        if (this.historyPointer > 1) {
            this.historyPointer--;
            this.invalidate();

//            canvasActionListener.onActionListener(historyPointer);

            Log.d("logPathRect", "undo regionList.size(): " + regionList.size());
            Log.d("logPathRect", "undo pathLists.size(): " + pathLists.size());

            if (regionList.size() > 0)
                regionList.remove(regionList.size() - 1);

            return true;
        } else {
            return false;
        }
    }


    /**
     * This method is getter for canvas background color
     *
     * @return
     */
    public int getBaseColor() {
        return this.baseColor;
    }

    /**
     * This method is setter for canvas background color
     *
     * @param color
     */
    public void setBaseColor(int color) {
        this.baseColor = color;
    }


    /**
     * This method is getter for drawn text.
     *
     * @return
     */
    public String getText() {
        return this.text;
    }

    /**
     * This method is setter for drawn text.
     *
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * This method is getter for stroke or fill.
     *
     * @return
     */
    public Paint.Style getPaintStyle() {
        return this.paintStyle;
    }

    /**
     * This method is setter for stroke or fill.
     *
     * @param style
     */
    public void setPaintStyle(Paint.Style style) {
        this.paintStyle = style;
    }

    /**
     * This method is getter for stroke color.
     *
     * @return
     */
    public int getPaintStrokeColor() {
        return this.paintStrokeColor;
    }

    /**
     * This method is setter for stroke color.
     *
     * @param color
     */
    public void setPaintStrokeColor(int color) {
        this.paintStrokeColor = color;

        if (mode == Mode.TEXT)
            this.textPaint = this.createPaint();

    }

    /**
     * This method is getter for fill color.
     * But, current Android API cannot set fill color (?).
     *
     * @return
     */
    public int getPaintFillColor() {
        return this.paintFillColor;
    }

    ;

    /**
     * This method is setter for fill color.
     * But, current Android API cannot set fill color (?).
     *
     * @param color
     */
    public void setPaintFillColor(int color) {
        this.paintFillColor = color;
    }

    /**
     * This method is getter for stroke width.
     *
     * @return
     */
    public float getPaintStrokeWidth() {
        return this.paintStrokeWidth;
    }

    /**
     * This method is setter for stroke width.
     *
     * @param width
     */
    public void setPaintStrokeWidth(float width) {
        if (width >= 0) {
            this.paintStrokeWidth = width;
        } else {
            this.paintStrokeWidth = 3F;
        }
    }

    /**
     * This method is getter for alpha.
     *
     * @return
     */
    public int getOpacity() {
        return this.opacity;
    }

    /**
     * This method is setter for alpha.
     * The 1st argument must be between 0 and 255.
     *
     * @param opacity
     */
    public void setOpacity(int opacity) {
        if ((opacity >= 0) && (opacity <= 255)) {
            this.opacity = opacity;
        } else {
            this.opacity = 255;
        }
    }

    /**
     * This method is getter for amount of blur.
     *
     * @return
     */
    public float getBlur() {
        return this.blur;
    }

    /**
     * This method is setter for amount of blur.
     * The 1st argument is greater than or equal to 0.0.
     *
     * @param blur
     */
    public void setBlur(float blur) {
        if (blur >= 0) {
            this.blur = blur;
        } else {
            this.blur = 0F;
        }
    }

    /**
     * This method is getter for line cap.
     *
     * @return
     */
    public Paint.Cap getLineCap() {
        return this.lineCap;
    }

    /**
     * This method is setter for line cap.
     *
     * @param cap
     */
    public void setLineCap(Paint.Cap cap) {
        this.lineCap = cap;
    }

    /**
     * This method is getter for font size,
     *
     * @return
     */
    public float getFontSize() {
        return this.fontSize;
    }

    /**
     * This method is setter for font size.
     * The 1st argument is greater than or equal to 0.0.
     *
     * @param size
     */
    public void setFontSize(float size) {
        if (size >= 0F) {
            this.fontSize = size;
        } else {
            this.fontSize = 32F;
        }
    }

    /**
     * This method is getter for font-family.
     *
     * @return
     */
    public Typeface getFontFamily() {
        return this.fontFamily;
    }

    /**
     * This method is setter for font-family.
     *
     * @param face
     */
    public void setFontFamily(Typeface face) {
        this.fontFamily = face;
    }


    public PhotoViewAttacherCompx mAttacher;

    private ScaleType mPendingScaleType;


    @Override
    public void setRotationTo(float rotationDegree) {
        mAttacher.setRotationTo(rotationDegree);
    }

    @Override
    public void setRotationBy(float rotationDegree) {
        mAttacher.setRotationBy(rotationDegree);
    }

    @Override
    public boolean canZoom() {
        return mAttacher.canZoom();
    }

    @Override
    public RectF getDisplayRect() {
        return mAttacher.getDisplayRect();
    }

    @Override
    public void getDisplayMatrix(Matrix matrix) {
        mAttacher.getDisplayMatrix(matrix);
    }

    @Override
    public boolean setDisplayMatrix(Matrix finalRectangle) {
        return mAttacher.setDisplayMatrix(finalRectangle);
    }

    @Override
    public float getMinimumScale() {
        return mAttacher.getMinimumScale();
    }

    @Override
    public float getMediumScale() {
        return mAttacher.getMediumScale();
    }

    @Override
    public float getMaximumScale() {
        return mAttacher.getMaximumScale();
    }

    @Override
    public float getScale() {
        return mAttacher.getScale();
    }

    @Override
    public ScaleType getScaleType() {
        return mAttacher.getScaleType();
    }

    @Override
    public Matrix getImageMatrix() {
        return mAttacher.getImageMatrix();
    }

    @Override
    public void setAllowParentInterceptOnEdge(boolean allow) {
        mAttacher.setAllowParentInterceptOnEdge(allow);
    }

    @Override
    public void setMinimumScale(float minimumScale) {
        mAttacher.setMinimumScale(minimumScale);
    }

    @Override
    public void setMediumScale(float mediumScale) {
        mAttacher.setMediumScale(mediumScale);
    }

    @Override
    public void setMaximumScale(float maximumScale) {
        mAttacher.setMaximumScale(maximumScale);
    }

    @Override
    public void setScaleLevels(float minimumScale, float mediumScale, float maximumScale) {
        mAttacher.setScaleLevels(minimumScale, mediumScale, maximumScale);
    }


    @Override
// setImageBitmap calls through to this method
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (null != mAttacher) {
            mAttacher.update();
        }
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (null != mAttacher) {
            mAttacher.update();
        }
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (null != mAttacher) {
            mAttacher.update();
        }
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        boolean changed = super.setFrame(l, t, r, b);
        if (null != mAttacher) {
            mAttacher.update();
        }
        return changed;
    }

    @Override
    public void setOnMatrixChangeListener(PhotoViewAttacherCompx.OnMatrixChangedListener listener) {
        mAttacher.setOnMatrixChangeListener(listener);
    }

    @Override
    public void setOnLongClickListener(OnLongClickListener l) {
        mAttacher.setOnLongClickListener(l);
    }

    @Override
    public void setOnPhotoTapListener(PhotoViewAttacherCompx.OnPhotoTapListener listener) {
        mAttacher.setOnPhotoTapListener(listener);
    }

    @Override
    public void setOnViewTapListener(PhotoViewAttacherCompx.OnViewTapListener listener) {
        mAttacher.setOnViewTapListener(listener);
    }

    @Override
    public void setScale(float scale) {
        mAttacher.setScale(scale);
    }

    @Override
    public void setScale(float scale, boolean animate) {
        mAttacher.setScale(scale, animate);
    }

    @Override
    public void setScale(float scale, float focalX, float focalY, boolean animate) {
        mAttacher.setScale(scale, focalX, focalY, animate);
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (null != mAttacher) {
            mAttacher.setScaleType(scaleType);
        } else {
            mPendingScaleType = scaleType;
        }
    }

    @Override
    public void setZoomable(boolean zoomable) {
        mAttacher.setZoomable(zoomable);
    }

    @Override
    public Bitmap getVisibleRectangleBitmap() {
        return mAttacher.getVisibleRectangleBitmap();
    }

    @Override
    public void setZoomTransitionDuration(int milliseconds) {
        mAttacher.setZoomTransitionDuration(milliseconds);
    }

    @Override
    public IPhotoView getIPhotoViewImplementation() {
        return mAttacher;
    }

    @Override
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener newOnDoubleTapListener) {
        mAttacher.setOnDoubleTapListener(newOnDoubleTapListener);
    }

    @Override
    public void setOnScaleChangeListener(PhotoViewAttacherCompx.OnScaleChangeListener onScaleChangeListener) {
        mAttacher.setOnScaleChangeListener(onScaleChangeListener);
    }

    @Override
    public void setOnSingleFlingListener(PhotoViewAttacherCompx.OnSingleFlingListener onSingleFlingListener) {
        mAttacher.setOnSingleFlingListener(onSingleFlingListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        mAttacher.cleanup();
        mAttacher = null;
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        init();
        super.onAttachedToWindow();
    }

    public void resetZoom() {
        mAttacher.resetMatrix();
    }


    //endregion unused


}