package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public abstract class EditView extends LinearLayout {

    public EditView(Context context) {
        super(context);
        init();
    }

    public EditView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    abstract void init();

    abstract void resetBorders();

}
