package com.solidict.cropysdk.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.FocusListener;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Focus extends EditView {

    SeekBar sbFocus;
    ImageView ivResetFocus;


    public Focus(Context context) {
        super(context);
    }

    public Focus(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Focus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    void init() {
        inflate(getContext(), R.layout.view_focus, this);
        final FocusListener focusListener = (FocusListener) getContext();

        sbFocus = (SeekBar) findViewById(R.id.sbFocus);
        ivResetFocus = (ImageView) findViewById(R.id.ivResetFocus);

        sbFocus.getThumb().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));
        sbFocus.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));

        ivResetFocus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                focusListener.onFocusRadiusChanged(5000);
                sbFocus.setProgress(202);

            }
        });

        sbFocus.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                focusListener.onFocusRadiusChanged(seekBar.getProgress() == 0 ? 0 : seekBar.getProgress() / 2 + 128);
                focusListener.onFocusRadiusChanged((int) ((Math.cbrt(seekBar.getProgress()) / Math.cbrt(255)) * 255));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

//                focusListener.onFocusRadiusChanged(seekBar.getProgress());
//                focusListener.onFocusChanged(-1);


            }
        });


    }

    public void resetValues() {
        sbFocus.setProgress(128);

    }

    @Override
    void resetBorders() {
//        tvOriginal.setTextColor(getResources().getColor(R.color.white));
//        tvBlackWhite.setTextColor(getResources().getColor(R.color.white));

    }


}
