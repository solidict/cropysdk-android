package com.solidict.cropysdk.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ScrollView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.ShareActivity;
import com.solidict.cropysdk.interfaces.FocusListener;


/**
 * Created by serdarbuyukkanli on 02/11/16.
 */
public class CircleBlurView extends ScrollView {
    private float radius = 150;
    private int bitmapAlpha = 202;

    private Paint mBackgroundPaint;
    private float mCx = -1;
    private float mCy = -1;
    Bitmap bitmap;
    int w, h, x, y;
    private int bitmapPadding = 0;

    int init = 0;

    public float getmCy() {
        return mCy;
    }

    public void setmCy(float mCy) {
        this.mCy = mCy;
    }

    public float getmCx() {
        return mCx;
    }

    public void setmCx(float mCx) {
        this.mCx = mCx;
    }

    public int getBitmapAlpha() {
        return bitmapAlpha;
    }

    public void setBitmapAlpha(int bitmapAlpha) {
        this.bitmapAlpha = bitmapAlpha;
    }

    public int getBitmapPadding() {
        return bitmapPadding;
    }

    public void setBitmapPadding(int bitmapPadding) {
        this.bitmapPadding = bitmapPadding;

    }

    boolean drawModeEnabled = true;

    public boolean isDrawModeEnabled() {
        return drawModeEnabled;
    }

    public void setDrawModeEnabled(boolean drawModeEnabled) {
        this.drawModeEnabled = drawModeEnabled;
    }

    public float getRadius() {
        return radius * mScaleFactor;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    private int mTutorialColor = Color.parseColor("#D20E0F02");

    public CircleBlurView(Context context) {
        super(context);
        init();
    }

    public CircleBlurView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleBlurView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleBlurView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setWillNotDraw(false);
        setLayerType(LAYER_TYPE_HARDWARE, null);

        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//        mBackgroundPaint.setMaskFilter(new BlurMaskFilter(300, BlurMaskFilter.Blur.NORMAL));


        invalidate();

    }

    boolean zoom = false;

    long lastTouchTime = 0;


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!drawModeEnabled)
            return true;

        init++;

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:

                Log.d("logActionFocus", "ACTION_DOWN");

//                mCx = event.getX();
//                mCy = event.getY() + bitmapPadding;
//                invalidate();


                lastTouchTime = System.currentTimeMillis();

                return true;
            case MotionEvent.ACTION_POINTER_DOWN:
                zoom = true;
                Log.d("logActionFocus", "ACTION_POINTER_DOWN");

                mScaleDetector.onTouchEvent(event);
                return true;
            case MotionEvent.ACTION_MOVE:

                Log.d("logActionFocus", "ACTION_MOVE: " + Math.abs(event.getX() - mCx));
                if (zoom) {
                    mScaleDetector.onTouchEvent(event);
                    return true;

                } else {

//                    if (Math.abs(event.getX() - mCx) > 100 || Math.abs(event.getY() - mCy) > 100 && canMove) {
//                        mCx = event.getX();
//                        mCy = event.getY() + bitmapPadding;
//                        invalidate();
//                        return true;
//                    } else {
//                        canMove = true;
//                    }

                    if (System.currentTimeMillis() - lastTouchTime > 100) {
                        mCx = event.getX();
                        mCy = event.getY() + bitmapPadding;
                        invalidate();
                    } else {

                    }


                    return true;

                }


            case MotionEvent.ACTION_UP:
                Log.d("logActionFocus", "ACTION_UP");
                zoom = false;

            case MotionEvent.ACTION_POINTER_UP:

                Log.d("logActionFocus", "ACTION_POINTER_UP");

                if (!zoom) {
                    FocusListener listener = (ShareActivity) getContext();
                    listener.onFocusChanged(radius);
                }


            default:
                return false;
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {


//        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sample_oldie);

        if (bitmap != null) {

//            canvas.drawBitmap(bitmap, x, y, null);

//            canvas.scale(mScaleFactor, mScaleFactor);

            Log.d("bitmapPadding", "bitmapPadding: " + bitmapPadding);

            if (bitmapPadding < 0)
                bitmapPadding = 0;

            Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            Rect dest = new Rect(x, y - bitmapPadding, w + x, h + y - bitmapPadding);
            Paint paint = new Paint();
            paint.setAlpha(bitmapAlpha);
            canvas.drawBitmap(bitmap, src, dest, paint);

            if (mCx == -1 && mCy == -1) {

                Log.d("bitmapPaddingInit", "if mCy: " + mCy + ", this.getHeight(): " + this.getHeight() + ", y: " + y + ", radius: " + radius);

                mCx = (w + 2 * x) / 2;
//                mCy = (h + 2 * y) / 2;
                mCy = (this.getHeight()) / 2 + bitmapPadding;

            } else {
                Log.d("bitmapPaddingInit", "else mCy: " + mCy + ", mCx: " + mCx + ", this.getHeight(): " + this.getHeight() + ", y: " + y + ", radius: " + radius);

            }


            if (mCx >= 0 && mCy >= 0) {
                canvas.drawCircle(mCx, mCy - bitmapPadding, mScaleFactor * radius, mBackgroundPaint);

                Log.d("bitmapPaddingInit", "drawModeEnabled: " + drawModeEnabled);


                if (drawModeEnabled) {
                    Paint mPaint = new Paint();
                    mPaint.setStrokeWidth(6f);
                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setColor(getResources().getColor(R.color.yellow));

                    canvas.drawCircle(mCx, mCy - bitmapPadding, mScaleFactor * radius, mPaint);

                    Log.d("logCircle", "bitmapPadding: " + bitmapPadding);


                    Log.d("logFocusChanged", "x: " + mCx + ", y: " + (mCy - bitmapPadding) + ", radius: " + mScaleFactor * radius + ", alpha: " + bitmapAlpha);

                    FocusListener focusListener = (FocusListener) getContext();
                    focusListener.onFocusLocationChanged(mCx, mCy - bitmapPadding, mScaleFactor * radius, bitmapAlpha);

//                    if (init) {
//                        FocusListener focusListener = (FocusListener) get"Context();
//                        focusListener.onFocusLocationChanged(mCx, mCy + bitmapPadding, mScaleFactor * radius, bitmapAlpha);
//                    }

                }

                if (noChanges) {
                    FocusListener focusListener = (FocusListener) getContext();
                    focusListener.onFocusLocationChanged(mCx, mCy - bitmapPadding, mScaleFactor * radius, bitmapAlpha);
                    noChanges = false;
                }


            }
        }

    }

    private boolean noChanges = false;

    public boolean isNoChanges() {
        return noChanges;
    }

    public void setNoChanges(boolean noChanges) {
        this.noChanges = noChanges;
    }

    public void setBitmap(Bitmap bitmap, int w, int h, int x, int y) {
        this.bitmap = bitmap;
        this.w = w;
        this.h = h;
        this.x = x;
        this.y = y;

        if (this.y < 0)
            this.y = 0;
        if (this.x < 0)
            this.x = 0;

    }

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;

    public void setmScaleFactor(float mScaleFactor) {
        this.mScaleFactor = mScaleFactor;
    }

    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

            invalidate();
            return true;
        }
    }
}