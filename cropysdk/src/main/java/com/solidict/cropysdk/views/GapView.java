package com.solidict.cropysdk.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

import com.solidict.cropysdk.R;


/**
 * Created by serdarbuyukkanli on 02/11/16.
 */
public class GapView extends ScrollView {

    private Paint mBackgroundPaint;
    int totalWidth, totalHeight, gapX, gapY, gapWidth, gapHeight, color;

    public void setColor(int color) {
        this.color = color;
    }

    public GapView(Context context) {
        super(context);
        init();
    }

    public GapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GapView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setWillNotDraw(false);
        setLayerType(LAYER_TYPE_HARDWARE, null);

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//        mBackgroundPaint.setMaskFilter(new BlurMaskFilter(300, BlurMaskFilter.Blur.NORMAL));


        invalidate();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {


        return false;
    }


    @Override
    protected void onDraw(Canvas canvas) {

        Log.d("logGap", "onDraw");

        if (this.gapWidth != 0) {
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(color);
            canvas.drawRect(0, 0, totalWidth, totalHeight, paint);
            canvas.drawRect(gapX, gapY, gapX + gapWidth, gapY + gapHeight, mBackgroundPaint);
        }

    }

    public void setLocation(int totalWidth, int totalHeight, int gapX, int gapY, int gapWidth, int gapHeight) {

        this.totalWidth = totalWidth;
        this.totalHeight = totalHeight;

        this.gapX = gapX;
        this.gapY = gapY;
        this.gapWidth = gapWidth;
        this.gapHeight = gapHeight;

    }
}