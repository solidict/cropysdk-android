package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.CapsTextStyleListener;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class CapsTextStyles extends EditView {

    ImageView ivThin;
    ImageView ivMedium;
    ImageView ivThick;

    TextView tvThin;
    TextView tvMedium;
    TextView tvThick;

    LinearLayout llThin;
    LinearLayout llMedium;
    LinearLayout llThick;

    ImageView ivClear;
    RelativeLayout root;

    public CapsTextStyles(Context context) {
        super(context);
    }

    public CapsTextStyles(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CapsTextStyles(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    void init() {
        inflate(getContext(), R.layout.view_caps_text_styles, this);
        final CapsTextStyleListener textStyleListener = (CapsTextStyleListener) getContext();

        ivThin = (ImageView) findViewById(R.id.ivThin);
        ivMedium = (ImageView) findViewById(R.id.ivMedium);
        ivThick = (ImageView) findViewById(R.id.ivThick);

        tvThin = (TextView) findViewById(R.id.tvThin);
        tvMedium = (TextView) findViewById(R.id.tvMedium);
        tvThick = (TextView) findViewById(R.id.tvThick);

        llThin = (LinearLayout) findViewById(R.id.llThin);
        llMedium = (LinearLayout) findViewById(R.id.llMedium);
        llThick = (LinearLayout) findViewById(R.id.llThick);

        ivClear = (ImageView) findViewById(R.id.ivClear);
        root = (RelativeLayout) findViewById(R.id.root);

        llThin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivThin.setImageResource(R.drawable.text_secili);
                tvThin.setTextColor(getResources().getColor(R.color.yellow));
                textStyleListener.onCapsTextStyleChanged(20);
            }
        });

        llMedium.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivMedium.setImageResource(R.drawable.text_secili);
                tvMedium.setTextColor(getResources().getColor(R.color.yellow));
                textStyleListener.onCapsTextStyleChanged(24);
            }
        });

        llThick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivThick.setImageResource(R.drawable.text_secili);
                tvThick.setTextColor(getResources().getColor(R.color.yellow));
                textStyleListener.onCapsTextStyleChanged(28);
            }
        });

        ivClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                textStyleListener.onCapsTextStyleChanged(-1);
            }
        });

        root.setOnClickListener(null);


    }

    @Override
    void resetBorders() {
        ivThin.setImageResource(R.drawable.text);
        ivMedium.setImageResource(R.drawable.text);
        ivThick.setImageResource(R.drawable.text);

        tvThin.setTextColor(getResources().getColor(R.color.white));
        tvMedium.setTextColor(getResources().getColor(R.color.white));
        tvThick.setTextColor(getResources().getColor(R.color.white));


    }


}
