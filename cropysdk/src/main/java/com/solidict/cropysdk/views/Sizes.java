package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.SizeListener;
import com.solidict.cropysdk.utils.Utils;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Sizes extends EditView {

    ImageView ivThinLine;
    ImageView ivMediumLine;
    ImageView ivThickLine;
    Context context;

    public Sizes(Context context) {
        super(context);
        this.context = context;
    }

    public Sizes(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

    }

    public Sizes(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    void init() {
        inflate(getContext(), R.layout.view_sizes, this);
        final SizeListener sizeListener = (SizeListener) getContext();

        ivThinLine = (ImageView) findViewById(R.id.ivThinLine);
        ivMediumLine = (ImageView) findViewById(R.id.ivMediumLine);
        ivThickLine = (ImageView) findViewById(R.id.ivThickLine);

        ivThinLine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivThinLine.setImageResource(R.drawable.cizgi_1_selected);
                sizeListener.onSizeChanged(Utils.dpToPx(context, 3f));
            }
        });

        ivMediumLine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivMediumLine.setImageResource(R.drawable.cizgi_2_selected);
                sizeListener.onSizeChanged(Utils.dpToPx(context, 6f));
            }
        });

        ivThickLine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivThickLine.setImageResource(R.drawable.cizgi_3_selected);
                sizeListener.onSizeChanged(Utils.dpToPx(context, 9f));
            }
        });
    }

    @Override
    void resetBorders() {
        ivThinLine.setImageResource(R.drawable.cizgi_1);
        ivMediumLine.setImageResource(R.drawable.cizgi_2);
        ivThickLine.setImageResource(R.drawable.cizgi_3);

    }

}
