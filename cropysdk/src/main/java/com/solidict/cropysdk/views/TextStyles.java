package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.TextStyleListener;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class TextStyles extends EditView {

    ImageView ivBold;
    ImageView ivItalic;
    ImageView ivUnderlined;
    ImageView ivTextDone;
    boolean bold = false;
    boolean italic = false;
    boolean underlined = false;


    public TextStyles(Context context) {
        super(context);
    }

    public TextStyles(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextStyles(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    void init() {
        inflate(getContext(), R.layout.view_text_styles, this);
        final TextStyleListener textStyleListener = (TextStyleListener) getContext();

        ivBold = (ImageView) findViewById(R.id.ivBold);
        ivItalic = (ImageView) findViewById(R.id.ivItalic);
        ivUnderlined = (ImageView) findViewById(R.id.ivUnderlined);
        ivTextDone = (ImageView) findViewById(R.id.ivTextDone);


        ivBold.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                resetBorders();
                if (bold)
                    ivBold.setImageResource(R.drawable.bold);
                else
                    ivBold.setImageResource(R.drawable.bold_secili);
                bold = !bold;

                textStyleListener.onTextStyleChanged(0);
            }
        });

        ivItalic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                resetBorders();
                if (italic)
                    ivItalic.setImageResource(R.drawable.italic);
                else
                    ivItalic.setImageResource(R.drawable.italic_secili);
                italic = !italic;

                textStyleListener.onTextStyleChanged(1);
            }
        });

        ivUnderlined.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                resetBorders();
                if (underlined)
                    ivUnderlined.setImageResource(R.drawable.alt_cizgili);
                else
                    ivUnderlined.setImageResource(R.drawable.alt_cizgili_secili);
                underlined = !underlined;

                textStyleListener.onTextStyleChanged(2);
            }
        });

        ivTextDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                textStyleListener.onTypingFinished();
            }
        });


    }

    @Override
    void resetBorders() {
        ivBold.setImageResource(R.drawable.bold);
        ivItalic.setImageResource(R.drawable.italic);
        ivUnderlined.setImageResource(R.drawable.alt_cizgili);

    }


}
