package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.FrameListener;
import com.solidict.cropysdk.log.LogManager;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Frames extends EditView {


    ImageView ivOriginal;
    TextView tvOriginal;
    ImageView ivDuzSiyah;
    TextView tvDuzSiyah;
    ImageView ivDuzBeyaz;
    TextView tvDuzBeyaz;
    ImageView ivOvalSiyah;
    TextView tvOvalSiyah;
    ImageView ivOvalBeyaz;
    TextView tvOvalBeyaz;
    ImageView ivPolaroid;
    TextView tvPolaroid;

    ImageView ivNegative;
    TextView tvNegative;
    ImageView ivGrunge;
    TextView tvGrunge;
    ImageView ivVintage;
    TextView tvVintage;
    ImageView ivPapercut;
    TextView tvPapercut;
    ImageView ivStamp;
    TextView tvStamp;
    ImageView ivVintageNewspaper;
    TextView tvVintageNewspaper;
    ImageView ivOldPaper;
    TextView tvOldPaper;
    ImageView ivGallery;
    TextView tvGallery;
    ImageView ivClip;
    TextView tvClip;
    ImageView ivRecord;
    TextView tvRecord;

    public Frames(Context context) {
        super(context);
    }

    public Frames(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Frames(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

//    @Override
//    void init() {
//
//
//    }

    //TODO extended
    @Override
    void init() {
        inflate(getContext(), R.layout.view_frames, this);
        final FrameListener frameListener = (FrameListener) getContext();

        ivOriginal = (ImageView) findViewById(R.id.ivOriginal);
        tvOriginal = (TextView) findViewById(R.id.tvOriginal);
        ivDuzSiyah = (ImageView) findViewById(R.id.ivDuzSiyah);
        tvDuzSiyah = (TextView) findViewById(R.id.tvDuzSiyah);
        ivDuzBeyaz = (ImageView) findViewById(R.id.ivDuzBeyaz);
        tvDuzBeyaz = (TextView) findViewById(R.id.tvDuzBeyaz);
        ivOvalSiyah = (ImageView) findViewById(R.id.ivOvalSiyah);
        tvOvalSiyah = (TextView) findViewById(R.id.tvOvalSiyah);
        ivOvalBeyaz = (ImageView) findViewById(R.id.ivOvalBeyaz);
        tvOvalBeyaz = (TextView) findViewById(R.id.tvOvalBeyaz);
        ivPolaroid = (ImageView) findViewById(R.id.ivPolaroid);
        tvPolaroid = (TextView) findViewById(R.id.tvPolaroid);
        ivNegative = (ImageView) findViewById(R.id.ivNegative);
        tvNegative = (TextView) findViewById(R.id.tvNegative);
        ivGrunge = (ImageView) findViewById(R.id.ivGrunge);
        tvGrunge = (TextView) findViewById(R.id.tvGrunge);
        ivVintage = (ImageView) findViewById(R.id.ivVintage);
        tvVintage = (TextView) findViewById(R.id.tvVintage);
        ivPapercut = (ImageView) findViewById(R.id.ivPapercut);
        tvPapercut = (TextView) findViewById(R.id.tvPapercut);
        ivStamp = (ImageView) findViewById(R.id.ivStamp);
        tvStamp = (TextView) findViewById(R.id.tvStamp);
        ivVintageNewspaper = (ImageView) findViewById(R.id.ivVintageNewspaper);
        tvVintageNewspaper = (TextView) findViewById(R.id.tvVintageNewspaper);
        ivOldPaper = (ImageView) findViewById(R.id.ivOldPaper);
        tvOldPaper = (TextView) findViewById(R.id.tvOldPaper);
        ivGallery = (ImageView) findViewById(R.id.ivGallery);
        tvGallery = (TextView) findViewById(R.id.tvGallery);
        ivClip = (ImageView) findViewById(R.id.ivClip);
        tvClip = (TextView) findViewById(R.id.tvClip);
        ivRecord = (ImageView) findViewById(R.id.ivRecord);
        tvRecord = (TextView) findViewById(R.id.tvRecord);

        ivOriginal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvOriginal.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(-1);
                LogManager.addLog("Editing Screen - Frame applied - Original");
                sendEvent("Original");
            }
        });
        ivDuzSiyah.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvDuzSiyah.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(R.drawable.duz_siyah);
                LogManager.addLog("Editing Screen - Frame applied - Black");
                sendEvent("Black");

            }
        });
        ivDuzBeyaz.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvDuzBeyaz.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(R.drawable.duz_beyaz);
                LogManager.addLog("Editing Screen - Frame applied - White");
                sendEvent("White");

            }
        });
        ivOvalSiyah.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvOvalSiyah.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(R.drawable.frame_oval_siyah);
                LogManager.addLog("Editing Screen - Frame applied - Black rounded");
                sendEvent("Black rounded");

            }
        });
        ivOvalBeyaz.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvOvalBeyaz.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(R.drawable.frame_oval_beyaz);
                LogManager.addLog("Editing Screen - Frame applied - White rounded");
                sendEvent("White rounded");

            }
        });
        ivPolaroid.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvPolaroid.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(R.drawable.frame_polaroid);
                LogManager.addLog("Editing Screen - Frame applied - Polaroid");
                sendEvent("Polaroid");


            }
        });

        ivNegative.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvNegative.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(0);
                LogManager.addLog("Editing Screen - Frame applied - Negative");
                sendEvent("Negative");

            }
        });
        ivGrunge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvGrunge.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(1);
                LogManager.addLog("Editing Screen - Frame applied - Grunge");
                sendEvent("Grunge");

            }
        });
        ivVintage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvVintage.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(2);
                LogManager.addLog("Editing Screen - Frame applied - Vintage");
                sendEvent("Vintage");

            }
        });
        ivPapercut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvPapercut.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(3);
                LogManager.addLog("Editing Screen - Frame applied - Paper Cut");
                sendEvent("Paper Cut");

            }
        });
        ivStamp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvStamp.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(4);
                LogManager.addLog("Editing Screen - Frame applied - Stamp");
                sendEvent("Stamp");

            }
        });
        ivVintageNewspaper.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvVintageNewspaper.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(5);
                LogManager.addLog("Editing Screen - Frame applied - Vintage Newspaper");
                sendEvent("Vintage Newspaper");


            }
        });
        ivOldPaper.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvOldPaper.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(6);
                LogManager.addLog("Editing Screen - Frame applied - Old Paper");
                sendEvent("Old Paper");

            }
        });
        ivGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvGallery.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(7);
                LogManager.addLog("Editing Screen - Frame applied - Gallery");
                sendEvent("Gallery");
            }
        });
        ivClip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvClip.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(8);
                LogManager.addLog("Editing Screen - Frame applied - Clip");
                sendEvent("Clip");

            }
        });
        ivRecord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                tvRecord.setTextColor(getResources().getColor(R.color.yellow));
                frameListener.onFrameChanged(9);
                LogManager.addLog("Editing Screen - Frame applied - Record");
                sendEvent("Record");
            }
        });


    }

    public void resetValues() {
        resetBorders();
        tvOriginal.setTextColor(getResources().getColor(R.color.yellow));
    }

    @Override
    void resetBorders() {
        tvOriginal.setTextColor(getResources().getColor(R.color.white));
        tvDuzSiyah.setTextColor(getResources().getColor(R.color.white));
        tvDuzBeyaz.setTextColor(getResources().getColor(R.color.white));
        tvOvalSiyah.setTextColor(getResources().getColor(R.color.white));
        tvOvalBeyaz.setTextColor(getResources().getColor(R.color.white));
        tvPolaroid.setTextColor(getResources().getColor(R.color.white));
        tvNegative.setTextColor(getResources().getColor(R.color.white));
        tvGrunge.setTextColor(getResources().getColor(R.color.white));
        tvVintage.setTextColor(getResources().getColor(R.color.white));
        tvPapercut.setTextColor(getResources().getColor(R.color.white));
        tvStamp.setTextColor(getResources().getColor(R.color.white));
        tvVintageNewspaper.setTextColor(getResources().getColor(R.color.white));
        tvOldPaper.setTextColor(getResources().getColor(R.color.white));
        tvGallery.setTextColor(getResources().getColor(R.color.white));
        tvClip.setTextColor(getResources().getColor(R.color.white));
        tvRecord.setTextColor(getResources().getColor(R.color.white));

    }

    private void sendEvent(String name) {
//        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        Bundle params = new Bundle();
//        params.putBoolean("success", true);
//        params.putString("name", name);
//        firebaseAnalytics.logEvent("FRAME", params);
    }


}
