package com.solidict.cropysdk.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.ColorListener;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Palette extends EditView {

    ImageView ivBlue;
    ImageView ivGreen;
    ImageView ivYellow;
    ImageView ivOrange;
    ImageView ivRed;
    ImageView ivPink;
    ImageView ivBlack;

    public Palette(Context context) {
        super(context);
    }

    public Palette(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Palette(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    void init() {
        inflate(getContext(), R.layout.view_palette, this);
        final ColorListener colorListener = (ColorListener) getContext();

        ivBlue = (ImageView) findViewById(R.id.ivBlue);
        ivGreen = (ImageView) findViewById(R.id.ivGreen);
        ivYellow = (ImageView) findViewById(R.id.ivYellow);
        ivOrange = (ImageView) findViewById(R.id.ivOrange);
        ivRed = (ImageView) findViewById(R.id.ivRed);
        ivPink = (ImageView) findViewById(R.id.ivPink);
        ivBlack = (ImageView) findViewById(R.id.ivBlack);

        ivBlue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivBlue.setImageResource(R.drawable.mavi_secili);
                colorListener.onColorchanged(Color.parseColor("#FF188AB1"));
            }
        });
        ivGreen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivGreen.setImageResource(R.drawable.yesil_secili);
                colorListener.onColorchanged(Color.parseColor("#FF389738"));
            }
        });
        ivYellow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivYellow.setImageResource(R.drawable.sari_secili);
                colorListener.onColorchanged(Color.parseColor("#FFD7D718"));
            }
        });
        ivOrange.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivOrange.setImageResource(R.drawable.turuncu_secili);
                colorListener.onColorchanged(Color.parseColor("#FFD33F20"));
            }
        });
        ivRed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivRed.setImageResource(R.drawable.kirmizi_secili);
                colorListener.onColorchanged(Color.parseColor("#FFD4001E"));
            }
        });
        ivPink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivPink.setImageResource(R.drawable.pembe_secili);
                colorListener.onColorchanged(Color.parseColor("#FFAD0068"));
            }
        });
        ivBlack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBorders();
                ivBlack.setImageResource(R.drawable.lacivert_secili);
                colorListener.onColorchanged(Color.parseColor("#FF07091D"));
            }
        });

    }

    @Override
    void resetBorders() {
        ivBlue.setImageResource(R.drawable.mavi);
        ivGreen.setImageResource(R.drawable.yesil);
        ivYellow.setImageResource(R.drawable.sari);
        ivOrange.setImageResource(R.drawable.turuncu);
        ivRed.setImageResource(R.drawable.kirmizi);
        ivPink.setImageResource(R.drawable.pembe);
        ivBlack.setImageResource(R.drawable.lacivert);
    }


}
