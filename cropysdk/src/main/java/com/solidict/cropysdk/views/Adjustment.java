package com.solidict.cropysdk.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.AdjustmentListener;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class Adjustment extends EditView {

    SeekBar sbBrightness;
    SeekBar sbContrast;

    TextView tvContrast;
    TextView tvBrightness;


    public Adjustment(Context context) {
        super(context);
    }

    public Adjustment(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Adjustment(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    void init() {
        inflate(getContext(), R.layout.view_adjustment, this);
        final AdjustmentListener adjustmentListener = (AdjustmentListener) getContext();

        sbBrightness = (SeekBar) findViewById(R.id.sbBrightness);
        sbContrast = (SeekBar) findViewById(R.id.sbContrast);
        tvContrast = (TextView) findViewById(R.id.tvContrast);
        tvBrightness = (TextView) findViewById(R.id.tvBrightness);


        sbBrightness.getThumb().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));
        sbBrightness.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));
        sbContrast.getThumb().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));
        sbContrast.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(com.solidict.cropysdk.R.color.yellow), PorterDuff.Mode.SRC_IN));

        sbBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                if ((seekBar.getProgress() * 1.25f - 100) > 0)
                    tvBrightness.setText("+" + (int) (seekBar.getProgress() * 1.25f - 100));
                else tvBrightness.setText("" + (int) (seekBar.getProgress() * 1.25f - 100));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("logAdjustment", "value: " + seekBar.getProgress());
//                adjustmentListener.onAdjustmentChanged(sbBrightness.getProgress() - 80, sbContrast.getProgress() / 50f);
                adjustmentListener.onAdjustmentChanged(sbBrightness.getProgress() - 80, (sbContrast.getProgress() + 25) / 75f);


            }
        });

        sbContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (seekBar.getProgress() * 2 - 100 > 0)
                    tvContrast.setText("+" + (seekBar.getProgress() * 2 - 100));
                else
                    tvContrast.setText("" + (seekBar.getProgress() * 2 - 100));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                Log.d("logCont", "cont: " + (sbContrast.getProgress() + 25) / 75f);

                adjustmentListener.onAdjustmentChanged(sbBrightness.getProgress() - 80, 0.5f + (float) (Math.pow(sbContrast.getProgress(), 2)) / 5000f);


            }
        });


    }

    public void resetValues() {
        tvBrightness.setText("0");
        tvContrast.setText("0");
        sbBrightness.setProgress(80);
        sbContrast.setProgress(50);

    }

    @Override
    void resetBorders() {
//        tvOriginal.setTextColor(getResources().getColor(R.color.white));
//        tvBlackWhite.setTextColor(getResources().getColor(R.color.white));

    }


}
