package com.solidict.cropysdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.solidict.cropysdk.R;
import com.solidict.cropysdk.interfaces.ShapeListener;
import com.solidict.cropysdk.log.LogManager;


/**
 * Created by serdarbuyukkanli on 01/09/16.
 */
public class RotateShapes extends EditView {


    ImageView ivArrow;
    ImageView ivLine;
    ImageView ivFree;
    ImageView ivSquare;
    ImageView ivCircle;
    ImageView ivFrame;
    ImageView ivText;
    ImageView ivFilter;
    ImageView ivPixelate;
    ImageView ivAdjustment;
    ImageView ivFocus;
    ImageView ivRotate;
    ImageView ivCaps;

    public RotateShapes(Context context) {
        super(context);
    }

    public RotateShapes(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RotateShapes(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    void init() {
        inflate(getContext(), R.layout.view_shapes, this);

        final ShapeListener shapeListener = (ShapeListener) getContext();

        ivArrow = (ImageView) findViewById(R.id.ivArrow);
        ivLine = (ImageView) findViewById(R.id.ivLine);
        ivFree = (ImageView) findViewById(R.id.ivFree);
        ivSquare = (ImageView) findViewById(R.id.ivSquare);
        ivCircle = (ImageView) findViewById(R.id.ivCircle);
        ivFrame = (ImageView) findViewById(R.id.ivFrame);
        ivText = (ImageView) findViewById(R.id.ivText);
        ivFilter = (ImageView) findViewById(R.id.ivFilter);
        ivPixelate = (ImageView) findViewById(R.id.ivPixelate);
        ivAdjustment = (ImageView) findViewById(R.id.ivAdjustment);
        ivFocus = (ImageView) findViewById(R.id.ivFocus);
        ivRotate = (ImageView) findViewById(R.id.ivRotate);
        ivCaps = (ImageView) findViewById(R.id.ivCaps);


        ivArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "This will be available soon..", Toast.LENGTH_SHORT).show();

                if (ivArrow.getImageAlpha() == 254) {
                    ivArrow.setImageResource(R.drawable.ok);
                    ivArrow.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Arrow");
                    resetBorders();
                    ivArrow.setImageResource(R.drawable.ok_selected);
                    ivArrow.setImageAlpha(254);
                    //TODO
                    shapeListener.onShapeChanged(5);
                }

            }
        });

        ivLine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ivLine.getImageAlpha() == 254) {
                    ivLine.setImageResource(R.drawable.duz_cizgi);
                    ivLine.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Line");
                    resetBorders();
                    ivLine.setImageResource(R.drawable.duz_cizgi_selected);
                    ivLine.setImageAlpha(254);
                    shapeListener.onShapeChanged(6);
                }


            }
        });
        ivFree.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivFree.getImageAlpha() == 254) {
                    ivFree.setImageResource(R.drawable.serbest_cizgi);
                    ivFree.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Pen");
                    resetBorders();
                    ivFree.setImageResource(R.drawable.serbest_cizgi_selected);
                    ivFree.setImageAlpha(254);
                    shapeListener.onShapeChanged(7);
                }

            }
        });

        ivSquare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivSquare.getImageAlpha() == 254) {
                    ivSquare.setImageResource(R.drawable.kare);
                    ivSquare.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Rectangle");
                    resetBorders();
                    ivSquare.setImageResource(R.drawable.kare_selected);
                    ivSquare.setImageAlpha(254);
                    shapeListener.onShapeChanged(8);
                }

            }
        });

        ivCircle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivCircle.getImageAlpha() == 254) {
                    ivCircle.setImageResource(R.drawable.daire);
                    ivCircle.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Circle");
                    resetBorders();
                    ivCircle.setImageResource(R.drawable.daire_selected);
                    ivCircle.setImageAlpha(254);
                    shapeListener.onShapeChanged(9);
                }

            }
        });

        ivFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivFrame.getImageAlpha() == 254) {
                    ivFrame.setImageResource(R.drawable.frame);
                    ivFrame.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Frame");
                    resetBorders();
                    ivFrame.setImageResource(R.drawable.frame_secili);
                    ivFrame.setImageAlpha(254);
                    shapeListener.onShapeChanged(10);
                }

            }
        });

        ivText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivText.getImageAlpha() == 254) {
                    ivText.setImageResource(R.drawable.text);
                    ivText.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Text");
                    resetBorders();
                    ivText.setImageResource(R.drawable.text_secili);
                    ivText.setImageAlpha(254);
                    shapeListener.onShapeChanged(4);
                }

            }
        });

        ivFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivFilter.getImageAlpha() == 254) {
                    ivFilter.setImageResource(R.drawable.filtre);
                    ivFilter.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Filter");
                    resetBorders();
                    ivFilter.setImageResource(R.drawable.filtre_secili);
                    ivFilter.setImageAlpha(254);
                    shapeListener.onShapeChanged(3);
                }

            }
        });

        ivPixelate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivPixelate.getImageAlpha() == 254) {
                    ivPixelate.setImageResource(R.drawable.pixelate);
                    ivPixelate.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Pixelate");
                    resetBorders();
                    ivPixelate.setImageResource(R.drawable.pixelate_secili);
                    ivPixelate.setImageAlpha(254);
                    shapeListener.onShapeChanged(11);
                }

            }
        });

        ivFocus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivFocus.getImageAlpha() == 254) {
                    ivFocus.setImageResource(R.drawable.target);
                    ivFocus.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Focus");
                    resetBorders();
                    ivFocus.setImageResource(R.drawable.target_secili);
                    ivFocus.setImageAlpha(254);
                    shapeListener.onShapeChanged(12);
                }

            }
        });

        ivAdjustment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ivAdjustment.getImageAlpha() == 254) {
                    ivAdjustment.setImageResource(R.drawable.frekans);
                    ivAdjustment.setImageAlpha(255);
                    shapeListener.onShapeChanged(-1);
                } else {
                    LogManager.addLog("Editing Screen - Feature changed - Adjustment");
                    resetBorders();
                    ivAdjustment.setImageResource(R.drawable.frekans_secili);
                    ivAdjustment.setImageAlpha(254);
                    shapeListener.onShapeChanged(2);
                }

            }
        });

        ivCaps.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                LogManager.addLog("Editing Screen - Feature changed - Caps");
                resetBorders();
                shapeListener.onShapeChanged(13);

            }
        });


    }

    @Override
    public void resetBorders() {
        ivArrow.setImageResource(R.drawable.ok);
        ivLine.setImageResource(R.drawable.duz_cizgi);
        ivFree.setImageResource(R.drawable.serbest_cizgi);
        ivSquare.setImageResource(R.drawable.kare);
        ivCircle.setImageResource(R.drawable.daire);
        ivFrame.setImageResource(R.drawable.frame);
        ivText.setImageResource(R.drawable.text);
        ivFilter.setImageResource(R.drawable.filtre);
        ivPixelate.setImageResource(R.drawable.pixelate);
        ivAdjustment.setImageResource(R.drawable.frekans);
        ivFocus.setImageResource(R.drawable.target);
        ivCaps.setImageResource(R.drawable.caps);

        ivArrow.setImageAlpha(255);
        ivLine.setImageAlpha(255);
        ivFree.setImageAlpha(255);
        ivSquare.setImageAlpha(255);
        ivCircle.setImageAlpha(255);
        ivFrame.setImageAlpha(255);
        ivText.setImageAlpha(255);
        ivFilter.setImageAlpha(255);
        ivPixelate.setImageAlpha(255);
        ivAdjustment.setImageAlpha(255);
        ivFocus.setImageAlpha(255);
        ivCaps.setImageAlpha(255);

        invalidate();

    }

    public void hideItem(int position) {
        switch (position) {
            case 0:
                ivAdjustment.setVisibility(GONE);
                break;
            case 1:
                ivFilter.setVisibility(GONE);
                break;
            case 2:
                ivText.setVisibility(GONE);
                break;
            case 3:
                ivArrow.setVisibility(GONE);
                break;
            case 4:
                ivLine.setVisibility(GONE);
                break;
            case 5:
                ivFree.setVisibility(GONE);
                break;
            case 6:
                ivSquare.setVisibility(GONE);
                break;
            case 7:
                ivCircle.setVisibility(GONE);
                break;
            case 8:
                ivFrame.setVisibility(GONE);
                break;
            case 9:
                ivPixelate.setVisibility(GONE);
                break;
            case 10:
                ivFocus.setVisibility(GONE);
                break;
            case 11:
                ivCaps.setVisibility(GONE);
                break;

        }
    }

    public void selectItem(int type) {

        resetBorders();

        switch (type) {
            case 2:
                ivAdjustment.setImageResource(R.drawable.frekans_secili);
                ivAdjustment.setImageAlpha(254);
                break;
            case 3:
                ivFilter.setImageResource(R.drawable.filtre_secili);
                ivFilter.setImageAlpha(254);
                break;
            case 4:
                ivText.setImageResource(R.drawable.text_secili);
                ivText.setImageAlpha(254);
                break;
            case 5:
                ivArrow.setImageResource(R.drawable.ok_selected);
                ivArrow.setImageAlpha(254);
                break;
            case 6:
                ivLine.setImageResource(R.drawable.duz_cizgi_selected);
                ivLine.setImageAlpha(254);
                break;
            case 7:
                ivFree.setImageResource(R.drawable.serbest_cizgi_selected);
                ivFree.setImageAlpha(254);
                break;
            case 8:
                ivSquare.setImageResource(R.drawable.kare_selected);
                ivSquare.setImageAlpha(254);
                break;
            case 9:
                ivCircle.setImageResource(R.drawable.daire_selected);
                ivCircle.setImageAlpha(254);
                break;
            case 10:
                ivFrame.setImageResource(R.drawable.frame_secili);
                ivFrame.setImageAlpha(254);
                break;
            case 11:
                ivPixelate.setImageResource(R.drawable.pixelate_secili);
                ivPixelate.setImageAlpha(254);
                break;
            case 12:
                ivFocus.setImageResource(R.drawable.target_secili);
                ivFocus.setImageAlpha(254);
                break;
            case 13:
//                ivCaps.setImageResource(R.drawable.frekans_secili);
                ivCaps.setImageAlpha(254);
                break;

        }
    }


}
