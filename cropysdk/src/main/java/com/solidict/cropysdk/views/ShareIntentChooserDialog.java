package com.solidict.cropysdk.views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.solidict.cropysdk.R;
import com.solidict.cropysdk.adapters.ShareMailAdapter;

import java.io.File;
import java.util.List;

/**
 * Created by serdarbuyukkanli on 21/07/16.
 */
public class ShareIntentChooserDialog extends Dialog {

    public ShareIntentChooserDialog(final Context context, final File url,
                                    final String mail, final String subject) {
        super(context, R.style.hidetitle);

        setContentView(R.layout.popup_poi_list);

        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final Intent i = new Intent(Intent.ACTION_SENDTO);
        // ListView lvContentName = (ListView) findViewById(R.id.lv_poi);
        String uriText = "mailto:" + Uri.encode("") + "?subject="
                + Uri.encode("ew") + "&body=" + Uri.encode("ds");
        Uri uri = Uri.parse(uriText);

        // String uriText = "mailto:" + Uri.encode("") + "?subject="
        // + Uri.encode(subject) + "&body=" + Uri.encode(mail);
        // Uri uri = Uri.parse(uriText);
        // Uri pictureUri = Uri.fromFile(url);
        // i.putExtra(Intent.EXTRA_STREAM, pictureUri);
        // i.setData(uri);

        // Uri pictureUri = Uri.fromFile(url);
        // i.putExtra(Intent.EXTRA_STREAM, pictureUri);
        i.setData(uri);
        // startActivity(Intent.createChooser(i, "Send mail..."));

        // Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
        // sendIntent.setType("text/plain");

        List activities = context.getPackageManager().queryIntentActivities(i,
                0);

        ListView lv = (ListView) findViewById(R.id.lv_poi);
        final ShareMailAdapter adapter = new ShareMailAdapter(context,
                activities.toArray());
        lv.setAdapter(adapter);
        lv.setDividerHeight(0);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
//                ((Activity) context).finish();
                ResolveInfo info = (ResolveInfo) adapter.getItem(position);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setClassName(info.activityInfo.packageName,
                        info.activityInfo.name);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_TEXT, Html
                        .fromHtml(mail));
                Uri pictureUri = Uri.fromFile(url);
                intent.putExtra(Intent.EXTRA_STREAM, pictureUri);
                (context).startActivity(intent);
                dismiss();
            }
        });

    }

}