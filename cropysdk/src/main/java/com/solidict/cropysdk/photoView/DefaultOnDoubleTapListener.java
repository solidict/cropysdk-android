package com.solidict.cropysdk.photoView;

import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Provided default implementation of GestureDetector.OnDoubleTapListener, to be overridden with custom behavior, if needed
 * <p>&nbsp;</p>
 * To be used via {@link PhotoViewAttacherCompx#setOnDoubleTapListener(GestureDetector.OnDoubleTapListener)}
 */
public class DefaultOnDoubleTapListener implements GestureDetector.OnDoubleTapListener {

    private PhotoViewAttacherCompx photoViewAttacherCompx;

    /**
     * Default constructor
     *
     * @param photoViewAttacherCompx PhotoViewAttacherCompx to bind to
     */
    public DefaultOnDoubleTapListener(PhotoViewAttacherCompx photoViewAttacherCompx) {
        setPhotoViewAttacherCompx(photoViewAttacherCompx);
    }

    /**
     * Allows to change PhotoViewAttacherCompx within range of single instance
     *
     * @param newPhotoViewAttacherCompx PhotoViewAttacherCompx to bind to
     */
    public void setPhotoViewAttacherCompx(PhotoViewAttacherCompx newPhotoViewAttacherCompx) {
        this.photoViewAttacherCompx = newPhotoViewAttacherCompx;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        if (this.photoViewAttacherCompx == null)
            return false;

        ImageView imageView = photoViewAttacherCompx.getImageView();

        if (null != photoViewAttacherCompx.getOnPhotoTapListener()) {
            final RectF displayRect = photoViewAttacherCompx.getDisplayRect();

            if (null != displayRect) {
                final float x = e.getX(), y = e.getY();

                // Check to see if the user tapped on the photo
                if (displayRect.contains(x, y)) {

                    float xResult = (x - displayRect.left)
                            / displayRect.width();
                    float yResult = (y - displayRect.top)
                            / displayRect.height();

                    photoViewAttacherCompx.getOnPhotoTapListener().onPhotoTap(imageView, xResult, yResult);
                    return true;
                }else{
                    photoViewAttacherCompx.getOnPhotoTapListener().onOutsidePhotoTap();
                }
            }
        }
        if (null != photoViewAttacherCompx.getOnViewTapListener()) {
            photoViewAttacherCompx.getOnViewTapListener().onViewTap(imageView, e.getX(), e.getY());
        }

        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent ev) {
        if (photoViewAttacherCompx == null)
            return false;

        try {
            float scale = photoViewAttacherCompx.getScale();
            float x = ev.getX();
            float y = ev.getY();

            if (scale < photoViewAttacherCompx.getMediumScale()) {
                photoViewAttacherCompx.setScale(photoViewAttacherCompx.getMediumScale(), x, y, true);
            } else if (scale >= photoViewAttacherCompx.getMediumScale() && scale < photoViewAttacherCompx.getMaximumScale()) {
                photoViewAttacherCompx.setScale(photoViewAttacherCompx.getMaximumScale(), x, y, true);
            } else {
                photoViewAttacherCompx.setScale(photoViewAttacherCompx.getMinimumScale(), x, y, true);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // Can sometimes happen when getX() and getY() is called
        }

        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        // Wait for the confirmed onDoubleTap() instead
        return false;
    }

}
