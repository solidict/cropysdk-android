package com.solidict.cropysdk;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.solidict.cropysdk.interfaces.RestApi;
import com.solidict.cropysdk.models.ActionResult;
import com.solidict.cropysdk.models.Eula;
import com.solidict.cropysdk.models.NewUser;
import com.solidict.cropysdk.utils.Utils;
import com.solidict.cropysdk.views.AdjustableImageView;
import com.squareup.okhttp.OkHttpClient;

import java.io.ByteArrayOutputStream;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class DepoActivity extends Activity {

    ImageView ivBack;
    AdjustableImageView ivImage;
    TextView tvSave;

    String path;
    String xAuthToken;
    CropyApp cropyApp;
    RestApi restApi;
    boolean read = false;
    int id = -1;
    int errorCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depo);


        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivImage = (AdjustableImageView) findViewById(R.id.ivImage);
        tvSave = (TextView) findViewById(R.id.tvSave);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (xAuthToken != null) {
                    getBaseUrl();
                } else {
                    showErrorDialog(getResources().getString(R.string.login_fail), false);
//            showSignupAkilliDepoDialog(null, false);
                }            }
        });




        cropyApp = (CropyApp) getApplicationContext();


//        path = getIntent().getExtras().getString("path", null);

        String path = Utils.createDirectoryAndSaveFile(CropyApp.croppedBitmapPath, DepoActivity.this, CropyApp.croppedBitmap, true).getPath();
        ivImage.setImageBitmap(CropyApp.croppedBitmap);

        SharedPreferences preferences = this.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        String xRememberMe = preferences.getString("xRememberMe", null);

//TODO bunu kaldır
//        startXNewUserScenario();

        if (xRememberMe != null) {
            rememberMe();
        } else {
            startLoginScenario();
        }

    }

    private void rememberMe() {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);

        restApi = cropyApp.setupRestClient(true, true);

        restApi.rememberMe(Utils.getDevice(), new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equals("X-Auth-Token")) {
                        xAuthToken = header.getValue();

                    }

                }

                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                showErrorDialog(getResources().getString(R.string.error_occured), false);
                dialog.dismiss();
            }
        });

    }

    private void getBaseUrl() {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);
        restApi = setupRestClient(false, null);

        restApi.baseUrl(new Callback<ActionResult>() {

            @Override
            public void success(ActionResult actionResult, Response response) {

                if (actionResult.getStatus().equals("OK"))
                    uploadImage(actionResult.getValue());
                else
                    showErrorDialog(getResources().getString(R.string.error_occured), false);


                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {

                if (Utils.connected(DepoActivity.this))
                    showErrorDialog(getResources().getString(R.string.error_occured), false);
                else
                    showErrorDialog(getResources().getString(R.string.check_connection), false);

                dialog.dismiss();


            }
        });
    }

    private void uploadImage(String url) {
        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);
        restApi = setupRestClient(true, url);

        Bitmap bitmap = ((BitmapDrawable) ivImage.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 99, stream);
        byte[] image = stream.toByteArray();

        TypedInput typedBytes = new TypedByteArray("application/octet-stream", image);

        restApi.uploadImage(Utils.getUuid(), typedBytes, new Callback<ActionResult>() {
            @Override
            public void success(ActionResult actionResult, Response response) {
                dialog.dismiss();
                showErrorDialog(getResources().getString(R.string.saved_to_lifebox), true);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.dismiss();
                showErrorDialog(getResources().getString(R.string.error_occured), false);

            }
        });

    }

    private void startLoginScenario() {


        //TODO check whether connected wifi or not
        Boolean isWifi = Utils.connectedWifi(this);
//        boolean isWifi = false;

        Log.d("logWifi", "isWifi" + isWifi);


        if (isWifi != null) {
            if (isWifi)
                showLoginDialog();
            else
                mobileNetwork(false);
        } else
            showErrorDialog(getResources().getString(R.string.check_connection), false);


    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("logWifi", "onPause");


    }

    private void loginWhenConnectedWifi(String username, String password, final Dialog loginDialog) {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);

        restApi = cropyApp.setupRestClient(false, true);
        restApi.login(new NewUser(username, password, Utils.getDevice()), new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {

                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {

                    if (header.getName().equals("X-New-User")) {

                        showNotAUserDialog();

                    } else {
                        if (header.getName() != null) {
                            if (header.getName().equals("X-Auth-Token"))
                                xAuthToken = header.getValue();
                            else if (header.getName().equals("X-Remember-Me-Token"))
                                cropyApp.setxRememberMe(header.getValue());
                        }
                    }
                }

                dialog.dismiss();
                loginDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                dialog.dismiss();

                if (error.getResponse().getStatus() == 401 || error.getResponse().getStatus() == 412) {
                    errorCount++;
                    if (errorCount > 4) {
                        showErrorDialog(getResources().getString(R.string.fail_five_times), true);
                        errorCount = 0;
                    } else
                        showErrorDialog(getResources().getString(R.string.invalid_username_or_password), false);
//                        showSignupAkilliDepoDialog(null, false);

                } else
                    showErrorDialog(getResources().getString(R.string.error_occured), false);

            }
        });
    }

    private void provision() {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);

        restApi = setupRestClient(false, null);

        restApi.provision(new Callback<ActionResult>() {


            @Override
            public void success(ActionResult actionResult, Response response) {
                dialog.dismiss();
                mobileNetwork(true);

            }

            @Override
            public void failure(RetrofitError error) {

//                showErrorDialog(getResources().getString(R.string.dont_have_akilli_depo), false);
                showSignupAkilliDepoDialog(null, false);

                dialog.dismiss();


            }
        });
    }


    private void showNotAUserDialog() {
        final Dialog dialog = new Dialog(DepoActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_not_a_user);
        dialog.setCancelable(false);

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    dialog.dismiss();
                }
                return false;
            }
        });

        TextView tvYes = (TextView) dialog.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.tvNo);

        TextView tvTerms = (TextView) dialog.findViewById(R.id.tvTerms);
        final CheckBox cbTerms = (CheckBox) dialog.findViewById(R.id.cbTerms);


        cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !read) {
                    read = true;
                    eulaGet();

                }

            }
        });


        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                read = true;

                eulaGet();

            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cbTerms.isChecked()) {
                    Toast.makeText(DepoActivity.this, getResources().getString(R.string.accept_terms_and_conditions), Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    provision();
                }
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();

            }
        });

        dialog.show();

    }

    private void showErrorDialog(String body, final boolean finish) {

        final Dialog dialog = new Dialog(DepoActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_error);
        TextView tvOkay = (TextView) dialog.findViewById(R.id.tvOkay);
        TextView tvBody = (TextView) dialog.findViewById(R.id.tvBody);
        tvBody.setText(body);

        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (finish)
                    finish();
            }
        });
        dialog.show();

    }

    private void showSignupAkilliDepoDialog(String body, final boolean finish) {

        final Dialog dialog = new Dialog(DepoActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_signup_akilli_depo);
        TextView tvOkay = (TextView) dialog.findViewById(R.id.tvOkay);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (finish)
                    finish();
            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openApp(DepoActivity.this, "tr.com.turkcell.akillidepo");
            }
        });

        dialog.show();

    }

    private void showLoginDialog() {
        final Dialog dialog = new Dialog(DepoActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    dialog.dismiss();
                }
                return false;
            }
        });


        final TextView tvNumber = (TextView) dialog.findViewById(R.id.tvNumber);
        final TextView tvEmail = (TextView) dialog.findViewById(R.id.tvEmail);

        final ImageView ivNumber = (ImageView) dialog.findViewById(R.id.ivNumber);
        final ImageView ivEmail = (ImageView) dialog.findViewById(R.id.ivEmail);
        final TextView tvLogin = (TextView) dialog.findViewById(R.id.tvLogin);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        final EditText etEmail = (EditText) dialog.findViewById(R.id.etEmail);
        final EditText etNumber = (EditText) dialog.findViewById(R.id.etNumber);
        final EditText etCode = (EditText) dialog.findViewById(R.id.etCode);
        final EditText etPassword = (EditText) dialog.findViewById(R.id.etPassword);
        final RelativeLayout rlNumber = (RelativeLayout) dialog.findViewById(R.id.rlNumber);
        final RelativeLayout rlEmail = (RelativeLayout) dialog.findViewById(R.id.rlEmail);
        final RelativeLayout rlNumberBox = (RelativeLayout) dialog.findViewById(R.id.rlNumberBox);
        final RelativeLayout rlEmailBox = (RelativeLayout) dialog.findViewById(R.id.rlEmailBox);
        final LinearLayout llSignUp = (LinearLayout) dialog.findViewById(R.id.llSignUp);

        llSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openApp(DepoActivity.this, "tr.com.turkcell.akillidepo");
                dialog.dismiss();
                finish();
            }
        });


        rlNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlNumberBox.setVisibility(View.VISIBLE);
                rlEmailBox.setVisibility(View.INVISIBLE);
                tvNumber.setTextColor(getResources().getColor(R.color.white));
                tvEmail.setTextColor(getResources().getColor(R.color.blue));
                ivNumber.setVisibility(View.VISIBLE);
                ivEmail.setVisibility(View.INVISIBLE);

            }
        });

        rlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlNumberBox.setVisibility(View.INVISIBLE);
                rlEmailBox.setVisibility(View.VISIBLE);
                tvNumber.setTextColor(getResources().getColor(R.color.blue));
                tvEmail.setTextColor(getResources().getColor(R.color.white));
                ivNumber.setVisibility(View.INVISIBLE);
                ivEmail.setVisibility(View.VISIBLE);

            }
        });

        etCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() < 5)
                    etCode.setText(etCode.getText().toString() + "0 ");
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String username = (rlNumberBox.getVisibility() == View.VISIBLE) ? etNumber.getText().toString() : etEmail.getText().toString();

                if (username.equals("") || etPassword.getText().toString().equals(""))
                    Toast.makeText(DepoActivity.this, getResources().getString(R.string.text_field_cannot_empty), Toast.LENGTH_SHORT).show();
                else {
                    loginWhenConnectedWifi(username, etPassword.getText().toString(), dialog);
                    if (errorCount > 3) {
                        dialog.dismiss();
                    }
                }


            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }

    private void openApp(Context context, String packageName) {


        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//        final List pkgAppsList = context.getPackageManager().queryIntentActivities( mainIntent, 0);

//        Intent share = new Intent(Intent.ACTION_SEND);
        boolean depoAppFound = false;
        List<ResolveInfo> matches2 = DepoActivity.this.getPackageManager()
                .queryIntentActivities(mainIntent, 0);

        for (ResolveInfo info : matches2) {

            if (info.activityInfo.packageName.toLowerCase().startsWith(
                    packageName)) {
                mainIntent.setPackage(info.activityInfo.packageName);
                depoAppFound = true;
                break;
            }
        }

        if (depoAppFound) {
            PackageManager manager = context.getPackageManager();
            try {
                Intent i = manager.getLaunchIntentForPackage(packageName);
                if (i != null) {
                    //throw new PackageManager.NameNotFoundException();
                    i.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(i);
                }

            } catch (Exception e) {
            }
        } else {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=tr.com.turkcell.akillidepo"));
            startActivity(browserIntent);
        }

    }

    private void mobileNetwork(final boolean approveEula) {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);

        restApi = cropyApp.setupRestClient(false, false);
        restApi.gsm(Utils.getDevice(), new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {

                String newUser = null;
                String authToken = null;
                String rememberMe = null;


                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equals("X-Auth-Token"))
                        authToken = header.getValue();
                    else if (header.getName().equals("X-Remember-Me-Token"))
                        rememberMe = header.getValue();
                    else if (header.getName().equals("X-New-User"))
                        newUser = header.getValue();
                }

                if (newUser != null) {
                    xAuthToken = authToken;
                    startXNewUserScenario();
                } else {
                    xAuthToken = authToken;
                    cropyApp.setxRememberMe(rememberMe);
                    if (approveEula) {
                        Log.d("logId", "approveEula id: " + id);
                        eulaApprove(id);
                    }

                }


                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                dialog.dismiss();

                if (error.getResponse().getStatus() == 401) {
                    showLoginDialog();
                }
            }
        });


        String xNewUser = null;

        if (xNewUser == null) {

        } else {

        }
    }

    private void eulaApprove(int eulaid) {

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);

        restApi = setupRestClient(false, null);


        restApi.eulaApprove(eulaid, new Callback<ActionResult>() {


            @Override
            public void success(ActionResult actionResult, Response response) {
                dialog.dismiss();

                tvSave.performClick();

            }

            @Override
            public void failure(RetrofitError error) {

//                showErrorDialog(getResources().getString(R.string.dont_have_akilli_depo), false);
                showSignupAkilliDepoDialog(null, false);

                dialog.dismiss();


            }
        });
    }

    private void eulaGet() {

        String language = getResources().getString(R.string.language);

        final ProgressDialog dialog = ProgressDialog.show(this, "",
                getResources().getString(R.string.please_wait), true);
        restApi = cropyApp.setupRestClient(false, true);

        restApi.eulaGet(language, new Callback<Eula>() {

            @Override
            public void success(Eula eula, Response response) {


                id = eula.getId();

                Log.d("logId", "success id: " + id);

                Intent intent = new Intent(DepoActivity.this, TermsActivity.class);
                intent.putExtra("content", eula.getContent());
                DepoActivity.this.startActivity(intent);


                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                showErrorDialog(getResources().getString(R.string.error_occured), false);
                Log.d("logTerms", "error: " + error.getMessage());
                dialog.dismiss();
            }
        });
    }

    private void startXNewUserScenario() {
        showNotAUserDialog();
    }

    public RestApi setupRestClient(boolean upload, String url) {


        RestAdapter.Builder builder;
        if (upload) {

            builder = new RestAdapter.Builder()
                    .setEndpoint(url)
                    .setRequestInterceptor(requestInterceptorForImage)
                    .setClient(new OkClient(new OkHttpClient()))
                    .setLogLevel(RestAdapter.LogLevel.FULL);
        } else {
            builder = new RestAdapter.Builder()
                    .setEndpoint(Utils.BASE_URL_HTTPS)
                    .setRequestInterceptor(requestInterceptorWithAuthToken)
                    .setClient(new OkClient(new OkHttpClient()))
                    .setLogLevel(RestAdapter.LogLevel.FULL);
        }


        RestAdapter restAdapter = builder.build();
        RestApi restApi = restAdapter.create(RestApi.class);
        return restApi;
    }

    RequestInterceptor requestInterceptorWithAuthToken = new RequestInterceptor() {
        @Override
        public void intercept(RequestInterceptor.RequestFacade request) {
            request.addHeader("X-Auth-Token", xAuthToken);
            request.addHeader("Accept", "application/json");
            request.addHeader("Content-Type", "application/json");

        }
    };

    RequestInterceptor requestInterceptorForImage = new RequestInterceptor() {
        @Override
        public void intercept(RequestInterceptor.RequestFacade request) {
            request.addHeader("X-Auth-Token", xAuthToken);
            request.addHeader("Content-Type", "image/jpeg");
            request.addHeader("X-Meta-Strategy", "1");
            request.addHeader("X-Object-Meta-File-Name", "resim");
            request.addHeader("X-Object-Meta-Special-Folder", "CROPY");
            request.addHeader("Accept", "application/json");
        }
    };
}
