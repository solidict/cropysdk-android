package com.solidict.cropysdk;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.solidict.cropysdk.interfaces.RestApi;

public class TermsActivity extends Activity {

    CropyApp cropyApp;
    RestApi restApi;

    int id;

    ImageView ivBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        cropyApp = (CropyApp) getApplicationContext();

        String content = getIntent().getExtras().getString("content", null);

        WebView webview = (WebView) findViewById(R.id.wv);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadDataWithBaseURL("", content, "text/html", "UTF-8", "");

    }
}
