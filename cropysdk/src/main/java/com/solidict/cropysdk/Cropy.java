package com.solidict.cropysdk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;

import com.solidict.cropysdk.models.UiProperty;
import com.solidict.cropysdk.utils.Utils;

import java.util.UUID;

/**
 * Created by serdarbuyukkanli on 02/01/17.
 */
public class Cropy {

    public static String apiKey = "";
    public static String origin = "";

    private Bitmap bitmap;

    protected static Activity activity;

    private boolean crop;
    private boolean rotate;
    private boolean adjustment;
    private boolean filter;
    private boolean text;
    private boolean arrow;
    private boolean line;
    private boolean pen;
    private boolean rect;
    private boolean circle;
    private boolean frame;
    private boolean pixelate;
    private boolean focus;
    private boolean caps;
    private UiProperty uiProperty;
    private static String sessionId = "";
    private boolean preCrop;
    private boolean autoFinish = true;

    public boolean isPreCrop() {
        return preCrop;
    }

    public void setPreCrop(boolean preCrop) {
        this.preCrop = preCrop;
    }

    public boolean isAutoFinish() {
        return autoFinish;
    }

    public void setAutoFinish(boolean autoFinish) {
        this.autoFinish = autoFinish;
    }

    public static String getSessionId() {
        return sessionId;
    }

    public void setUiProperty(UiProperty uiProperty) {
        this.uiProperty = uiProperty;
    }

    ShareScenario shareScenario = ShareScenario.Toolbar;

    public enum ShareScenario {
        Toolbar, Url, Bitmap
    }

    public void setShareScenario(ShareScenario shareScenario) {
        this.shareScenario = shareScenario;
    }

    public enum Feature {
        CROP, ROTATE, ADJUSTMENT, FILTER, TEXT, ARROW, LINE, PEN, RECT, CIRCLE, FRAME, PIXELATE, FOCUS, CAPS
    }

    public static class Builder {
        //required
        private final Bitmap bitmap;

        //optional
        private boolean crop = true;
        private boolean rotate = true;
        private boolean adjustment = true;
        private boolean filter = true;
        private boolean text = true;
        private boolean arrow = true;
        private boolean line = true;
        private boolean pen = true;
        private boolean rect = true;
        private boolean circle = true;
        private boolean frame = true;
        private boolean pixelate = true;
        private boolean focus = true;
        private boolean caps = true;

        public Builder(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        public Builder without(Feature... args) {
            for (Feature arg : args) {

                switch (arg) {
                    case CROP:
                        crop = false;
                        break;
                    case ROTATE:
                        rotate = false;
                        break;
                    case ADJUSTMENT:
                        adjustment = false;
                        break;
                    case FILTER:
                        filter = false;
                        break;
                    case TEXT:
                        text = false;
                        break;
                    case ARROW:
                        arrow = false;
                        break;
                    case LINE:
                        line = false;
                        break;
                    case PEN:
                        pen = false;
                        break;
                    case RECT:
                        rect = false;
                        break;
                    case CIRCLE:
                        circle = false;
                        break;
                    case FRAME:
                        frame = false;
                        break;
                    case PIXELATE:
                        pixelate = false;
                        break;
                    case FOCUS:
                        focus = false;
                        break;
                    case CAPS:
                        caps = false;
                        break;
                }

            }
            return this;
        }

        public Cropy build() {
            return new Cropy(this);
        }
    }

    public void finish() {

        Intent intent = new Intent("finish_activity");
        activity.sendBroadcast(intent);
    }

    public void start(Activity activity, Bitmap rotatedBitmap) {
//        CropActivity.startCropActivity(activity, bitmap, true);


        if (preCrop) {
            CropActivity.startCropActivity(activity, bitmap, 0, uiProperty);
        } else {
            this.activity = activity;

            sessionId = UUID.randomUUID().toString();

            CropyApp.croppedBitmap = rotatedBitmap;
            CropyApp.croppedBitmapPath = Utils.generateRandomJpegName();


            Intent startIntent = new Intent(activity, ShareActivity.class);
            startIntent.putExtra("webPageTitle", "");
            startIntent.putExtra("webUrl", "");
            if (uiProperty != null)
                startIntent.putExtra("uiProperty", uiProperty);

            startIntent.putExtra("shareScenario", shareScenario);

            startIntent.putExtra("crop", crop);
            startIntent.putExtra("rotate", rotate);
            startIntent.putExtra("adjustment", adjustment);
            startIntent.putExtra("filter", filter);
            startIntent.putExtra("text", text);
            startIntent.putExtra("arrow", arrow);
            startIntent.putExtra("line", line);
            startIntent.putExtra("pen", pen);
            startIntent.putExtra("rect", rect);
            startIntent.putExtra("circle", circle);
            startIntent.putExtra("frame", frame);
            startIntent.putExtra("pixelate", pixelate);
            startIntent.putExtra("focus", focus);
            startIntent.putExtra("caps", caps);

            startIntent.putExtra("autoFinish", autoFinish);

            activity.startActivity(startIntent);
        }


    }

    private Cropy(Builder builder) {
        bitmap = builder.bitmap;
        crop = builder.crop;
        rotate = builder.rotate;
        adjustment = builder.adjustment;
        filter = builder.filter;
        text = builder.text;
        arrow = builder.arrow;
        line = builder.line;
        pen = builder.pen;
        rect = builder.rect;
        circle = builder.circle;
        frame = builder.frame;
        pixelate = builder.pixelate;
        focus = builder.focus;
        caps = builder.caps;

    }

}
