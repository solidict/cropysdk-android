package com.solidict.cropysdk;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.solidict.cropysdk.cropper.ZoomableRelativeLayout;
import com.solidict.cropysdk.interfaces.RotateListener;
import com.solidict.cropysdk.interfaces.ShapeListener;
import com.solidict.cropysdk.log.LogManager;
import com.solidict.cropysdk.models.UiProperty;
import com.solidict.cropysdk.utils.Utils;


public class RotateActivity extends Activity implements View.OnTouchListener, ShapeListener {


    private ImageView ivBack;

    private ImageView ivOriginal;
    private ImageView ivOriginal90;

    private ZoomableRelativeLayout rlImage;
    private ZoomableRelativeLayout rlImage90;

    RelativeLayout rlCenter;
    RelativeLayout rlCenter90;


    private SeekBar sbRotate;
    private ImageView ivRotate;
    private RelativeLayout rlBottom;
    private TextView tvRotation;
    private RelativeLayout rlshapes;

    private ImageView ivCropToolbar;
    private ImageView ivShare;
    private ImageView ivUndo;

    private ImageView ivRotateToolbar;

    RelativeLayout content;

    private TextView tvTitle;
    private RelativeLayout rlTopBar;
    private RelativeLayout rlFrame;


    private static boolean shareToolEnabled;
    private static boolean undoEnabled;

    private static Activity mActivity;
    private static Bitmap mBitmap;
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    private PointF start = new PointF();
    private PointF mid = new PointF();
    float oldDist = 1f;
    float d;
    float[] lastEvent;

    int currentRotation = 0;

    float zoomScale = 1;

    Bitmap bitmap90;
    Bitmap bitmap180;
    Bitmap bitmap270;
    int solidRotation = 0;

    boolean changed = false;
    boolean rotated = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Utils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
//            if (this instanceof IntroActivity || this instanceof IntroTermsActivity || this instanceof SplashActivity) {
            if (getResources().getConfiguration().orientation == 1)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            else
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//            }
        }

        setContentView(R.layout.activity_rotate);

        content = (RelativeLayout) findViewById(R.id.content);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        ivOriginal = (ImageView) findViewById(R.id.ivOriginal);
        ivOriginal90 = (ImageView) findViewById(R.id.ivOriginal90);

        rlImage = (ZoomableRelativeLayout) findViewById(R.id.rlImage);
        rlImage90 = (ZoomableRelativeLayout) findViewById(R.id.rlImage90);

        rlCenter = (RelativeLayout) findViewById(R.id.rlCenter);
        rlCenter90 = (RelativeLayout) findViewById(R.id.rlCenter90);

        sbRotate = (SeekBar) findViewById(R.id.sbRotation);
        ivRotate = (ImageView) findViewById(R.id.ivRotation);
        rlBottom = (RelativeLayout) findViewById(R.id.rlBottom);
        rlshapes = (RelativeLayout) findViewById(R.id.rlshapes);
        tvRotation = (TextView) findViewById(R.id.tvRotation);
        ivCropToolbar = (ImageView) findViewById(R.id.ivCropToolbar);
        ivShare = (ImageView) findViewById(R.id.ivShare);
        ivUndo = (ImageView) findViewById(R.id.ivUndo);
        ivRotateToolbar = (ImageView) findViewById(R.id.ivRotateToolbar);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rlTopBar = (RelativeLayout) findViewById(R.id.rlTopBar);
        rlFrame = (RelativeLayout) findViewById(R.id.rlFrame);

        sbRotate.getThumb().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.yellow_thumb), PorterDuff.Mode.SRC_IN));
        sbRotate.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.yellow_thumb), PorterDuff.Mode.SRC_IN));

//        ContextCompat.getColor(context, R.color.your_color);


//        ivOriginal.setScaleType(ImageView.ScaleType.MATRIX);

        if (!undoEnabled) {
            ivUndo.setEnabled(false);
            ivUndo.setAlpha(0.4f);
        }

        bitmap90 = rotateBitmap(mBitmap, 90);
        bitmap180 = rotateBitmap(mBitmap, 180);
        bitmap270 = rotateBitmap(mBitmap, 270);

        if (mShareScenario != Cropy.ShareScenario.Toolbar) {
            ivShare.setImageBitmap(Utils.stringToBitmap(mUiProperty.getDoneIcon()));
        } else {
            ivShare.setImageBitmap(Utils.stringToBitmap(mUiProperty.getShareIcon()));
        }


        ViewTreeObserver vto = rlImage.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (rlImage.getHeight() / ((float) mBitmap.getHeight() / mBitmap.getWidth())), rlImage.getHeight());
//                ivOriginal.setLayoutParams(layoutParams);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlImage.getLayoutParams();
                params.height = rlImage.getHeight();
                params.width = (int) (rlImage.getHeight() / ((float) mBitmap.getHeight() / mBitmap.getWidth()));
                rlImage.setLayoutParams(params);


            }
        });

        ViewTreeObserver vto90 = rlImage90.getViewTreeObserver();
        vto90.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (rlImage.getHeight() / ((float) mBitmap.getHeight() / mBitmap.getWidth())), rlImage.getHeight());
//                ivOriginal.setLayoutParams(layoutParams);

                Log.d("logBitmapSize", "bitmap w: " + mBitmap.getWidth() + ", h: " + mBitmap.getHeight());
                Log.d("logBitmapSize", "bitmap90 w: " + bitmap90.getWidth() + ", h: " + bitmap90.getHeight());

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlImage90.getLayoutParams();
                params.width = rlImage90.getWidth();
                params.height = (int) (rlImage90.getWidth() / ((float) bitmap90.getWidth() / bitmap90.getHeight()));
                rlImage90.setLayoutParams(params);

            }
        });


        sbRotate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                changed = true;

                scale(-(progress - 45), rlImage, ivOriginal);
                scale(-(progress - 45), rlImage90, ivOriginal90);

//                ivOriginal.setImageMatrix(matrix);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

//                applyRotate(0);

            }
        });

        ivCropToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (solidRotation == 0) {
//                    finishActivityWithResult(1);
//                } else {
//                    applyRotate(1);
//                }

                LogManager.addLog("Editing Screen - Feature changed - Crop");

                if (changed) {
                    applyRotate(1);
                } else {
                    finishActivityWithResult(1);

                }


            }
        });
        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (solidRotation == 0) {
//                    finishActivityWithResult(-1);
//                } else {
//                    applyRotate(-1);
//                }


                if (changed) {
                    applyRotate(-1);
                } else {
                    finishActivityWithResult(-1);

                }


            }
        });


        ivRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changed = true;
                rotated = true;

                ivUndo.setEnabled(true);
                ivUndo.setAlpha(1f);

//                solidRotation += 90;
//                solidRotation %= 360;

//                scale(solidRotation, true);

                rlCenter.setVisibility(View.INVISIBLE);
                rlCenter90.setVisibility(View.INVISIBLE);


                solidRotation = (solidRotation + 1) % 4;

                switch (solidRotation) {
                    case 0:
                        rlCenter.setVisibility(View.VISIBLE);
                        ivOriginal.setImageBitmap(mBitmap);
                        break;
                    case 1:
                        rlCenter90.setVisibility(View.VISIBLE);
                        ivOriginal90.setImageBitmap(bitmap90);
                        break;
                    case 2:
                        rlCenter.setVisibility(View.VISIBLE);
                        ivOriginal.setImageBitmap(bitmap180);
                        break;
                    case 3:
                        rlCenter90.setVisibility(View.VISIBLE);
                        ivOriginal90.setImageBitmap(bitmap270);
                        break;

                }


            }
        });

        ivRotateToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (changed) {
                    applyRotate(0);
                } else {
                    finishActivityWithResult(0);

                }


            }
        });

        rlBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


//        Bitmap scaledBitmap = scaleBitmap(mBitmap);
        ivOriginal.setImageBitmap(mBitmap);
        ivOriginal90.setImageBitmap(bitmap90);


        ivOriginal.setOnTouchListener(this);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finishActivityWithResult(-2);

            }
        });
//        ivBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

        ivUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rotated) {
                    finish();
                } else {
                    finishActivityWithResult(-3);
                }


            }
        });


        if (mUiProperty != null)
            makeUiChanges(mUiProperty);
        else {
            mUiProperty = new UiProperty(RotateActivity.this);
        }

    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    private void scale(int progress, ZoomableRelativeLayout rlImage, ImageView ivOriginal) {
        Log.d("logZoomScale", "onProgressChanged: " + (progress));

//                matrix.postRotate(currentRotation - (progress - 45), ivOriginal.getMeasuredWidth() / 2,
//                        ivOriginal.getMeasuredHeight() / 2);

//        if (!solid) {
//            rlImage.rotate(progress + solidRotation);
//            currentRotation = (progress + solidRotation);
//
//        } else {
//            rlImage.rotate(progress);
//            currentRotation = (progress);
//
//        }

//        if (!solid) {
//            progress += solidRotation;
//        }

        rlImage.rotate(progress);
        currentRotation = (progress);


//        if (!solid)
//            progress %= 45;


        Log.d("logZoomScale", "onProgressChanged: " + (progress));

//        rlImage.rotate(progress);


        double tan = (double) ivOriginal.getHeight() / ivOriginal.getWidth();

        if (tan > 1) {
            tan = 1 / tan;
//            progress = 90 - progress;


            double alphaRd = Math.PI / Math.atan(tan);
            double alpha = 180 / alphaRd;

            double rotateAngle = Math.abs(progress) + alpha;
            Log.d("logZoomScaleA", "rotateAngle: " + rotateAngle + ", progress: " + progress + ", alpha: " + alpha);
            double h = Math.sqrt(Math.pow(ivOriginal.getWidth(), 2) + Math.pow(ivOriginal.getHeight(), 2));
            double createdWidth = Math.sin(Math.toRadians(rotateAngle)) * h;

            if (createdWidth < 0)
                createdWidth *= -1;


            float ratio = (float) (createdWidth / ivOriginal.getWidth());

            Log.d("logZoomScalee", "createdWidth: " + createdWidth + ", ivOriginal.getWidth(): " + ivOriginal.getWidth());


            if (rotateAngle > 0) {
//                        matrix.postScale(ratio / zoomScale, ratio / zoomScale, ivOriginal.getWidth() / 2, ivOriginal.getHeight() / 2);
                rlImage.scale(ratio, ivOriginal.getWidth() / 2, ivOriginal.getHeight() / 2);
                zoomScale = ratio;

            }


        } else {
            double alphaRd = Math.PI / Math.atan(tan);
            double alpha = 180 / alphaRd;

            double rotateAngle = Math.abs(progress) + alpha;
            double h = Math.sqrt(Math.pow(ivOriginal.getWidth(), 2) + Math.pow(ivOriginal.getHeight(), 2));
            double createdHeight = Math.sin(Math.toRadians(rotateAngle)) * h;

            if (createdHeight < 0)
                createdHeight *= -1;

            float ratio = (float) (createdHeight / ivOriginal.getHeight());


            if (rotateAngle > 0) {
                rlImage.scale(ratio, ivOriginal.getWidth() / 2, ivOriginal.getHeight() / 2);
                zoomScale = ratio;

            }

        }


    }

    private void applyRotate(int type) {

        if (solidRotation % 2 == 0) {
            rlImage.setPadding(0, 0, 0, 0);
            rlImage.setBackgroundColor(getResources().getColor(R.color.share_background));
            Bitmap cropped = viewToBitmap(rlImage, rlImage.getWidth(), rlImage.getHeight());
            finish();
            RotateListener rotateListener = (RotateListener) mActivity;
            rotateListener.onRotated(cropped, type);
        } else {
            rlImage90.setPadding(0, 0, 0, 0);
            rlImage90.setBackgroundColor(getResources().getColor(R.color.share_background));
            Bitmap cropped = viewToBitmap(rlImage90, rlImage90.getWidth(), rlImage90.getHeight());
            finish();
            RotateListener rotateListener = (RotateListener) mActivity;
            rotateListener.onRotated(cropped, type);
        }


    }

    public static Bitmap viewToBitmap(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    static UiProperty mUiProperty;

    static Cropy.ShareScenario mShareScenario = Cropy.ShareScenario.Toolbar;

    public static void startRotateActivity(Activity activity, Bitmap bitmap, boolean shareToolEnabled, boolean undoEnabled, Cropy.ShareScenario shareScenario, UiProperty uiProperty) {

        mUiProperty = uiProperty;
        mActivity = activity;
        mBitmap = bitmap;
        mShareScenario = shareScenario;
        RotateActivity.shareToolEnabled = shareToolEnabled;
        RotateActivity.undoEnabled = undoEnabled;

        Intent intent = new Intent(activity, RotateActivity.class);

        activity.startActivityForResult(intent, 1);
        activity.overridePendingTransition(0, 0);


    }

    private void makeUiChanges(UiProperty uiProperty) {

        //activity changes
        rlTopBar.setBackgroundColor(uiProperty.getHeaderColor());
        tvTitle.setText(uiProperty.getHeaderTitle());
        tvTitle.setTextColor(uiProperty.getHeaderTextColor());
        rlFrame.setBackgroundColor(uiProperty.getBackgroundColor());
        rlshapes.setBackgroundColor(uiProperty.getFooterColor());

        ivBack.setImageBitmap(Utils.stringToBitmap(uiProperty.getBackIcon()));

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        return true; // indicate event was handled

    }


    @Override
    public void onShapeChanged(int type) {

//        if (solidRotation == 0) {
//            finishActivityWithResult(type);
//        } else {
//            applyRotate(type);
//        }


        if (changed) {
            applyRotate(type);
        } else {
            finishActivityWithResult(type);
        }


    }

    private void finishActivityWithResult(int type) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", type);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap90 != null) {
            bitmap90.recycle();
            bitmap90 = null;
        }
        if (bitmap180 != null) {
            bitmap180.recycle();
            bitmap180 = null;

        }
        if (bitmap270 != null) {
            bitmap270.recycle();
            bitmap270 = null;
        }

        if (((BitmapDrawable) ivOriginal.getDrawable()).getBitmap() != null) {
            Log.d("logDrawable", "ivOriginal");
            ((BitmapDrawable) ivOriginal.getDrawable()).getBitmap().recycle();
            ivOriginal.setImageDrawable(null);
        }

        if (((BitmapDrawable) ivOriginal90.getDrawable()).getBitmap() != null) {
            Log.d("logDrawable", "ivOriginal90");
            ((BitmapDrawable) ivOriginal90.getDrawable()).getBitmap().recycle();
            ivOriginal90.setImageDrawable(null);
        }
        Runtime.getRuntime().gc();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        ((BitmapDrawable) ivOriginal.getDrawable()).getBitmap().recycle();
//        ((BitmapDrawable) ivOriginal90.getDrawable()).getBitmap().recycle();
//        bitmap90.recycle();
//        bitmap90=null;
//        bitmap180.recycle();
//        bitmap180=null;
//        bitmap270.recycle();
//        bitmap270=null;
//
//        System.gc();

    }

}
