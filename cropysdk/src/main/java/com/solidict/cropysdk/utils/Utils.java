package com.solidict.cropysdk.utils;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.solidict.cropysdk.interfaces.DeleteListener;
import com.solidict.cropysdk.models.Bookmark;
import com.solidict.cropysdk.models.DeviceInfo;
import com.solidict.cropysdk.models.SnapShot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by serdarbuyukkanli on 12/07/16.
 */
public class Utils {

    static boolean prod = true;

    //test
    public static String BASE_URL_HTTPS_TEST = "https://tcloudstb.turkcell.com.tr";
    //prod
    public static String BASE_URL_HTTPS = "https://mylifebox.com";

    //test
    public static String BASE_URL_HTTP_TEST = "http://tcloudstb.turkcell.com.tr";
    //prod
    public static String BASE_URL_HTTP = "http://mylifebox.com";

    //test
    public static String BASE_URL_SHARE_TEST = "https://fast-depths-2920.herokuapp.com";
    //prod
    public static String BASE_URL_SHARE = "https://cropy.com/upload";

    public static String BASE_URL_CROPY_HTTPS = "https://cropy.com";

    //test
    public static String BIP_SHARE_TEST = "bipent://send?text=";
    //prod
    public static String BIP_SHARE = "bip://send?text=";


    public static String SHARE_BODY = "http://cropyapp.com";
    public static String SHARE_TARGET = "support@cropyapp.com";

    public static int NUMBER_OF_SCROLL = 5;


    public static String getBaseUrlHttps() {
        return prod ? BASE_URL_HTTPS : BASE_URL_HTTPS_TEST;
    }

    public static String getBaseUrlHttp() {
        return prod ? BASE_URL_HTTP : BASE_URL_HTTP_TEST;
    }

    public static String getBaseUrlShare() {
        return prod ? BASE_URL_SHARE : BASE_URL_SHARE_TEST;
    }

    public static String getBipShare() {
        return prod ? BIP_SHARE : BIP_SHARE_TEST;
    }

    public static String getBaseUrlCropyHttps() {
        return BASE_URL_CROPY_HTTPS;
    }

    public static void setBaseUrlCropyHttps(String baseUrlCropyHttps) {
        BASE_URL_CROPY_HTTPS = baseUrlCropyHttps;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static String convertToJson(ArrayList<Bookmark> bookmarks) {
//        Gson gson = new Gson();
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson.toJson(bookmarks);
        return json;
    }

    public static ArrayList<Bookmark> convertToModel(String jsonString) {
        Gson gson = new Gson();
        Bookmark[] bookmarks = gson.fromJson(jsonString, Bookmark[].class);
        ArrayList<Bookmark> bookmarkList = new ArrayList<>();

        for (int i = 0; i < bookmarks.length; i++) {
            bookmarkList.add(bookmarks[i]);
        }
        return bookmarkList;
    }

    public static String snapShotToJson(SnapShot snapShot) {
        Gson gson = new Gson();
        String json = gson.toJson(snapShot);
        return json;
    }

    public static SnapShot jsonToSnapShot(String jsonString) {
        Gson gson = new Gson();
        SnapShot snapShot = gson.fromJson(jsonString, SnapShot.class);
        return snapShot;
    }


    public static boolean checkDomain(String keyWord) {
        String domains[] = {".com", ".org", ".net", ".int", ".edu", ".gov", ".info", ".tv", ".co"};
        for (int i = 0; i < domains.length; i++) {
            if (keyWord.contains(domains[i]))
                return true;
        }
        return false;
    }

    public static void addBookmark(Context context, String url, String keyword, boolean googleSearch) {

        String domainName = "";
        try {
            domainName = Utils.getDomainName(url, true);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (domainName.equals("google") || domainName.equals("googleadservices") || domainName.equals("admatic"))
            return;

        Log.d("logBookmark", "bookmark added: " + url);

        ArrayList<Bookmark> bookmarks = new ArrayList<>();

        SharedPreferences preferences = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        String jsonBookmarks = preferences.getString("bookmarks", null);

        if (jsonBookmarks != null)
            bookmarks = convertToModel(jsonBookmarks);

        Bookmark newBookmark = new Bookmark();
        newBookmark.setUrl(url);
        if (googleSearch) {
            newBookmark.setGoogleSearch(true);

            String formattedKeyword;
            if (keyword.length() > 10)
                formattedKeyword = keyword.substring(0, 9) + "...";
            else
                formattedKeyword = keyword;
            formattedKeyword = formattedKeyword.toUpperCase().charAt(0) + formattedKeyword.substring(1, formattedKeyword.length());

            newBookmark.setName(formattedKeyword);
            Log.d("logToken", "New bookmark: " + formattedKeyword);
            if (formattedKeyword.replace(" ", "").length() == 0)
                return;
        }


        String[] tokens;
        try {
            tokens = getDomainName(url, true).split("\\.");
            if (tokens[0].replace(" ", "").length() < 1)
                return;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }


        for (Bookmark bookmark : bookmarks) {
            try {
                if (getDomainName(bookmark.getUrl(), true).equals(getDomainName(newBookmark.getUrl(), true)))
                    return;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }

        bookmarks.add(0, newBookmark);
        if (bookmarks.size() > 8) {
            bookmarks.remove(bookmarks.size() - 1);
        }

        jsonBookmarks = convertToJson(bookmarks);

        Log.d("logBookmark", "bookmarks after adding: " + jsonBookmarks);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("bookmarks", jsonBookmarks);
        editor.commit();


    }

    public static float dpToPx(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return px;
    }

    public static String getDomainName(String url, boolean shortName) throws URISyntaxException {

        Log.d("logUrl", "url: " + url);

        URI uri = new URI(url);

        String domain = uri.getHost();
        domain = domain.startsWith("www.") ? domain.substring(4) : domain;
        Log.d("logUrl", "domain: " + domain);

        String[] tokens = domain.split("\\.");
        domain = tokens[0];
        String domainLong = "";

        List<String> topLevelDomains = Arrays.asList("com", "org", "net", "int", "edu", "gov", "mil", "co", "bel");
        for (int i = 0; i < tokens.length; i++) {
            if (topLevelDomains.contains(tokens[i])) {

                try {
                    domain = tokens[i - 1];
                    domainLong = tokens[i - 1] + "." + tokens[i];

                    if (tokens[i + 1] != null)
                        domainLong = domainLong + "." + tokens[i + 1];
                    break;
                } catch (Exception e) {

                }

            }

        }

        Log.d("logUrl", "domainLong: " + domainLong);

        if (shortName)
            return domain;
        else
            return domainLong;
    }


    public static String getPath(Activity activity, Uri uri) {
        // just some safety built in
        if (uri == null) {
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }


    public static DeviceInfo getDevice() {
        return new DeviceInfo(getUuid(), "Cropy", "CROPY");
    }

    public static String getUuid() {
        return UUID.randomUUID().toString();
    }

    public static int getValidUrlType(String keyWord) {

        if (keyWord.startsWith("http://") || keyWord.startsWith("https://"))
            return 0;
        else {
            if (keyWord.startsWith("www.") || keyWord.startsWith("m.") || keyWord.startsWith("mobile.") || keyWord.startsWith("web."))
                return 1;
            else
                return 2;
        }
    }

    public static boolean saveImage(Bitmap b, String path, boolean addToGallery) {

        boolean success;

        FileOutputStream fos = null;

        File imageFile = new File(path);

        try {
            fos = new FileOutputStream(imageFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (fos != null) {
            b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("logSave", "Utils saveImage");

            success = true;

            //saveToGallery
//            if (addToGallery) Utils.addImageToGallery(file);

            return success;

        } else
            return false;

    }

    private static String saveTemporaryImage(Bitmap bitmap, String path, String fileName) {
        try {
            String mPath = path + fileName;
            File imageFile = new File(mPath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 99, outputStream);
            outputStream.flush();
            outputStream.close();
            return mPath;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String generateRandomJpegName() {
//        Date now = new Date();
//        DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
//        String randomName = "/" + now + ".jpeg";


        String randomName = "/" + System.currentTimeMillis() + ".jpeg";
        return randomName;
    }

    public static void deleteTempFile(String path, Activity activity, boolean finishActivity) {

        String baseDirectory = Environment.getExternalStorageDirectory() + "/Cropy";

        if (!path.startsWith(baseDirectory)) {
            path = baseDirectory + path;
        }


        File file = new File(path);
        Log.d("logSave", "path: " + path);

        if (file.exists()) {
            if (file.delete()) {
                Log.d("logSave", "deleted");
//                callBroadCast(file, activity);
                callBroadCast(path, activity, finishActivity);
            } else {
                Log.d("logSave", "not found");
            }
        } else {
            Log.d("logSave", "does not exist");

        }

    }

    public static void callBroadCast(File file, Activity activity) {


        Uri contentUri = Uri.fromFile(file);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);

    }

    public static void callBroadCast(String path, final Activity activity, final boolean finishActivity) {


//        MediaScannerConnection mediaScannerConnection = new MediaScannerConnection(activity, new MediaScannerConnection.MediaScannerConnectionClient() {
//            @Override
//            public void onMediaScannerConnected() {
//
//            }
//
//            @Override
//            public void onScanCompleted(String path, Uri uri) {
//
//            }
//        });

//
//        mediaScannerConnection.connect();
//        mediaScannerConnection.scanFile(path, null);
//
//        activity.unbindService(mediaScannerConnection);


        MediaScannerConnection.scanFile(activity, new String[]{

                        path},

                null, new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri)

                    {
                        Log.d("logScanCompleted", "path: " + path);

                        if (finishActivity) {
                            DeleteListener deleteListener = (DeleteListener) activity;
                            deleteListener.onImageDeleted();
                        }

                    }


                });


    }

    public static File saveToInternalStorage(Context context, Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir


        File mypath = new File(directory, System.currentTimeMillis() + ".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath;
    }

    public static Bitmap loadImageFromStorage(File file) {

        try {
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static File createDirectoryAndSaveFile(String name, Context context, Bitmap imageToSave, boolean addToGallery) {

        Log.d("logSaveFile", "createDirectoryAndSaveFile: " + name + ", millis: " + System.currentTimeMillis());

        String baseDirectory = Environment.getExternalStorageDirectory() + "/Cropy";

        File direct = new File(baseDirectory);

        if (!direct.exists()) {
            File wallpaperDirectory = new File(baseDirectory);
            wallpaperDirectory.mkdirs();
        }

        String fileName;
        if (name != null)
            fileName = "/" + name;
        else
            fileName = generateRandomJpegName();


        String savedPath = saveTemporaryImage(imageToSave, baseDirectory, fileName);

        Log.d("logSaveFile", "savedPath: " + savedPath);


        final File file = new File(new File(baseDirectory), fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            File savedFile = new File(savedPath);

            FileOutputStream out = new FileOutputStream(savedFile);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 99, out);
            out.flush();
            out.close();

            if (addToGallery) {
                MediaScannerConnection.scanFile(context, new String[]{

                                savedFile.getAbsolutePath()},

                        null, new MediaScannerConnection.OnScanCompletedListener() {

                            public void onScanCompleted(String path, Uri uri)

                            {


                            }

                        });
            }


            return savedFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static Boolean connectedWifi(Activity activity) {
        ConnectivityManager conMan = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
//mobile
        NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState();
//wifi
        NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState();
        if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
            //mobile
            return false;
        } else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
            //wifi
            return true;
        }
        return null;
    }

    public static boolean connected(Activity activity) {

        Boolean connected = connectedWifi(activity);
        if (connected == null) return false;
        else return true;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter, boolean canBeLarger) {

        Log.d("scaleDown", "realImage width: " + realImage.getWidth() + ", height: " + realImage.getHeight());

        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());

        if ((maxImageSize < realImage.getWidth() || maxImageSize < realImage.getHeight()) || canBeLarger) {

            int width = Math.round((float) ratio * realImage.getWidth());
            int height = Math.round((float) ratio * realImage.getHeight());

            Log.d("scaleDown", "width: " + width + ", height: " + height);


            Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);

            Log.d("scaleDown", "newBitmap width: " + newBitmap.getWidth() + ", height: " + newBitmap.getHeight());


            return newBitmap;
        }


        return realImage;
    }

    public static Bitmap scaleDownHeight(Bitmap realImage, float maxImageSize,
                                         boolean filter, boolean canBeLarger) {

        Log.d("scaleDown", "realImage width: " + realImage.getWidth() + ", height: " + realImage.getHeight());

        float ratio = maxImageSize / realImage.getHeight();

        if ((maxImageSize < realImage.getWidth() || maxImageSize < realImage.getHeight()) || canBeLarger) {

            int width = Math.round((float) ratio * realImage.getWidth());
            int height = Math.round((float) ratio * realImage.getHeight());

            Log.d("scaleDown", "width: " + width + ", height: " + height);


            Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);

            Log.d("scaleDown", "newBitmap width: " + newBitmap.getWidth() + ", height: " + newBitmap.getHeight());


            return newBitmap;
        }


        return realImage;
    }

    public static Bitmap fitWidthToScreen(Bitmap realImage, float screenWidth) {
        float ratio = screenWidth / realImage.getWidth();

        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Log.d("logSize", "width: " + width + ", height: " + height);

        Bitmap bmOverlay = Bitmap.createScaledBitmap(realImage, width,
                height, false);


//        Bitmap bmOverlay = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
//        Canvas canvas = new Canvas(bmOverlay);
//        Rect src = new Rect(0, 0, realImage.getWidth(), realImage.getHeight());
//        Rect dest = new Rect(0, 0, width, height);
//        canvas.drawBitmap(realImage, src, dest, null);

        Log.d("logSize", "bitmap width: " + bmOverlay.getWidth() + ", height: " + bmOverlay.getHeight());

        return bmOverlay;
    }


    public static int getBitmapHeightLimit(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Log.d("logSize", "x: " + width + ", y: " + height);

        if (width == 1440)
            return 8191;
        else
            return 4095;
    }

    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getScreenHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static int convertSpToPixels(float sp, Context context) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
        return px;
    }

    public static void recycleBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap getBitmapFromFile(String imageLocation, boolean thumbnail) {
        File imageFile = new File(imageLocation);
        if (imageFile.exists()) {

            Bitmap bm = BitmapFactory.decodeFile(imageLocation);
            if (imageLocation.contains("/Camera/")) {
                Matrix matrix = new Matrix();
                matrix.postRotate(Utils.getImageOrientation(imageLocation));
                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), matrix, true);
            }

            try {
//                        Bitmap scaledBitmap = Utils.scaleDown(bm, 100, true, false);

                if (thumbnail) {
                    bm = decodeSampledBitmapFromResource(imageLocation, 100, 100);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(Utils.getImageOrientation(imageLocation));
                    Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                            bm.getHeight(), matrix, true);
                    return rotatedBitmap;

                } else {
                    return bm;
                }


            } catch (Exception e) {
            }

        }
        return null;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String imageLocation,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageLocation, options);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(imageLocation, options);

    }

    public static String getVersion(Activity activity) {
        String versionName = "1.0";
        try {
            versionName = activity.getPackageManager()
                    .getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    public static Bitmap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
