//package com.solidict.cropysdk.utils;
//
//import android.graphics.Bitmap;
//import android.graphics.Rect;
//
//
//import com.solidict.cropysdk.R;
//
//import java.util.ArrayList;
//
//import ly.img.android.sdk.filter.LutColorFilter;
//import ly.img.android.sdk.models.ImageSource;
//import ly.img.android.sdk.models.frame.CustomPatchFrameConfig;
//import ly.img.android.sdk.models.frame.CustomPatchFrameDrawer;
//import ly.img.android.sdk.models.frame.FrameImageGroup;
//import ly.img.android.sdk.models.frame.FrameLayoutMode;
//import ly.img.android.sdk.models.frame.FrameTileMode;
//import ly.img.android.sdk.utils.BitmapLayer;
//
///**
// * Created by serdarbuyukkanli on 05/01/17.
// */
//public class ImgLy {
//
//    public static Bitmap getFrameInRequiredSize(int width, int height, CustomPatchFrameConfig frameConfig) {
//
//        BitmapLayer bitmapLayer = new BitmapLayer(width, height, Bitmap.Config.ARGB_8888);
//        new CustomPatchFrameDrawer(frameConfig).draw(
//                bitmapLayer,                    // Canvas
//                new Rect(0, 0, width, height),  // Destination
//                frameConfig.getRelativeScale()  // Relative Scale
//        );
//        return bitmapLayer.getBitmap();
//    }
//
//    public static Bitmap getFilterAppliedBitmap(Bitmap bitmap, LutColorFilter filter) {
//        return filter.renderImage(
//                bitmap, // Input
//                1,      // intensity
//                true   // true is faster but results in only 262.144 Colors);
//        );
//    }
//
//    public static ArrayList<CustomPatchFrameConfig> createFramesList() {
//        ArrayList<CustomPatchFrameConfig> frames = new ArrayList<>();
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(new ImageSource(R.drawable.imgly_frame_dia_top), FrameTileMode.Repeat),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_dia_top_left),
////                                new ImageSource(R.drawable.imgly_frame_dia_left), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_dia_bottom_left)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_dia_top_right),
////                                new ImageSource(R.drawable.imgly_frame_dia_right), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_dia_bottom_right)
////                        ),
////                        new FrameImageGroup(new ImageSource(R.drawable.imgly_frame_dia_bottom), FrameTileMode.Repeat),
////                        0.1f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top_left),
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom_right)
////                        ),
////                        0.25f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_top_left),
////                                new ImageSource(R.drawable.imgly_frame_white_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_white_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_white_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_white_bottom_right)
////                        ),
////                        0.25f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner),
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner),
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner)
////                        ),
////                        0.08f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom_right)
////                        ),
////                        0.056f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom_right)
////                        ),
////                        0.16f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top_left),
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom_right)
////                        ),
////                        0.20f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_wood1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_wood1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_left), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom_right)
////                        ),
////                        0.08f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.VerticalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_small), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_left),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_left), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_left)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_right),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_right), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle_left),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle_right)
////                        ),
////                        0.1f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_top_left),
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_camera_view_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom_left),
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom_right)
////                        ),
////                        0.1f
////                )
////        );
//
//        return frames;
//    }
//
//
//    //TODO extended
////    public static ArrayList<CustomPatchFrameConfig> createFramesList() {
////        ArrayList<CustomPatchFrameConfig> frames = new ArrayList<>();
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(new ImageSource(R.drawable.imgly_frame_dia_top), FrameTileMode.Repeat),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_dia_top_left),
////                                new ImageSource(R.drawable.imgly_frame_dia_left), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_dia_bottom_left)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_dia_top_right),
////                                new ImageSource(R.drawable.imgly_frame_dia_right), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_dia_bottom_right)
////                        ),
////                        new FrameImageGroup(new ImageSource(R.drawable.imgly_frame_dia_bottom), FrameTileMode.Repeat),
////                        0.1f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top_left),
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_grunge2_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_grunge2_bottom_right)
////                        ),
////                        0.25f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_top_left),
////                                new ImageSource(R.drawable.imgly_frame_white_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_white_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_white_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_white_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_white_bottom_right)
////                        ),
////                        0.25f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner),
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner),
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scruffy1_corner)
////                        ),
////                        0.08f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_stamp1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_stamp1_bottom_right)
////                        ),
////                        0.056f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scroll1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_scroll1_bottom_right)
////                        ),
////                        0.16f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top_left),
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_left), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_right), FrameTileMode.Repeat
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom), FrameTileMode.Repeat,
////                                new ImageSource(R.drawable.imgly_frame_scruffy2_bottom_right)
////                        ),
////                        0.20f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_top_left),
////                                new ImageSource(R.drawable.imgly_frame_wood1_top), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_wood1_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_left), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom_left),
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frame_wood1_bottom_right)
////                        ),
////                        0.08f
////                )
////        );
////
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.VerticalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_small), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_left),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_left), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_left)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_top_right),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_right), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle_left),
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_paper_clip_bottom_middle_right)
////                        ),
////                        0.1f
////                )
////        );
////
////        frames.add(
////                new CustomPatchFrameConfig(
////                        FrameLayoutMode.HorizontalInside,
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_top_left),
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_camera_view_top_right)
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_right), FrameTileMode.Stretch
////                        ),
////                        new FrameImageGroup(
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom_left),
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom), FrameTileMode.Stretch,
////                                new ImageSource(R.drawable.imgly_frames_camera_view_bottom_right)
////                        ),
////                        0.1f
////                )
////        );
////
////        return frames;
////    }
//
//
//}
